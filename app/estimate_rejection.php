<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estimate_rejection extends Model
{
    protected $table = 'estimate_rejection';
    protected $primaryKey = 'id_rejection';
    public $timestamps = false;

    public function estimate(){
    	return $this->hasMany('\App\estimate', 'id_rejection', 'id_rejection');
    }
}
