<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class division extends Model
{
    protected $table = 'division';
    protected $primaryKey = 'id_division';
    public $timestamps = false;

    public function projects(){
    	return $this->hasMany('\App\project', 'id_division', 'id_division');
    }
}
