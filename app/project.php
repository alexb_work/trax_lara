<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class project extends Model
{
    protected $table = 'project';
    protected $primaryKey = 'id_project';
    public $timestamps = false;

    protected $color;
    protected $nbTasks;

    protected $dates = ['date_project', 'last_change', 'end_date', 'end_time', 'req_date', 'delivery_date'];


    public function job_types(){
        return $this->belongsToMany('\App\job_type', "job_type_project", "id_project", "id_job_type");
    }

    public function advisors(){
        return $this->belongsToMany('\App\User', 'advisor_project', 'id_project', 'id_advisor');
    }

    public function cost_centers(){
        return $this->hasMany('\App\cost_center_split', 'id_project', 'id_project');
    }

    public function estimates(){
        return $this->hasMany('\App\estimate', 'id_project', 'id_project');
    }

    public function tasks(){
        return $this->hasMany('\App\task', 'id_project', 'id_project');
    }

    public function print_job(){
        return $this->hasOne('\App\print_job', 'id_project', 'id_project');
    }

    public function file_uploads(){
        return $this->hasMany('\App\file_upload', 'id_project', 'id_project');
    }

    public function status(){
        return $this->belongsTo('\App\status', 'id_status', 'id_status');
    }

    public function province(){
        return $this->belongsTo('\App\province', 'id_province', 'id_province');
    }

    public function division(){
        return $this->belongsTo('\App\division', 'id_division', 'id_division');
    }

    //serialize, unserialize attributes

    public function getEndDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setEndDateAttribute($value){
        $this->attributes['end_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getEndTimeAttribute($value){
        return date("H:i", strtotime($value));
    }

    public function setEndTimeAttribute($value){
        $this->attributes['end_time'] = Carbon::createFromFormat('H:i', $value);
    }

    public function getReqDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setReqDateAttribute($value){
        $this->attributes['req_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getDeliveryDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setDeliveryDateAttribute($value){
        $this->attributes['delivery_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }





    

/*
    protected $color;
    protected $statusFr;
    protected $statusEn;
    protected $endDate;
    protected $nbTasks;
    protected $projectTypeFr;
    protected $projectTypeEn;
   */
}
