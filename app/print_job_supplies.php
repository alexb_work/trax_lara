<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class print_job_supplies extends Model
{
    protected $table = 'print_job_supplies';
    protected $primaryKey = 'id_print_job_supplies';
    public $timestamps = false;

    public function print_job(){
    	return $this->belongsTo('\App\print_job', 'id_print_job', 'id_print_job');
    }

    public function supply_item(){
    	return $this->belongsTo('\App\supplies_item', 'supply_code', 'supply_code');
    }
}
