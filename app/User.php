<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'personal';
    protected $primaryKey = 'id_personal';
    public $timestamps = false;
    protected $fillable = [
		'nom_personal', 
		'prenom_personal', 
		'mail_personal',
		'password',
		'categorie',
		'initiales',
		'phone',
		'title_en',
		'title_fr',
		'location',
		'salt'
	];

	public function advisorProjects(){
        return $this->belongsToMany('\App\project', 'advisor_project', 'id_advisor', 'id_project');
    }

    public function hours(){
        return $this->hasMany('\App\hours_per_task', 'id_personal', 'id_personal');
    }

    public function assignedTasks(){
        return $this->hasMany('\App\assigned_task', 'id_assigned_person', 'id_personal');
    }

    public function advisorTemplates(){
        return $this->belongsToMany('\App\template', 'advisor_template', 'id_advisor', 'id_template');
    }

    public function category(){
        return $this->belongsTo('\App\categore_estimate', 'id_estimate_categorie', 'id_categorie_estimate');
    }

}
