<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paper_prices extends Model
{
    protected $table = 'paper_prices';
    protected $primaryKey = 'id_paper_prices';
    public $timestamps = false;

    public function paper(){
        return $this->hasMany('\App\paper', 'supply_code', 'supply_code');
    }
}
