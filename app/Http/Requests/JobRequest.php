<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name' => 'required',
            'end_date' => 'required|date|after:yesterday',
            'end_time' => array('required', 'regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'),
            'delivery_date' => 'required|date|after:end_date',
            'advisor.0'  => 'min:1',
            'advisor.1'  => 'different:advisor.0',
            'job_type' => 'min:1',
            'text_other_job_type' => 'required_if:other_job_type,1',
            'project_desc' => 'required',
            'name_client' => 'required'

        ];
    }
}
