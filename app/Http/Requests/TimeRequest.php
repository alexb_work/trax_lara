<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TimeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_completed' => 'required|date',
            'hours_completed' => 'numeric|min:0',
            'hours_non_billable' => 'numeric|min:0',

        ];
    }
}
