<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brief'             => 'required',
            'req_date'          => 'date|after:yesterday',
            'assigned_person.*' => 'required',
            'assigned_hours.*'  => 'required|numeric|min:0'

        ];
    }
}
