<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrintRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'p_province' => 'min:1',
            'p_received_date' => 'date',
            'p_due_date' => 'date',
            'p_due_time' => array('regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'),
            'p_start_date' => 'date|after:p_due_date',
            'p_start_time' => array('regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'),
            'p_work_date' => 'required|date|after:p_start_date',
            'p_work_time' => array('required', 'regex:/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/'),
            'p_tel' => array('regex:/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i'),
            'p_adress' => 'required',
            'delivery_type' => 'min:1',
            'p_purolator' => 'required_if:delivery_type.0,5|required_if:delivery_type.1,5|required_if:delivery_type.2,5|required_if:delivery_type.3,5|required_if:delivery_type.4,5',
            'p_doc_name.*' => 'required',
            'p_one_sided.*' => 'required|numeric|min:0',
            'p_double_sided.*' => 'required|numeric|min:0',
            'p_nb_copies.*' => 'required|numeric|min:1',
            'supply_price.*' => 'sometimes|min:1',
            'p_doc_color.*' => 'sometimes|min:1',
            'supply_item.*' => 'sometimes|min:1',
            'supply_qty.*'  => 'required|numeric|min:1',
            'outsource_name.*' => 'required',
            'outsource_code.*' => 'required',
            'outsource_cost.*' => 'required|numeric|min:1',
            'print_mngt_supplier.*.*' => 'required',
            'print_mngt_price.*.*' => 'required|numeric|min:1',
            'print_mngt_winner.*' => 'required',
            //finition
            's_quantity.*' => 'sometimes|numeric|min:0',
            'book_qty.*' => 'sometimes|numeric|min:0',
            'book_sheets.*' => 'sometimes|numeric|min:0',
            'sc_book_qty.*' => 'sometimes|numeric|min:0',
            'sc_book_sheets.*' => 'sometimes|numeric|min:0',
            'bind_ins_qty.*' => 'sometimes|numeric|min:0',
            'drill_holes.*' => 'sometimes|numeric|min:0',
            'drill_sheets.*' => 'sometimes|numeric|min:0',
            'cut_qty.*' => 'sometimes|numeric|min:0',
            'cut_sheet_qty.*' => 'sometimes|numeric|min:0',
            'pad_qty.*' => 'sometimes|numeric|min:0',
            'pad_sheet_qty.*' => 'sometimes|numeric|min:0',
            'fold_qty.*' => 'sometimes|numeric|min:0',
            'fold_sheet_qty.*' => 'sometimes|numeric|min:0',
            'insert_qty.*' => 'sometimes|numeric|min:0',
            'assembly_quantity_lift.*' => 'sometimes|numeric|min:0',
            'shrink_qty.*' => 'sometimes|numeric|min:0',


        ];
    }
}
