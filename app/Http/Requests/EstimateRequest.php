<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstimateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_price'    => 'required|numeric',
            'test_hours'    => 'required|numeric',
            'test_percent'  => 'required|numeric',
            'qa_price'      => 'required|numeric',
            'qa_hours'      => 'required|numeric',
            'qa_percent'    => 'required|numeric',
            'pm_price'      => 'required|numeric',
            'pm_hours'      => 'required|numeric',
            'pm_percent'    => 'required|numeric',
            'total_price'   => 'required|numeric',
            'date'          => 'required|date',
            'prepared_for'  => 'required',
            'prepared_by'   => 'required',
            'title.*'       => 'max:50'

        ];
    }
}
