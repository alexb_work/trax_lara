<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\project;
use App\task;
use App\status;
use LaravelLocalization;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Input::get("all") + 0;
        $projectsTemp = project::latest('date_project')->get();
        $list_project = array();
        foreach($projectsTemp as $project){
            if (in_array(Auth::user()->id_personal, $project->advisors->lists('id_personal')->toArray())){
                $list_project[] = $project->id_project;
            }
            else{
                foreach($project->tasks as $task){
                    foreach($task->assignments as $assignment){
                        if(Auth::user()->id_personal == $assignment->id_assigned_person)
                           $list_project[] = $project->id_project;
                    }
                }
            }
        }
        $projects = project::latest('date_project')->whereIn('id_project', $list_project)->where('id_status', '<>', 10)->get();
        $status = Status::where('id_status', '<>', 10)->where('id_status', '<>', 2)->pluck("title_status_".LaravelLocalization::getCurrentLocale(), 'id_status')->toArray();
    

        $this->setProjectCustomAttributes($projects);
        return view('home', compact('projects', 'status'));
    }

    public function all(){
        $projects = project::latest('date_project')->whereIn('id_status', [1,-1, 2])->get();
        $status = Status::where('id_status', '<>', 10)->pluck("title_status_".LaravelLocalization::getCurrentLocale(), 'id_status')->toArray();
        $this->setProjectCustomAttributes($projects);
        return view('home', compact('projects', 'status'));
    }

    public function archives(){
        $projects = project::latest('date_project')->whereIn('id_status', [10])->get();
        $status = Status::where('id_status', '<>', 10)->pluck("title_status_".LaravelLocalization::getCurrentLocale(), 'id_status')->toArray();
        $this->setProjectCustomAttributes($projects);
        return view('home', compact('projects', 'status'));
    }

    public function setProjectCustomAttributes($projects){
        foreach ($projects as $project) {
            $project->nbTasks = task::where('id_project', $project->id_project)->count();
            if($project->status->id_status == -1){$project->color='#CC0033';} // en attente
            if($project->status->id_status == 1 ){$project->color='#00CC00';} // en cours
            if($project->status->id_status == 0 ){$project->color='#2890D5';} // ferme 
        }
    }
}
