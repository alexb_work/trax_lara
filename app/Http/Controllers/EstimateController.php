<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Requests\EstimateRequest;

use LaravelLocalization;

use URL;
use HTML2PDF;
use PDF;

use App\project;
use App\estimate;
use App\estimate_status;
use App\buffer;
use App\categorie_estimate;
use App\estimate_title;
use App\estimate_rejection;
use App\print_job;
use App\paper;

class EstimateController extends Controller
{
    public function index(project $project){
    	$estimates = estimate::where('id_project', $project->id_project)->get();
    	$estimate_status = estimate_status::pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_estimate_status');
        $estimate_rejection = estimate_rejection::pluck('rejection_message_'.LaravelLocalization::getCurrentLocale(), 'id_rejection');
    	return view('pages.estimate.list_estimate', compact('project', 'estimates', 'estimate_status', 'estimate_rejection'));
    }

    public function add(project $project){
        $print_job = $project->print_job;
        //dd($print_job!==null);

        $calcValues = $this->calculatePrice($project);
    	$estimatePrice = $calcValues['price'];
        $estimateHours = $calcValues['hours'];
        $days = ceil($estimateHours/6);
    	$buffer = buffer::find(1);
    	$estimatePrice = round($estimatePrice, 2);
        $testHours = $estimateHours * $buffer->test_percent;
        $testPrice = round($testHours * $buffer->test_rate, 2);
        $qaHours = $estimateHours * $buffer->qa_percent;
        $qaPrice = round($qaHours * $buffer->qa_rate, 2);
        $pmHours = $estimateHours * $buffer->pm_percent;
        $pmPrice = round($pmHours * $buffer->pm_rate, 2);
        $totalPrice = round($estimatePrice + $testPrice + $qaPrice + $pmPrice + (($print_job!==null)?($print_job->price):0), 2);
        $prices = array("estimatePrice"=>$estimatePrice, "testPrice"=>$testPrice, "qaPrice"=>$qaPrice, "pmPrice"=>$pmPrice, "totalPrice"=>$totalPrice);
        $hours = array("estimateHours"=>$estimateHours, "testHours"=>$testHours, "qaHours"=>$qaHours, "pmHours"=>$pmHours);
        $percents = array("testPercent"=>$buffer->test_percent*100, "qaPercent"=>$buffer->qa_percent*100, "pmPercent"=>$buffer->pm_percent*100);
        $rates = array("testRate"=>$buffer->test_rate, "qaRate"=>$buffer->qa_rate, "pmRate"=>$buffer->pm_rate);

    	return view('pages.estimate.add_estimate', compact('project', 'prices', 'hours', 'percents', 'rates', 'days', 'print_job'));
    }

    public function edit(estimate $estimate){
        $project = project::find($estimate->id_project);
        $print_job = $project->print_job;
        $estimates = estimate::where('id_project', $project->id_project)->get();
        $revision = 0;
        foreach($estimates as $key=>$value){
            if($estimate->id_estimate == $value->id_estimate){
                $revision = $key+1;
                break;
            }
            
        }
        $calcValues = $this->calculatePrice($project);
        $estimatePrice = $estimate->estimate_before;
        $estimateHours = $estimate->estimate_hours;
        $buffer = buffer::find(1);
        $testHours = $estimate->test_price / $buffer->test_rate;
        $qaHours = $estimate->qa_price / $buffer->qa_rate;
        $pmHours = $estimate->pm_price / $buffer->pm_rate;
        $testPercent = $testHours / $estimateHours;
        $qaPercent = $qaHours / $estimateHours;
        $pmPercent = $pmHours / $estimateHours;
        $prices = array("estimatePrice"=>$estimatePrice, "testPrice"=>$estimate->test_price, "qaPrice"=>$estimate->qa_price, "pmPrice"=>$estimate->pm_price, "totalPrice"=>$estimate->total_price);
        $hours = array("estimateHours"=>round($estimateHours, 2), "testHours"=>round($testHours, 2), "qaHours"=>round($qaHours, 2), "pmHours"=>round($pmHours, 2));
        $percents = array("testPercent"=>round($testPercent*100, 2), "qaPercent"=>round($qaPercent*100, 2), "pmPercent"=>round($pmPercent*100, 2));
        $rates = array("testRate"=>$buffer->test_rate, "qaRate"=>$buffer->qa_rate, "pmRate"=>$buffer->pm_rate);

        return view('pages.estimate.edit_estimate', compact('project', 'estimate', 'prices', 'hours', 'percents', 'rates', 'revision', 'print_job'));
    }

    public function store(EstimateRequest $request){
        $newEstimate = new estimate;
        $newEstimate->id_project = Input::get('id_project');
        $newEstimate->total_price = Input::get('total_price');
        $newEstimate->validate = 1;
        $newEstimate->prepared_for = Input::get('prepared_for');
        $newEstimate->prepared_by = Input::get('prepared_by');
        $newEstimate->date = Input::get('date');
        $newEstimate->estimate_before = Input::get('estimate_before');
        $newEstimate->estimate_hours = Input::get('estimate_hours');
        $newEstimate->test_price = Input::get('test_price');
        $newEstimate->qa_price = Input::get('qa_price');
        $newEstimate->pm_price = Input::get('pm_price');
        $newEstimate->req_days = Input::get('req_days');
        $newEstimate->description = Input::get('description');
        $newEstimate->save();

        if(null !== Input::get('title')){
            foreach(Input::get('title') as $key => $value){
                $newTitle = new estimate_title;
                $newTitle->id_estimate =  $newEstimate->id_estimate;
                $newTitle->title = Input::get('title')[$key];
                $newTitle->content = Input::get('content')[$key];
                $newTitle->save();
            }
        }

        

        return redirect()->action('EstimateController@index', ["project"=>Input::get('id_project')]);
    } 

    public function update(EstimateRequest $request){
        $newEstimate = estimate::find(Input::get('id_estimate'));
        $newEstimate->id_project = Input::get('id_project');
        $newEstimate->total_price = Input::get('total_price');
        $newEstimate->validate = 1;
        $newEstimate->prepared_for = Input::get('prepared_for');
        $newEstimate->prepared_by = Input::get('prepared_by');
        $newEstimate->date = Input::get('date');
        $newEstimate->estimate_before = Input::get('estimate_before');
        $newEstimate->estimate_hours = Input::get('estimate_hours');
        $newEstimate->test_price = Input::get('test_price');
        $newEstimate->qa_price = Input::get('qa_price');
        $newEstimate->pm_price = Input::get('pm_price');
        $newEstimate->req_days = Input::get('req_days');
        $newEstimate->description = Input::get('description');
        $newEstimate->save();

        $oldTitles = estimate_title::where('id_estimate', $newEstimate->id_estimate)->get();
        $oldNewDiff = count($oldTitles) - count(Input::get('title'));
        if(count(Input::get('title')) == 0){
            if(count($oldTitles)>=1){
                foreach($oldTitles as $oldTitle){
                    $oldTitle->delete();
                }
            }
        }
        else if(count(Input::get('title')) == 1){
            $newTitle = new estimate_title;
            $newTitle->id_estimate =  $newEstimate->id_estimate;
            foreach(Input::get('title') as $key=>$title){
                $newTitle->title = Input::get('title')[$key];
                $newTitle->content = Input::get('content')[$key];
            }
            if(count($oldTitles)>1){
                foreach($oldTitles as $oldTitle){
                    $oldTitle->delete();
                }
                $newTitle->save();
            }
            else if(count($oldTitles)==1){
                $oldTitles[0]->id_estimate = $newTitle->id_estimate;
                $oldTitles[0]->title = $newTitle->title;
                $oldTitles[0]->content = $newTitle->content;
                $oldTitles[0]->save();
            }
            else{
                $newTitle->save();
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldTitles as $key => $oldTitle){
                $oldTitle->id_estimate = Input::get('id_estimate');
                $oldTitle->title = Input::get('title')[$key];
                $oldTitle->content = Input::get('content')[$key];

                $oldTitle->save();
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldTitles[$i]->delete();
                unset($oldTitles[$i]);
            }
            $oldTitles2 = array_values($oldTitles);
            
            foreach($oldTitles2 as $key => $oldTitle){
                $oldTitle->id_estimate = Input::get('id_estimate');
                $oldTitle->title = Input::get('title')[$key];
                $oldTitle->content = Input::get('content')[$key];

                $oldTitle->save();
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldTitles as $key => $oldTitle){
                $oldTitle->id_estimate = Input::get('id_estimate');
                $oldTitle->title = Input::get('title')[$key];
                $oldTitle->content = Input::get('content')[$key];

                $oldTitle->save();
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newTitle = new estimate_title;
                $newTitle->id_estimate =  $newEstimate->id_estimate;
                foreach(Input::get('title') as $key=>$title){
                    $newTitle->title = Input::get('title')[$key];
                    $newTitle->content = Input::get('content')[$key];
                }
                $newTitle->save();
            }
        }
        return redirect()->action('EstimateController@index', ["project"=>Input::get('id_project')]);
    }

    public function updateStatus(Request $request)
    {
        $newEstimate = estimate::find(Input::get('id_estimate'));
        $newEstimate->validate = Input::get('status');
        $newEstimate->save();
        $returnHTML = view('partials.estimate_status')->with('newEstimate', $newEstimate)->render();
        return $returnHTML;
    }

    public function setRejection(Request $request){
        $newEstimate = estimate::find(Input::get('id_estimate'));
        $newEstimate->id_rejection = Input::get('id_rejection');
        $newEstimate->save();
        $reject_message = "";
        if(LaravelLocalization::getCurrentLocale() == 'en')
            $reject_message = $newEstimate->rejection->rejection_message_en;
        else if(LaravelLocalization::getCurrentLocale() == 'fr')
            $reject_message = $newEstimate->rejection->rejection_message_fr;
        $returnHTML = view('partials.rejection_icon')->with('reject_message', $reject_message)->render();
        return $returnHTML;
    }

    public function generatePDF(estimate $estimate){
        $estimate_content = "";
        if($estimate->titles !== null){
            foreach($estimate->titles->all() as $key=>$title)
                $estimate_content .= "<p style='color:#6cae3d;font-size:30px;margin-bottom:15px;font-weight:bold;'>".$title->title."</p><hr><p style='word-break:break-all'>".$title->content."</p>";
        }
        $project = $estimate->project;
        $values = $this->calculatePrice($project);
        $content = "
        
<page backbottom='0mm'>
    <p style='text-align:right'> <img width='150' src='".URL::asset('images/xeroxestimate.png') ."' /></p>
    <div style='padding-left:30px;font-family:Arial;'>
            <p style='color:#6cae3d;font-size:28px;margin-bottom:10px;font-weight:normal;'>Project  <span style='text-transform:uppercase;'> $project->project_name</span></p>
                <p style='margin-top:5px;font-size:26px;color:gray;'> Proposal </p>
                
                <p style='margin-top:80px;margin-bottom:0;color:gray;font-size:20px;'>Prepared for:</p>
                <p style='margin-top:10px;margin-bottom:0;color:gray;font-size:18px;'> <span style='font-weight:bold;font-size:20px;'>$estimate->prepared_for</span></p>
                
                <p style='margin-top:20px;margin-bottom:0;color:gray;font-size:20px;'>Prepared by:</p>
                <p style='margin-top:10px;margin-bottom:0;color:gray;font-size:18px;'> <span style='font-weight:bold;font-size:20px;'>$estimate->prepared_by </span> </p>
                
                <p style='margin-top:50px;color:gray;font-size:20px;'>$estimate->date </p>
        </div>
        
        <br /><br />
        <div style='background-image:url(".URL::asset('images/estimatebg.jpg').");background-position:center bottom;height:500px;'>&nbsp;</div>
</page>

<page style='color:gray;font-size:18px;'>
    <p style='color:#6cae3d;font-size:30px;margin-bottom:15px;font-weight:bold;'>Introduction</p>
    <hr>
    <p>
    $project->project_desc
    </p>
    $estimate_content

    <p style='color:#6cae3d;font-size:30px;margin-bottom:15px;font-weight:bold;'>Timeline</p>
    <hr>
    <p>Xerox will require approximately $estimate->req_days days to finish this application</p>
    <p style='color:#6cae3d;font-size:30px;margin-bottom:15px;font-weight:bold;'>Budgetary costs</p>
    <hr>
    <p>The cost will be ".number_format($estimate->total_price, 2)."\$</p>
    <div style='page-break-after: always;'></div>
</page>




<page style='color:gray;font-size:16px;'>
        <div style='color:gray;'>
        <p style='color:gray;font-weight:bold;'>Xerox Creative Services – Interactive Web solutions</p>
        <p style='margin-top:0;'>Xerox Creative Services provides state of the art Web solutions for all types of common business applications. Our dedicated staff of creative Web analysts and programmers will take to heart and mind your everyday problems and spin a Web of solutions that will cater to your needs in areas such as multimedia publishing, data gathering and analysis, job tracking and planning, stock management and many others. You provide the problem and we will give you the solution, available everywhere the Web goes.</p>
        
        <p style='font-style:italic;color:gray;margin-top:0;'>Web applications</p>
        <p style='margin-top:0;'>Use our services to communicate with your clients directly: email marketing, with online ecommerce support. Connect this to an online stock management and delivery application and the whole process is integrated and available online.</p>
        <p style='margin-top:0;'>Need to speed things up? Use the latest technology in data recognition or bar code scanning to efficiently gather information and relay it to a centralized server for easy online management. Data gathering has never been easier, archiving becomes simple and retrieving and analyzing your data is always at your fingertips.</p>
        <p style='margin-top:0;'>Communicate with your personnel, your clients and your partners through online event and project planning applications.</p>
        <p style='margin-top:0;'>Complex product and pricing information gives your representatives a headache? Use an online interactive product and services application that knows all the rules and show them in real time what is available and at what price. Want to enhance each sale? Then let the application make suggestions based on the representatives selections and profile of the client.</p>
        
        <p style='font-style:italic;color:gray;margin-top:0;'>Interactive media</p>
        <p style='margin-top:0;'>Xerox can design and implement your whole Web presence. Our skilled staff of designers and Web specialists will oversee the visual and interactive aspects of your Web sites and maintain the conformity to your branding standards.</p>
        
        <p style='margin-top:0;'>Leverage your current documents by posting them online through interactive publications. Add visual animations such as pages that corner and turn naturally, with dimension and shading. Liven up your message by adding sound and motion, Flash animations and videos then make them connect to associated content you would want them to see.</p>
        <p style='margin-top:0;'>Want to manage everything or only a portion of your online content? Xerox will provide you with the online tools and content management system to allow you to maintain and update your site at your leisure.</p>
        
        <p style='font-style:italic;color:gray;margin-top:0;'>Mobile applications</p>
        <p style='margin-top:0;'>You have an online presence and Web applications and still think you don’t have room to grow? Keep yourself ahead of the competition. Maximize your online presence by building apps for mobile phones and tablets. Providing a useful mobile application tool is a valued and engaging way to interact with your internal or external clients.</p>
        
        <p style='font-style:italic;color:gray;margin-top:0;'>Create new ways to engage.</p>
        <p style='margin-top:0;margin-bottom:0;'>Trust Xerox Creative Services and their dynamic group of design and Web solutions specialists to successfully bring you where you need to be. Let us tackle the hurdles along the way.</p>
        </div>
        
</page>

";      
    //print $content;


        $pdf = PDF::loadHTML($content)->setPaper('a4', 'portrait');
        ob_end_clean();
        return $pdf->stream();
        
        
    }

    public function displayPrices(print_job $print_job){
        //dd($request);
        $repros = $print_job->reprographies;
        $supplies = $print_job->supplies;
        $outsources = $print_job->outsourcings;
        $finish = array();
        if($print_job->finition !== null)
            $finish['global'] = $print_job->finition;
        if($repros !== null){
            foreach($repros as $key=>$repro){
                if($repro->finition !== null)
                    $finish[$key] = $repro->finition;
            }
        }
        $paperPrices = paper::where('active', 1)->get();

        $reproPrices = array();
        $printPrices = array();
        if($repros !== null){
            foreach($repros as $key => $repro){
                $reproPrices[$key] = $repro->price;
                $printPrices[$key] = $repro->print_price;
            }
        }
        
        $supplyPrices = array();
        if($supplies !== null){
            foreach($supplies as $key => $supply){
                $supplyPrices[$key] = $supply->price;
            }
        }

        $finishPrices = array();
        if(!empty($finish)){
            foreach($finish as $key => $fini){
                $finishPrices[$key]['staple'] = $fini->s_price;
                $finishPrices[$key]['booklet'] = $fini->book_price;
                $finishPrices[$key]['sc'] = $fini->sc_price;
                $finishPrices[$key]['binder'] = $fini->bind_price;
                $finishPrices[$key]['drill'] = $fini->drill_price;
                $finishPrices[$key]['cut'] = $fini->cut_price;
                $finishPrices[$key]['pads'] = $fini->pad_price;
                $finishPrices[$key]['fold'] = $fini->fold_price;
                $finishPrices[$key]['insert'] = $fini->insert_price;
                $finishPrices[$key]['assembly'] = $fini->assembly_price;
                $finishPrices[$key]['total'] = $fini->total_price;
            }
        }

        $finishCategories = array();
        $finishCategories = array_pad($finishCategories, 10, 0);
        if($finishPrices != array()){
            foreach($finishPrices as $finish){
                $finishCategories[0] += $finish['staple'];
                $finishCategories[1] += $finish['booklet'];
                $finishCategories[2] += $finish['sc'];
                $finishCategories[3] += $finish['binder'];
                $finishCategories[4] += $finish['drill'];
                $finishCategories[5] += $finish['cut'];
                $finishCategories[6] += $finish['pads'];
                $finishCategories[7] += $finish['fold'];
                $finishCategories[8] += $finish['insert'];
                $finishCategories[9] += $finish['assembly'];
            }
        }

        $outsourcePrice = 0;
        if($outsources !== null){
            foreach ($outsources as $key => $outsource) {
                $outsourcePrice += $outsource->cost;
            }
        }

        $totalPrice = $print_job->price;
        
        $outputString = "
        <html>
        <head>
        <style>
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
            padding: 5px;
        }
        </style>
        </head>
        <body>";
        if($repros !== null){
            $outputString .= "
            <h1>". trans('print.repro') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.docName') ."</th>
                    <th>". trans('print.oneSided') ."</th>
                    <th>". trans('print.dblSided') ."</th>
                    <th>". trans('print.copies') ."</th>
                    <th>". trans('print.paperSupport') ."</th>
                    <th>". trans('print.type') ."</th>
                    <th>". trans('print.supplyPrice') ."</th>
                    <th>". trans('print.printPrice') ."</th>
                    <th>". trans('print.finishPrice') ."</th>

                </tr>
            ";
            foreach($repros as $key => $repro){
                $outputString .= "
                <tr>
                    <td>".$repro->doc_name."</td>
                    <td>".$repro->oneSided."</td>
                    <td>".$repro->dblSided."</td>
                    <td>".$repro->quantity."</td>
                    <td>".$repro->paper->description."</td>";
                if(LaravelLocalization::getCurrentLocale() == 'en')
                    $outputString .= "<td>".$repro->color_type->name_en."</td>";
                else if(LaravelLocalization::getCurrentLocale() == 'fr')
                    $outputString .= "<td>".$repro->color_type->name_fr."</td>";
                $outputString .= "<td>".number_format($reproPrices[$key], 2)."$</td>";
                $outputString .= "<td>".number_format($printPrices[$key], 2)."$</td>";
                $outputString .= "<td>".number_format((array_key_exists($key, $finishPrices)?$finishPrices[$key]['total']:0), 2)."$</td>
                </tr>
                ";
            }
            $outputString .= "</table>";
        }
        if($supplies!==null){
            $outputString .= "
            <h1>". trans('print.supplies') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.item') ." </th>
                    <th>". trans('print.qty') ." </th>
                    <th>". trans('print.price') ."</th>
                </tr>
            ";
            foreach($supplies as $key => $supply){
                $outputString .= "
                <tr>
                    <td>".$supply->supply_item->description."</td>
                    <td>".$supply->qty."</td>
                    <td>".number_format($supplyPrices[$key], 2)."$</td>
                </tr>
                ";
                
            }
            $outputString .= "</table>";
        }
        if($outsources!==null){
            $outputString .= "<h1>". trans('print.outsourcePrice') .": ".number_format($outsourcePrice, 2)."$</h1>";
        }
        if(!empty($finish)){  
            if(array_key_exists('global', $finish)){
            $outputString .= "
            <h1>". trans('print.global') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.category') ." </th>
                    <th>". trans('print.price') ." </th>
                </tr>
                <tr>
                    <td>". trans('print.stapled') ." </td>
                    <td>".number_format($finishPrices['global']['staple'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.booklet') ." </td>
                    <td>".number_format($finishPrices['global']['booklet'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.spiral')."/".trans('print.cerlox') ."</td>
                    <td>".number_format($finishPrices['global']['sc'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.binder') ." </td>
                    <td>".number_format($finishPrices['global']['binder'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.drill') ." </td>
                    <td>".number_format($finishPrices['global']['drill'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.cuts') ." </td>
                    <td>".number_format($finishPrices['global']['cut'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.pads') ." </td>
                    <td>".number_format($finishPrices['global']['pads'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.folds') ." </td>
                    <td>".number_format($finishPrices['global']['fold'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.insertion') ." </td>
                    <td>".number_format($finishPrices['global']['insert'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.assembly') ." </td>
                    <td>".number_format($finishPrices['global']['assembly'], 2)."$</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>".number_format($finishPrices['global']['total'], 2)."$</td>
                </tr>
                </table>";
            }

        }
        if(!empty($finish)){
            $outputString .= "
            <h1>Finish Details</h1>
            <table>
                <tr>
                    <th>". trans('print.category') ." </th>
                    <th>". trans('print.price') ." </th>
                </tr>
                <tr>
                    <td>". trans('print.stapled') ." </td>
                    <td>".number_format($finishCategories[0], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.booklet') ." </td>
                    <td>".number_format($finishCategories[1], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.spiral')."/".trans('print.cerlox') ."</td>
                    <td>".number_format($finishCategories[2], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.binder') ." </td>
                    <td>".number_format($finishCategories[3], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.drill') ." </td>
                    <td>".number_format($finishCategories[4], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.cuts') ." </td>
                    <td>".number_format($finishCategories[5], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.pads') ." </td>
                    <td>".number_format($finishCategories[6], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.folds') ." </td>
                    <td>".number_format($finishCategories[7], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.insertion') ." </td>
                    <td>".number_format($finishCategories[8], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.assembly') ." </td>
                    <td>".number_format($finishCategories[9], 2)."$</td>
                </tr>

                </table>";
        }
        $outputString .= "<h1>Total: ".number_format($totalPrice, 2)."$</h1>";
        $outputString .= "</body></html>";
        return $outputString;
    }

    private function calculatePrice(project $project){
        $tasks = $project->tasks;
        $assignments = array();
        $values = array();
        $hours = 0;
        $price = 0;
        foreach($tasks as $task){
            $assignments = array_merge($task->assignments->toArray(), $assignments);
        }
        foreach($assignments as $assignment){
            $category = categorie_estimate::find($assignment["id_estimate_categorie"]);
            $price += $assignment["assigned_hours"] * $category->prix_heure;
            $hours += $assignment["assigned_hours"];
        }
        $values['hours'] = $hours;
        $values['price'] = $price;
        return $values;
    }
}
