<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Requests\PrintRequest;

use LaravelLocalization;

use URL;
use DB;

use Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\project;
use App\province;
use App\outsource_type;
use App\color_type;
use App\stapled_type;
use App\delivery_type;
use App\print_job;
use App\reprography;
use App\finition;
use App\print_job_supplies;
use App\file_upload;
use App\outsourcing;

use App\supplies_item;
use App\supplies_price;
use App\paper;
use App\paper_prices;
use App\finish_services;
use App\finish_prices;
use App\print_category;
use App\print_category_prices;
use App\print_management_submission as submission;

class PrintController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(project $project){
    	$provinces = province::pluck('nom', 'id_province');
        $suppliesPrices = supplies_item::where('active', 1)->get();
        
        $supplies = array("0" => '---');
        foreach ($suppliesPrices as $supply){
            $supplies[$supply->supply_code] = $supply->supply_code." - ".$supply->description;
        }
        $paperPrices = paper::where('active', 1)->get();
        $paper = array('0' => '---');
        foreach ($paperPrices as $supPrice){
            $paper[$supPrice->supply_code] = $supPrice->supply_code." - ".$supPrice->description;
        }
        $outsources = outsource_type::orderBy('name_'.LaravelLocalization::getCurrentLocale(), 'ASC')->pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_outsource_type');
        $colors = color_type::pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_color_type');
        $deliveries = delivery_type::pluck('nom_'.LaravelLocalization::getCurrentLocale(), 'id_delivery_type');
        $stapledArr = stapled_type::pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_stapled_type');
        $stapledTypes = array('0' => '---');

        foreach ($stapledArr as $key=>$value){
            $stapledTypes[$key] = $value;
        }

        if($project->print_job !== null){
            $print_job = $project->print_job;
            return view('pages.print.form', compact('project', 'print_job', 'provinces', 'supplies', 'outsources', 'paper', 'colors', 'stapledTypes', 'deliveries'));
        }
        else
    	   return view('pages.print.form', compact('project', 'provinces', 'supplies', 'outsources', 'paper', 'colors', 'stapledTypes', 'deliveries'));
    }

    public function store(PrintRequest $request){
        $reproPrices = array();
        $supplyPrices = array();
        $printPrices = array();
        $finishPrices = array();
        $outsourcePrice = 0;
        $totalPrice = 0;
        
        list($reproPrices, $printPrices, $supplyPrices, $finishPrices, $outsourcePrice, $totalPrice) = $this->calculate_prices();
        $print_job = new print_job;
        $print_job->id_project = Input::get('id_project');
        
        $this->save_print($print_job, $totalPrice);

        $this->save_files($print_job);
        
        if(Input::get('p_doc_name')!==null){
            foreach(Input::get('p_doc_name') as $key => $value){
                $reprography = new reprography;
                $reprography->id_print_job = $print_job->id_print_job;
                $reprography->doc_name = Input::get('p_doc_name')[$key];
                $reprography->oneSided = Input::get('p_one_sided')[$key];
                $reprography->dblSided = Input::get('p_double_sided')[$key];
                $reprography->quantity = Input::get('p_nb_copies')[$key];
                $reprography->supply_code = Input::get('id_supply_price')[$key];
                $reprography->id_color_type = Input::get('p_doc_color')[$key];
                $reprography->price = $reproPrices[$key];
                $reprography->print_price = $printPrices[$key];
                $reprography->save();
                if(Input::get('id_stapled_type')!==null){  
                    if(array_key_exists($key, Input::get('id_stapled_type'))){
                        $finition = new finition;
    
                        $finition->id_repro = $reprography->id_repro;
                        $this->save_finition($finition, $key, $finishPrices[$key]);
                    }
                }       
            }
        }
        if(Input::get('supply_item')!==null){
            foreach(Input::get('supply_item') as $key => $value){
                $print_job_supplies = new print_job_supplies;
                $print_job_supplies->id_print_job = $print_job->id_print_job;
                $print_job_supplies->supply_code = $value;
                $print_job_supplies->qty = Input::get('supply_qty')[$key];
                $print_job_supplies->price = $supplyPrices[$key];
                $print_job_supplies->save();
            }
        }


        if(Input::get('outsource_type')!==null){  
            foreach(Input::get('outsource_type') as $key => $value){
                $outsourcing = new outsourcing;
                $outsourcing->id_outsource_type = $value;
                $outsourcing->id_print_job = $print_job->id_print_job;
                $outsourcing->name = Input::get('outsource_name')[$key];
                $outsourcing->code = Input::get('outsource_code')[$key];
                $outsourcing->cost = Input::get('outsource_cost')[$key];
                $outsourcing->save();
                if($value == '1'){
                    foreach(Input::get('print_mngt_supplier')[$key] as $subKey => $supValue){
                        $submission = new submission;
                        $submission->id_outsourcing = $outsourcing->id_outsourcing;
                        $submission->supplier = Input::get('print_mngt_supplier')[$key][$subKey];
                        $submission->price = Input::get('print_mngt_price')[$key][$subKey];
                        $submission->winner = (Input::get('print_mngt_winner')[$key] == $subKey)?true:false;
                        $submission->comments = Input::get('print_mngt_comment')[$key][$subKey];
                        $submission->save();
                        $this->store_submission_file($submission, $key, $subKey);
                    }
                }
            }
        }
        if(Input::get('id_stapled_type')!==null){  
            if(array_key_exists('global', Input::get('id_stapled_type'))){
                $finition = new finition;
                $finition->id_print_job = $print_job->id_print_job;
                $this->save_finition($finition, 'global', $finishPrices['global']);
            }
        }
            

        return redirect()->action('HomeController@index');

        
    }

    public function update(PrintRequest $request){
        $reproPrices = array();
        $supplyPrices = array();
        $finishPrices = array();
        $printPrices = array();
        $outsourcePrice = 0;
        $totalPrice = 0;
        list($reproPrices, $printPrices, $supplyPrices, $finishPrices, $outsourceType, $totalPrice) = $this->calculate_prices();

        $print_job = print_job::find(Input::get('id_print_job'));
        
        $this->save_print($print_job, $totalPrice);

        $this->save_files($print_job);

        $this->update_supplies($print_job, $supplyPrices);
        $this->update_outsourcing($print_job);

        if($print_job->finition !== null){
            if(Input::get('id_stapled_type')!==null){  
                if(array_key_exists('global', Input::get('id_stapled_type'))){
                    $finition = $print_job->finition;
                    $this->save_finition($finition, 'global', $finishPrices['global']);
                }
                else
                    $print_job->finition->delete();
            }
        }
        else{
            if(Input::get('id_stapled_type')!==null){  
                if(array_key_exists('global', Input::get('id_stapled_type'))){
                    $finition = new finition;
                    $finition->id_print_job = $print_job->id_print_job;
                    $this->save_finition($finition, 'global', $finishPrices['global']);
                }
            }
        }
        

        $this->update_repro($print_job, $reproPrices, $printPrices, $finishPrices);

        return redirect()->action('HomeController@index');
    }

    public function priceDisplay(PrintRequest $request){
        //dd($request);
        $paperPrices = paper::where('active', 1)->get();
        $reproPrices = array();
        $printPrices = array();
        $supplyPrices = array();
        $finishPrices = array();
        $outsourcePrice = 0;
        $totalPrice = 0;
        
        list($reproPrices, $printPrices, $supplyPrices, $finishPrices, $outsourcePrice, $totalPrice) = $this->calculate_prices();
        $outputString = "
        <html>
        <head>
        <style>
        table {
            border-collapse: collapse;
        }
        table, th, td {
            border: 1px solid black;
            padding: 5px;
        }
        </style>
        </head>
        <body>";
        if(Input::get('p_doc_name')!==null){
            $outputString .= "
            <h1>". trans('print.repro') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.docName') ."</th>
                    <th>". trans('print.oneSided') ."</th>
                    <th>". trans('print.dblSided') ."</th>
                    <th>". trans('print.copies') ."</th>
                    <th>". trans('print.paperSupport') ."</th>
                    <th>". trans('print.type') ."</th>
                    <th>". trans('print.supplyPrice') ."</th>
                    <th>". trans('print.printPrice') ."</th>
                    <th>". trans('print.finishPrice') ."</th>

                </tr>
            ";
            foreach(Input::get('p_doc_name') as $key => $value){
                $outputString .= "
                <tr>
                    <td>".Input::get('p_doc_name')[$key]."</td>
                    <td>".Input::get('p_one_sided')[$key]."</td>
                    <td>".Input::get('p_double_sided')[$key]."</td>
                    <td>".Input::get('p_nb_copies')[$key]."</td>
                    <td>".paper::find(Input::get('id_supply_price')[$key])->description."</td>";
                if(LaravelLocalization::getCurrentLocale() == 'en')
                    $outputString .= "<td>".color_type::find(Input::get('p_doc_color')[$key])->name_en."</td>";
                else if(LaravelLocalization::getCurrentLocale() == 'fr')
                    $outputString .= "<td>".color_type::find(Input::get('p_doc_color')[$key])->name_fr."</td>";
                $outputString .= "<td>".number_format($reproPrices[$key], 2)."$</td>";
                $outputString .= "<td>".number_format($printPrices[$key], 2)."$</td>";
                $outputString .= "<td>".number_format((array_key_exists($key, $finishPrices)?$finishPrices[$key]['total']:0), 2)."$</td>
                </tr>
                ";
            }
            $outputString .= "</table>";
        }
        if(Input::get('supply_item')!==null){
            $outputString .= "
            <h1>". trans('print.supplies') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.item') ." </th>
                    <th>". trans('print.qty') ." </th>
                    <th>". trans('print.price') ."</th>
                </tr>
            ";
            foreach(Input::get('supply_item') as $key => $value){
                $outputString .= "
                <tr>
                    <td>".supplies_item::find(Input::get('supply_item')[$key])->description."</td>
                    <td>".Input::get('supply_qty')[$key]."</td>
                    <td>".number_format($supplyPrices[$key], 2)."$</td>
                </tr>
                ";
                
            }
            $outputString .= "</table>";
        }
        if($outsourcePrice > 0){
            $outputString .= "<h1>". trans('print.outsourcePrice') .": ".number_format($outsourcePrice, 2)."$</h1>";
        }
        if(Input::get('id_stapled_type')!==null){  
            if(array_key_exists('global', Input::get('id_stapled_type'))){
            $outputString .= "
            <h1>". trans('print.global') ."</h1>
            <table>
                <tr>
                    <th>". trans('print.category') ." </th>
                    <th>". trans('print.price') ." </th>
                </tr>
                <tr>
                    <td>". trans('print.stapled') ." </td>
                    <td>".number_format($finishPrices['global']['staple'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.booklet') ." </td>
                    <td>".number_format($finishPrices['global']['booklet'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.spiral')."/".trans('print.cerlox') ."</td>
                    <td>".number_format($finishPrices['global']['sc'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.binder') ." </td>
                    <td>".number_format($finishPrices['global']['binder'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.drill') ." </td>
                    <td>".number_format($finishPrices['global']['drill'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.cuts') ." </td>
                    <td>".number_format($finishPrices['global']['cut'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.pads') ." </td>
                    <td>".number_format($finishPrices['global']['pads'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.folds') ." </td>
                    <td>".number_format($finishPrices['global']['fold'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.insertion') ." </td>
                    <td>".number_format($finishPrices['global']['insert'], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.assembly') ." </td>
                    <td>".number_format($finishPrices['global']['assembly'], 2)."$</td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>".number_format($finishPrices['global']['total'], 2)."$</td>
                </tr>
                </table>";
            }

        }
        if(Input::get('id_stapled_type')!==null){
            $finishCategories = $this->getFinishCategories($finishPrices);
            $outputString .= "
            <h1>Finish Details</h1>
            <table>
                <tr>
                    <th>". trans('print.category') ." </th>
                    <th>". trans('print.price') ." </th>
                </tr>
                <tr>
                    <td>". trans('print.stapled') ." </td>
                    <td>".number_format($finishCategories[0], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.booklet') ." </td>
                    <td>".number_format($finishCategories[1], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.spiral')."/".trans('print.cerlox') ."</td>
                    <td>".number_format($finishCategories[2], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.binder') ." </td>
                    <td>".number_format($finishCategories[3], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.drill') ." </td>
                    <td>".number_format($finishCategories[4], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.cuts') ." </td>
                    <td>".number_format($finishCategories[5], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.pads') ." </td>
                    <td>".number_format($finishCategories[6], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.folds') ." </td>
                    <td>".number_format($finishCategories[7], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.insertion') ." </td>
                    <td>".number_format($finishCategories[8], 2)."$</td>
                </tr>
                <tr>
                    <td>". trans('print.assembly') ." </td>
                    <td>".number_format($finishCategories[9], 2)."$</td>
                </tr>

                </table>";
        }
        $outputString .= "<h1>Total: ".number_format($totalPrice, 2)."$</h1>";
        $outputString .= "</body></html>";
        return $outputString;
    }

    private function save_finition(finition $finition, $key, $prices){
        $finition->id_stapled_type = Input::get('id_stapled_type')[$key]!=0?Input::get('id_stapled_type')[$key]:null;
        $finition->s_quantity = Input::get('s_quantity')[$key];
        $finition->book_qty = Input::get('book_qty')[$key];
        $finition->book_sheets = Input::get('book_sheets')[$key];
        $finition->book_size = Input::get('book_size')[$key];
        $finition->drill_holes = Input::get('drill_holes')[$key];
        $finition->drill_sheets = Input::get('drill_sheets')[$key];
        $finition->cut_final_size = Input::get('cut_final_size')[$key];
        $finition->cut_qty = Input::get('cut_qty')[$key];
        $finition->pad_size = Input::get('pad_size')[$key];
        $finition->pad_qty = Input::get('pad_qty')[$key];
        $finition->fold_qty =Input::get('fold_qty')[$key];
        $finition->fold_shape = Input::get('fold_shape')[$key];
        $finition->insert_env_size = Input::get('insert_env_size')[$key];
        $finition->insert_qty = Input::get('insert_qty')[$key];
        $finition->assembly_quantity_lift = Input::get('assembly_quantity_lift')[$key];
        $finition->sc_type = Input::get('sc_type')[$key]!='0'?Input::get('sc_type')[$key]:null;
        $finition->sc_book_qty = Input::get('sc_book_qty')[$key];
        $finition->sc_book_sheets = Input::get('sc_book_sheets')[$key];
        $finition->sc_color = Input::get('sc_color')[$key];
        $finition->sc_cover = Input::get('sc_cover')[$key];
        $finition->sc_back = Input::get('sc_back')[$key];
        $finition->bind_ins_qty = Input::get('bind_ins_qty')[$key];
        $finition->bind_comment = Input::get('bind_comment')[$key];
        $finition->cut_sheet_qty = Input::get('cut_sheet_qty')[$key];
        $finition->pad_sheet_qty = Input::get('pad_sheet_qty')[$key];
        $finition->fold_sheet_qty = Input::get('fold_sheet_qty')[$key];

        $finition->s_over79 = Input::get('s_over79')!==null?array_key_exists($key, Input::get('s_over79'))?Input::get('s_over79')[$key]:null:null;
        $finition->fold_method = Input::get('fold_method')!==null?array_key_exists($key, Input::get('fold_method'))?Input::get('fold_method')[$key]:null:null;
        $finition->insert_method = Input::get('insert_method')!==null?array_key_exists($key, Input::get('insert_method'))?Input::get('insert_method')[$key]:null:null;
        $finition->assembly_collat_method = Input::get('assembly_collat_method')!==null?array_key_exists($key, Input::get('assembly_collat_method'))?Input::get('assembly_collat_method')[$key]:null:null;
        $finition->bind_cust = Input::get('bind_cust')!==null?array_key_exists($key, Input::get('bind_cust'))?Input::get('bind_cust')[$key]:null:null;
        $finition->bind_cover = Input::get('bind_cover')!==null?array_key_exists($key, Input::get('bind_cover'))?Input::get('bind_cover')[$key]:null:null;
        $finition->bind_spine = Input::get('bind_spine')!==null?array_key_exists($key, Input::get('bind_spine'))?Input::get('bind_spine')[$key]:null:null;
        $finition->insert_label_qty = Input::get('insert_label_qty')!==null?array_key_exists($key, Input::get('insert_label_qty'))?Input::get('insert_label_qty')[$key]:null:null;
        $finition->insert_man_seal = Input::get('insert_man_seal')!==null?array_key_exists($key, Input::get('insert_man_seal'))?Input::get('insert_man_seal')[$key]:null:null;
        $finition->insert_canada_post = Input::get('insert_canada_post')!==null?array_key_exists($key, Input::get('insert_canada_post'))?Input::get('insert_canada_post')[$key]:null:null;
        $finition->insert_internal_mail = Input::get('insert_internal_mail')!==null?array_key_exists($key, Input::get('insert_internal_mail'))?Input::get('insert_internal_mail')[$key]:null:null;

        $finition->s_price = $prices['staple'];
        $finition->book_price = $prices['booklet'];
        $finition->sc_price = $prices['sc'];
        $finition->bind_price = $prices['binder'];
        $finition->drill_price = $prices['drill'];
        $finition->cut_price = $prices['cut'];
        $finition->pad_price = $prices['pads'];
        $finition->fold_price = $prices['fold'];
        $finition->insert_price = $prices['insert'];
        $finition->assembly_price = $prices['assembly'];
        $finition->total_price = $prices['total'];
        $finition->save();
    }

    private function save_print(print_job $print_job, $totalPrice){
        $print_job->p_received_date = Input::get('p_received_date');
        $print_job->p_due_date = Input::get('p_due_date');
        $print_job->p_due_time = Input::get('p_due_time');
        $print_job->p_start_date = Input::get('p_start_date');
        $print_job->p_start_time = Input::get('p_start_time');
        $print_job->p_work_date = Input::get('p_work_date');
        $print_job->p_work_time = Input::get('p_work_time');
        $print_job->p_tel = Input::get('p_tel');
        $print_job->p_other = Input::get('p_other');
        $print_job->p_adress = Input::get('p_adress');
        $print_job->p_purolator = Input::get('p_purolator');
        $print_job->p_scanning = Input::get('p_scanning');
        $print_job->p_finition_spec = Input::get('finition_spec');
        $print_job->p_info_type = Input::get('info_type');
        $print_job->p_info_explication = Input::get('info_explication');
        $print_job->id_province = Input::get('id_province');
        $print_job->price = $totalPrice;
        $print_job->save();

        if(null !== Input::get('delivery_type')){
            $print_job->delivery_types()->sync(Input::get('delivery_type'));
        }
        else
            $print_job->delivery_types()->sync(array());

        
    }

    private function save_files(print_job $print_job){
        if (Input::hasFile('project_file')){
                    

            foreach(Input::file('project_file') as $key => $file){
                //nom du fichier choisi:
                $nomFichier = $file->getClientOriginalName();
                //extension
                $extension = $file->getClientOriginalExtension();
                $newFile = new \App\file_upload;
                $newFile->id_file_upload = uniqid();
                $newFile->original_filename = $nomFichier;
                $newFile->id_print_job = $print_job->id_print_job;
                $newFile->extension = $extension;
                $newFile->file_desc = Input::get('file_desc')[$key];
                $fullFileName = $newFile->id_file_upload .'.'.$extension;
                $newFile->save();
                Storage::disk('local')->put($fullFileName, File::get($file));

            }
        }
    }

    private function store_submission_file(submission $submission, $outKey, $subKey){
        //dd(Input::file('sub_file')[$outKey][$subKey]);
        if (Input::file('sub_file')[$outKey][$subKey] !== null){
            $file = Input::file('sub_file')[$outKey][$subKey];
            $nomFichier = $file->getClientOriginalName();
            //extension
            $extension = $file->getClientOriginalExtension();
            $newFile = new \App\file_upload;
            $newFile->id_file_upload = uniqid();
            $newFile->original_filename = $nomFichier;
            $newFile->id_print_management_submission = $submission->id_print_management_submission;
            $newFile->extension = $extension;
            $newFile->file_desc = Input::get('sub_file_desc')[$outKey][$subKey];
            $fullFileName = $newFile->id_file_upload .'.'.$extension;
            $newFile->save();
            Storage::disk('local')->put($fullFileName, File::get($file));
        }
    }

    private function update_supplies(print_job $print_job, $supplyPrices){
        $oldSupplies = print_job_supplies::where('id_print_job', $print_job->id_print_job)->get();
        $oldNewDiff = count($oldSupplies) - count(Input::get('supply_item'));
        if(count(Input::get('supply_item')) == 0){
            if(count($oldSupplies)>=1){
                foreach($oldSupplies as $oldSupply){
                    $oldSupply->delete();
                }
            }
        }
        else if(count(Input::get('supply_item')) == 1){
            $newSupply = new print_job_supplies;
            $newSupply->id_print_job = $print_job->id_print_job;
            $newSupply->supply_code = Input::get('supply_item')[0];
            $newSupply->qty = Input::get('supply_qty')[0];
            $newSupply->price = $supplyPrices[0];
            if(count($oldSupplies)>1){
                foreach($oldSupplies as $oldSupply){
                    $oldSupply->delete();
                }
                $newSupply->save();
            }
            else if(count($oldSupplies)==1){
                $oldSupplies[0]->id_print_job = $newSupply->id_print_job;
                $oldSupplies[0]->supply_code = $newSupply->supply_code;
                $oldSupplies[0]->qty = $newSupply->qty;
                $oldSupplies[0]->price = $newSupply->price;
                $oldSupplies[0]->save();
            }
            else{
                $newSupply->save();
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldSupplies as $key => $oldSupply){
                $oldSupply->id_print_job = $print_job->id_print_job;
                $oldSupply->supply_code = Input::get('supply_item')[$key];
                $oldSupply->qty = Input::get('supply_qty')[$key];
                $oldSupply->price = $supplyPrices[$key];

                $oldSupply->save();
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldSupplies[$i]->delete();
                unset($oldSupplies[$i]);
            }
            $oldSupplies2 = array_values($oldSupplies);
            
            foreach($oldSupplies2 as $key => $oldSupply){
                $oldSupply->id_print_job = $print_job->id_print_job;
                $oldSupply->supply_code = Input::get('supply_item')[$key];
                $oldSupply->qty = Input::get('supply_qty')[$key];
                $oldSupply->price = $supplyPrices[$key];

                $oldSupply->save();
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldSupplies as $key => $oldSupply){
                $oldSupply->id_print_job = $print_job->id_print_job;
                $oldSupply->supply_code = Input::get('supply_item')[$key];
                $oldSupply->qty = Input::get('supply_qty')[$key];
                $oldSupply->price = $supplyPrices[$key];

                $oldSupply->save();
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newSupply = new print_job_supplies;
                $newSupply->id_print_job =  $print_job->id_print_job;
                $newSupply->supply_code = Input::get('supply_item')[0];
                $newSupply->qty = Input::get('supply_qty')[0];
                $oldSupply->price = $supplyPrices[0];
                $newSupply->save();
            }
        }
    }

    private function update_outsourcing(print_job $print_job){
        $oldOutsources = outsourcing::where('id_print_job', $print_job->id_print_job)->get();
        $oldNewDiff = count($oldOutsources) - count(Input::get('outsource_type'));
        if(count(Input::get('outsource_type')) == 0){
            if(count($oldOutsources)>=1){
                foreach($oldOutsources as $oldOutsource){
                    $oldOutsource->delete();
                }
            }
        }
        else if(count(Input::get('outsource_type')) == 1){
            $newOutsource = new outsourcing;
            $newOutsource->id_print_job = $print_job->id_print_job;
            $newOutsource->id_outsource_type = Input::get('outsource_type')[0];
            $newOutsource->name = Input::get('outsource_name')[0];
            $newOutsource->code = Input::get('outsource_code')[0];
            $newOutsource->cost = Input::get('outsource_cost')[0];

            if(count($oldOutsources)>1){
                foreach($oldOutsources as $oldOutsource){
                    $oldOutsource->delete();
                }
                $newOutsource->save();
                if($newOutsource->id_outsource_type == '1'){
                    foreach(Input::get('print_mngt_supplier')[0] as $subKey => $supValue){
                        $submission = new submission;
                        $submission->id_outsourcing = $newOutsource->id_outsourcing;
                        $submission->supplier = Input::get('print_mngt_supplier')[0][$subKey];
                        $submission->price = Input::get('print_mngt_price')[0][$subKey];
                        $submission->winner = (Input::get('print_mngt_winner')[0] == $subKey)?true:false;
                        $submission->comments = Input::get('print_mngt_comment')[0][$subKey];
                        $submission->save();
                    }
                }
            }
            else if(count($oldOutsources)==1){
                $oldOutsources[0]->id_print_job = $newOutsource->id_print_job;
                $oldOutsources[0]->id_outsource_type = $newOutsource->id_outsource_type;
                $oldOutsources[0]->name = $newOutsource->name;
                $oldOutsources[0]->code = $newOutsource->code;
                $oldOutsources[0]->cost = $newOutsource->cost;
                $oldOutsources[0]->save();
                if($oldOutsources[0]->submissions !== null)
                $this->update_submission($oldOutsources[0], 0);
            }
            else{
                $newOutsource->save();
                if($newOutsource->id_outsource_type == '1'){
                    foreach(Input::get('print_mngt_supplier')[0] as $subKey => $supValue){
                        $submission = new submission;
                        $submission->id_outsourcing = $newOutsource->id_outsourcing;
                        $submission->supplier = Input::get('print_mngt_supplier')[0][$subKey];
                        $submission->price = Input::get('print_mngt_price')[0][$subKey];
                        $submission->winner = (Input::get('print_mngt_winner')[0] == $subKey)?true:false;
                        $submission->comments = Input::get('print_mngt_comment')[0][$subKey];
                        $submission->save();
                    }
                }
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldOutsources as $key => $oldOutsource){
                $oldOutsource->id_print_job = $print_job->id_print_job;
                $oldOutsource->id_outsource_type = Input::get('outsource_type')[$key];
                $oldOutsource->name = Input::get('outsource_name')[$key];
                $oldOutsource->code = Input::get('outsource_code')[$key];
                $oldOutsource->cost = Input::get('outsource_cost')[$key];
                $oldOutsource->save();
                if($oldOutsources[$key]->submissions !== null)
                $this->update_submission($oldOutsources[$key], $key);
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldOutsources[$i]->delete();
                unset($oldOutsources[$i]);
            }
            $oldOutsources2 = array_values($oldOutsources);
            
            foreach($oldOutsources2 as $key => $oldOutsource){
                $oldOutsource->id_print_job = $print_job->id_print_job;
                $oldOutsource->id_outsource_type = Input::get('outsource_type')[$key];
                $oldOutsource->name = Input::get('outsource_name')[$key];
                $oldOutsource->code = Input::get('outsource_code')[$key];
                $oldOutsource->cost = Input::get('outsource_cost')[$key];
                $oldOutsource->save();
                if($oldOutsources[$key]->submissions !== null)
                $this->update_submission($oldOutsources[$key], $key);
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldOutsources as $key => $oldOutsource){
                $oldOutsource->id_print_job = $print_job->id_print_job;
                $oldOutsource->id_outsource_type = Input::get('outsource_type')[$key];
                $oldOutsource->name = Input::get('outsource_name')[$key];
                $oldOutsource->code = Input::get('outsource_code')[$key];
                $oldOutsource->cost = Input::get('outsource_cost')[$key];
                $oldOutsource->save();
                if($oldOutsources[$key]->submissions !== null)
                $this->update_submission($oldOutsources[$key], $key);
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newOutsource = new outsourcing;
                $newOutsource->id_print_job =  $print_job->id_print_job;
                $newOutsource->id_outsource_type = Input::get('outsource_type')[0];
                $newOutsource->name = Input::get('outsource_name')[0];
                $newOutsource->code = Input::get('outsource_code')[0];
                $newOutsource->cost = Input::get('outsource_cost')[0];
                $newOutsource->save();
                if($newOutsource->id_outsource_type == '1'){
                    foreach(Input::get('print_mngt_supplier')[0] as $subKey => $supValue){
                        $submission = new submission;
                        $submission->id_outsourcing = $newOutsource->id_outsourcing;
                        $submission->supplier = Input::get('print_mngt_supplier')[0][$subKey];
                        $submission->price = Input::get('print_mngt_price')[0][$subKey];
                        $submission->winner = (Input::get('print_mngt_winner')[0] == $subKey)?true:false;
                        $submission->comments = Input::get('print_mngt_comment')[0][$subKey];
                        $submission->save();
                    }
                }
            }
        }
    }

    private function update_submission(outsourcing $outsource, $outKey){
        $oldSubmissions = submission::where('id_outsourcing', $outsource->id_outsourcing)->get();
        $oldNewDiff = count($oldSubmissions) - count(Input::get('print_mngt_supplier')[$outKey]);
        if(count(Input::get('print_mngt_supplier')[$outKey]) == 1){
            $newSubmission = new submission;
            $newSubmission->id_outsourcing = $outsource->id_outsourcing;
            $newSubmission->supplier = Input::get('print_mngt_supplier')[$outKey][0];
            $newSubmission->price = Input::get('print_mngt_price')[$outKey][0];
            $newSubmission->winner = (Input::get('print_mngt_winner')[$outKey] == 0)?true:false;
            $newSubmission->comments = Input::get('print_mngt_comment')[$outKey][0];

            if(count($oldSubmissions)>1){
                foreach($oldSubmissions as $oldSubmission){
                    $oldSubmission->delete();
                }
                $newSubmission->save();
                $this->store_submission_file($newSubmission, $outKey, 0);
            }
            else if(count($oldSubmissions)==1){
                $oldSubmissions[0]->id_outsourcing = $newSubmission->id_outsourcing;
                $oldSubmissions[0]->supplier = $newSubmission->supplier;
                $oldSubmissions[0]->price = $newSubmission->price;
                $oldSubmissions[0]->price = $newSubmission->price;
                $oldSubmissions[0]->comments = $newSubmission->comments;
                $oldSubmissions[0]->save();
                $this->store_submission_file($oldSubmissions[0], $outKey, 0);
            }
            else{
                $newSubmission->save();
                $this->store_submission_file($newSubmission, $outKey, 0);
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldSubmissions as $key => $oldSubmission){
                $oldSubmission->id_outsourcing = $outsource->id_outsourcing;
                $oldSubmission->supplier = Input::get('print_mngt_supplier')[$outKey][$key];
                $oldSubmission->price = Input::get('print_mngt_price')[$outKey][$key];
                $oldSubmission->winner = (Input::get('print_mngt_winner')[$outKey] == $key)?true:false;
                $oldSubmission->comments = Input::get('print_mngt_comment')[$outKey][$key];
                $oldSubmission->save();
                $this->store_submission_file($oldSubmission, $outKey, $key);
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldSubmissions[$i]->delete();
                unset($oldSubmissions[$i]);
            }
            $oldSubmissions2 = array_values($oldSubmissions);
            foreach($oldSubmissions2 as $key => $oldSubmission){
                $oldSubmission->id_outsourcing = $outsource->id_outsourcing;
                $oldSubmission->supplier = Input::get('print_mngt_supplier')[$outKey][$key];
                $oldSubmission->price = Input::get('print_mngt_price')[$outKey][$key];
                $oldSubmission->winner = (Input::get('print_mngt_winner')[$outKey] == $key)?true:false;
                $oldSubmission->comments = Input::get('print_mngt_comment')[$outKey][$key];
                $oldSubmission->save();
                $this->store_submission_file($oldSubmission, $outKey, $key);
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldSubmissions as $key => $oldSubmission){
                $oldSubmission->id_outsourcing = $outsource->id_outsourcing;
                $oldSubmission->supplier = Input::get('print_mngt_supplier')[$outKey][$key];
                $oldSubmission->price = Input::get('print_mngt_price')[$outKey][$key];
                $oldSubmission->winner = (Input::get('print_mngt_winner')[$outKey] == $key)?true:false;
                $oldSubmission->comments = Input::get('print_mngt_comment')[$outKey][$key];
                $oldSubmission->save();
                $this->store_submission_file($oldSubmission, $outKey, $key);
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newSubmission = new submission;
                $newSubmission->id_outsourcing = $outsource->id_outsourcing;
                $newSubmission->supplier = Input::get('print_mngt_supplier')[$outKey][0];
                $newSubmission->price = Input::get('print_mngt_price')[$outKey][0];
                $newSubmission->winner = (Input::get('print_mngt_winner')[$outKey] == 0)?true:false;
                $newSubmission->comments = Input::get('print_mngt_comment')[$outKey][0];
                $newSubmission->save();
                $this->store_submission_file($oldSubmission, $outKey, 0);
            }
        }
    }

    private function update_repro(print_job $print_job, $reproPrices, $printPrices, $finishPrices){
        $oldRepros = reprography::where('id_print_job', $print_job->id_print_job)->get();
        $oldNewDiff = count($oldRepros) - count(Input::get('p_doc_name'));
        if(count(Input::get('p_doc_name')) == 0){
            if(count($oldRepros)>=1){
                foreach($oldRepros as $oldRepro){
                    $oldRepro->delete();
                }
            }
        }
        else if(count(Input::get('p_doc_name')) == 1){
            $newRepro = new reprography;
            $newRepro->id_print_job = $print_job->id_print_job;
            $newRepro->doc_name = Input::get('p_doc_name')[0];
            $newRepro->oneSided = Input::get('p_one_sided')[0];
            $newRepro->dblSided = Input::get('p_double_sided')[0];
            $newRepro->quantity = Input::get('p_nb_copies')[0];
            $newRepro->supply_code = Input::get('id_supply_price')[0];
            $newRepro->id_color_type = Input::get('p_doc_color')[0];
            $newRepro->price = $reproPrices[0];
            $newRepro->print_price = $printPrices[0];
            if(count($oldRepros)>1){
                foreach($oldRepros as $oldRepro){
                    $oldRepro->delete();
                }
                $newRepro->save();
                if(Input::get('id_stapled_type')!==null){  
                    if(array_key_exists(0, Input::get('id_stapled_type'))){
                        $finition = new finition;
    
                        $finition->id_repro = $newRepro->id_repro;
                        $this->save_finition($finition, 0, $finishPrices[0]);
                    }
                }       
            }
            else if(count($oldRepros)==1){
                $oldRepros[0]->id_print_job = $newRepro->id_print_job;
                $oldRepros[0]->doc_name = $newRepro->doc_name;
                $oldRepros[0]->oneSided = $newRepro->oneSided;
                $oldRepros[0]->dblSided = $newRepro->dblSided;
                $oldRepros[0]->quantity = $newRepro->quantity;
                $oldRepros[0]->supply_code = $newRepro->supply_code;
                $oldRepros[0]->id_color_type = $newRepro->id_color_type;
                $oldRepros[0]->price = $newRepro->price;
                $oldRepros[0]->print_price = $newRepro->print_price;

                if($oldRepros[0]->finition !== null){
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists(0, Input::get('id_stapled_type'))){
                            $finition = $oldRepros[0]->finition;
                            $this->save_finition($finition, 0, $finishPrices[0]);
                        }
                        else
                            $oldRepros[0]->finition->delete();
                    }
                }
                else{
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists(0, Input::get('id_stapled_type'))){
                            $finition = new finition;
                            $finition->id_repro = $oldRepros[0]->id_repro;
                            $this->save_finition($finition, 0, $finishPrices[0]);
                        }
                    }
                }
                

                $oldRepros[0]->save();

            }
            else{
                $newRepro->save();
                if(Input::get('id_stapled_type')!==null){  
                    if(array_key_exists(0, Input::get('id_stapled_type'))){
                        $finition = new finition;
    
                        $finition->id_repro = $newRepro->id_repro;
                        $this->save_finition($finition, 0, $finishPrices[0]);
                    }
                }       
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldRepros as $key => $oldRepro){
                $oldRepro->id_print_job = $print_job->id_print_job;
                $oldRepro->doc_name = Input::get('p_doc_name')[$key];
                $oldRepro->oneSided = Input::get('p_one_sided')[$key];
                $oldRepro->dblSided = Input::get('p_double_sided')[$key];
                $oldRepro->quantity = Input::get('p_nb_copies')[$key];
                $oldRepro->supply_code = Input::get('id_supply_price')[$key];
                $oldRepro->id_color_type = Input::get('p_doc_color')[$key];
                $oldRepro->price = $reproPrices[$key];
                $oldRepro->print_price = $printPrices[$key];

                if($oldRepro->finition !== null){
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = $oldRepro->finition;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                        else
                            $oldRepro->finition->delete();
                    }
                }
                else{
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = new finition;
                            $finition->id_repro = $oldRepro->id_repro;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                    }
                }

                $oldRepro->save();
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldRepros[$i]->delete();
                unset($oldRepros[$i]);
            }
            $oldRepros2 = array_values($oldRepros);
            
            foreach($oldRepros2 as $key => $oldRepro){
                $oldRepro->id_print_job = $print_job->id_print_job;
                $oldRepro->doc_name = Input::get('p_doc_name')[$key];
                $oldRepro->oneSided = Input::get('p_one_sided')[$key];
                $oldRepro->dblSided = Input::get('p_double_sided')[$key];
                $oldRepro->quantity = Input::get('p_nb_copies')[$key];
                $oldRepro->supply_code = Input::get('id_supply_price')[$key];
                $oldRepro->id_color_type = Input::get('p_doc_color')[$key];
                $oldRepro->price = $reproPrices[$key];
                $oldRepro->print_price = $printPrices[$key];

                if($oldRepro->finition !== null){
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = $oldRepro->finition;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                        else
                            $oldRepro->finition->delete();
                    }
                }
                else{
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = new finition;
                            $finition->id_repro = $oldRepro->id_repro;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                    }
                }

                $oldRepro->save();
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldRepros as $key => $oldRepro){
                $oldRepro->id_print_job = $print_job->id_print_job;
                $oldRepro->doc_name = Input::get('p_doc_name')[$key];
                $oldRepro->oneSided = Input::get('p_one_sided')[$key];
                $oldRepro->dblSided = Input::get('p_double_sided')[$key];
                $oldRepro->quantity = Input::get('p_nb_copies')[$key];
                $oldRepro->supply_code = Input::get('id_supply_price')[$key];
                $oldRepro->id_color_type = Input::get('p_doc_color')[$key];
                $oldRepro->price = $reproPrices[$key];
                $oldRepro->print_price = $printPrices[$key];

                if($oldRepro->finition !== null){
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = $oldRepro->finition;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                        else
                            $oldRepro->finition->delete();
                    }
                }
                else{
                    if(Input::get('id_stapled_type')!==null){  
                        if(array_key_exists($key, Input::get('id_stapled_type'))){
                            $finition = new finition;
                            $finition->id_repro = $oldRepro->id_repro;
                            $this->save_finition($finition, $key, $finishPrices[$key]);
                        }
                    }
                }

                $oldRepro->save();
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newRepro = new reprography;
                $newRepro->id_print_job = $print_job->id_print_job;
                $newRepro->doc_name = Input::get('p_doc_name')[0];
                $newRepro->oneSided = Input::get('p_one_sided')[0];
                $newRepro->dblSided = Input::get('p_double_sided')[0];
                $newRepro->quantity = Input::get('p_nb_copies')[0];
                $newRepro->supply_code = Input::get('id_supply_price')[0];
                $newRepro->id_color_type = Input::get('p_doc_color')[0];
                $newRepro->price = $reproPrices[0];
                $newRepro->print_price = $printPrices[0];
                $newRepro->save();
                if(Input::get('id_stapled_type')!==null){  
                    if(array_key_exists(0, Input::get('id_stapled_type'))){
                        $finition = new finition;
    
                        $finition->id_repro = $newRepro->id_repro;
                        $this->save_finition($finition, 0, $finishPrices[0]);
                    }
                }     
            }
        }
    }

    private function calculate_prices(){
        $totalPrice = 0;
        $reproPrices = array();
        $finishPrices = array();
        $printPrices = array();
        $outsourcePrice = 0;
        if(Input::get('p_doc_name')!==null){
            
            foreach(Input::get('p_doc_name') as $key => $value){
                $paperPrice = paper_prices::where('supply_code', Input::get('id_supply_price')[$key])->get();
                $mostRecent= $this->getMostRecentDate($paperPrice);
                $paperPrice = paper_prices::where('supply_code', Input::get('id_supply_price')[$key])->where('date_active', $mostRecent)->get()[0];
                $paper = paper::find(Input::get('id_supply_price')[$key]);
                
                $printCat = print_category::where('id_color_type', Input::get('p_doc_color')[$key])->where('paper_dimension', $paper->paper_size)->get()[0];
                $printRate = print_category_prices::where('id_print_category', $printCat->id_print_category)->get();
                $mostRecent= $this->getMostRecentDate($printRate);
                $printRate = print_category_prices::where('id_print_category', $printRate[0]->id_print_category)->where('date_active', $mostRecent)->get()[0];
                $printPrices[$key] = (Input::get('p_double_sided')[$key] + Input::get('p_one_sided')[$key]) * Input::get('p_nb_copies')[$key] * $printRate->price;
                $reproPrices[$key] = (Input::get('p_double_sided')[$key] + Input::get('p_one_sided')[$key]) * Input::get('p_nb_copies')[$key] * $paperPrice->price;

                if(substr(Input::get('id_supply_price')[$key], 0, 4) == '3R44'){
                    $leftoverTabs = 5 - ((Input::get('p_double_sided')[$key] + Input::get('p_one_sided')[$key]) % 5);
                    if($leftoverTabs != 5){
                        $reproPrices[$key] += $leftoverTabs * Input::get('p_nb_copies')[$key] * $paperPrice->price;
                    }
                }

                $totalPrice += $reproPrices[$key] + $printPrices[$key];
                if(Input::get('id_stapled_type')!==null){  
                    if(array_key_exists($key, Input::get('id_stapled_type'))){
                        $finishPrices[$key] = $this->calculateFinition($key);
                        $totalPrice += $finishPrices[$key]['total'];
                    }
                }
            }
        }
        $supplyPrices = array();
        if(Input::get('supply_item')!==null){
            foreach(Input::get('supply_item') as $key => $value){
                DB::connection()->enableQueryLog();
                $supplyPrice = supplies_price::where('supply_code', Input::get('supply_item')[$key])->get();
                $mostRecent = $this->getMostRecentDate($supplyPrice);

                $supplyPrice = supplies_price::where('supply_code', Input::get('supply_item')[$key])->where('date_active', $mostRecent)->pluck('price')[0];
                $supplyPrices[$key] = $supplyPrice * Input::get('supply_qty')[$key];
                $totalPrice += $supplyPrices[$key];
            }
        }

        if(Input::get('id_stapled_type')!==null){  
            if(array_key_exists('global', Input::get('id_stapled_type'))){
                $finishPrices['global'] = $this->calculateFinition('global');
                $totalPrice += $finishPrices['global']['total'];
            }
        }
        foreach($finishPrices as $finish){
            if($finish['booklet'] != 0){
                $totalPrice += $this->getFinishPrices('FI304');
                break;
            }
        }

        foreach($finishPrices as $finish){
            if($finish['sc'] != 0){
                $totalPrice += $this->getFinishPrices('FI304');
                break;
            }
        }

        if(Input::get('outsource_type')!==null){
            foreach(Input::get('outsource_cost') as $value)
            $outsourcePrice += $value;
            $totalPrice += $outsourcePrice;
        }
        return [$reproPrices, $printPrices, $supplyPrices, $finishPrices, $outsourcePrice, $totalPrice];

    }

    private function getMostRecentDate($records){
        $mostRecent= 0;
        foreach($records as $record){

            $curDate = $record->date_active;
            if ($curDate > $mostRecent) {
                $mostRecent = $curDate;
            } 
        }
        return $mostRecent;
    }

    private function calculateFinition($key){
        $finishPrices = array();
        //staple price
        $staplePrice = 0;
        if(Input::get('id_stapled_type')[$key] != 0){
            if(Input::get('s_over79')!==null){
                if(array_key_exists($key, Input::get('s_over79'))){
                    $staplePrice = $this->getFinishPrices('FI291');
                }
            }
            else{
                    $staplePrice = $this->getFinishPrices('FI296');
            }
            if(Input::get('s_quantity')[$key]=="")
                $finishPrices['staple'] = 0;
            else
                $finishPrices['staple'] = $staplePrice * Input::get('s_quantity')[$key];
        }
        else {
            $finishPrices['staple'] = 0;
        }

        //booklet price
        if(Input::get('book_qty')[$key]>0){
            //$finishPrices['booklet'] = ($this->getFinishPrices('FI303') * Input::get('book_qty')[$key]) + $this->getFinishPrices('FI304');
            $finishPrices['booklet'] = $this->getFinishPrices('FI303') * Input::get('book_qty')[$key];
        }
        else{
            $finishPrices['booklet'] = 0;
        }

        //spiral/cerlox price
        if(Input::get('sc_type')[$key] != '0'){
            $sheetsPerBook = Input::get('sc_book_sheets')[$key]-50>0?Input::get('sc_book_sheets')[$key]-50:0;
            $sheets = $sheetsPerBook * Input::get('sc_book_qty')[$key];
            switch (Input::get('sc_type')[$key]){
                case 'spiral':
                    $pricePerPage = $this->getFinishPrices('FI316');
                    $basePrice = $this->getFinishPrices('FI337');
                    break;
                case 'cerlox':
                    $pricePerPage = $this->getFinishPrices('FI032');
                    $basePrice = $this->getFinishPrices('FI009');
                    break;
                default:
                    $pricePerPage = 0;
                    $basePrice = 0;
                    break;
            }
            $finishPrices['sc'] = ($sheets * $pricePerPage) + $basePrice;
        }
        else{
            $finishPrices['sc'] = 0;
        }

        //binder
        if(Input::get('bind_ins_qty')[$key]>0){
            $finishPrices['binder'] = Input::get('bind_ins_qty')[$key] * $this->getFinishPrices('FI160');
        }
        else{
            $finishPrices['binder'] = 0;
        }
        

        //drilling
        if(Input::get('drill_holes')[$key]>0 && Input::get('drill_sheets')[$key]>0){
            $totalHoles = ceil(Input::get('drill_holes')[$key] * Input::get('drill_sheets')[$key] / 500);
            $finishPrices['drill'] = $totalHoles * $this->getFinishPrices('FI104');
        }
        else{
            $finishPrices['drill'] = 0;
        }

        //cutting
        if(Input::get('cut_qty')[$key]>0 && Input::get('cut_sheet_qty')[$key]>0){
            $totalCuts = ceil(Input::get('cut_sheet_qty')[$key] / 500);
            $finishPrices['cut'] = $totalCuts * Input::get('cut_qty')[$key] * $this->getFinishPrices('FI091');
        }
        else{
            $finishPrices['cut'] = 0;
        }

        //pads
        if(Input::get('pad_size')[$key] != '0'){
            switch (Input::get('pad_size')[$key]){
                case '4.5 X 5.5':
                    $unityPadPrice = $this->getFinishPrices('FI257');
                    break;
                case '8.5 X 11' :
                    $unityPadPrice = $this->getFinishPrices('FI261');
                    break;
                case '8.5 X 14' :
                    $unityPadPrice = $this->getFinishPrices('FI266');
                    break;
                case '8.5 X 5.5':
                    $unityPadPrice = $this->getFinishPrices('FI268');
                    break;
                default:
                    $unityPadPrice = 0;
            }
            $finishPrices['pads'] = $unityPadPrice * Input::get('pad_qty')[$key];

        }
        else{
            $finishPrices['pads'] = 0;            
        }

        //folds
        if(Input::get('fold_method')!==null){
            if(array_key_exists($key, Input::get('fold_method'))){
                switch (Input::get('fold_method')[$key]){
                    case 'machine':
                        $finishPrices['fold'] = Input::get('fold_sheet_qty')[$key] * $this->getFinishPrices('FI141');
                        break;
                    case 'manual':
                        $finishPrices['fold'] = Input::get('fold_qty')[$key] * $this->getFinishPrices('FI134');
                        break;
                    default:
                        $finishPrices['fold'] = 0;
                        break;
                }
            }
            else{
                $finishPrices['fold'] = 0;
            }

        }
        else{
            $finishPrices['fold'] = 0;
        }

        //insertion
        if(Input::get('insert_qty')[$key]>0){
            if(Input::get('insert_method')!==null){
                if(array_key_exists($key, Input::get('insert_method'))){
                    switch (Input::get('insert_method')[$key]){
                        case 'machine':
                            $finishPrices['insert'] = Input::get('insert_qty')[$key] * $this->getFinishPrices('FI162');
                            break;
                        case 'manual':
                            $insertMethodPrice = Input::get('insert_qty')[$key] * $this->getFinishPrices('FI160');
                            if(Input::get('insert_label_qty')!==null){
                                if(array_key_exists($key, Input::get('insert_label_qty'))){
                                    $insertLabelPrice = Input::get('insert_qty')[$key] * $this->getFinishPrices('FI164');
                                }
                            }
                            if(Input::get('insert_man_seal')!==null){
                                if(array_key_exists($key, Input::get('insert_man_seal'))){
                                    $insertEnvSealPrice = Input::get('insert_qty')[$key] * $this->getFinishPrices('FI123');
                                }
                            }
                            $finishPrices['insert'] =  $insertMethodPrice + $insertLabelPrice + $insertEnvSealPrice;
                            break;
                        default:
                            $finishPrices['insert'] = 0;
                            break;
                    }
                }
                else
                    $finishPrices['insert'] = 0;
            }
            else
                $finishPrices['insert'] = 0;

        }
        else
            $finishPrices['insert'] = 0;

        //assembly
        if(Input::get('assembly_quantity_lift')[$key]>0){
            if(Input::get('assembly_collat_method')!==null){
                if(array_key_exists($key, Input::get('assembly_collat_method'))){
                    switch (Input::get('assembly_collat_method')[$key]){
                        case 'machine':
                            $finishPrices['assembly'] = Input::get('assembly_quantity_lift')[$key] * $this->getFinishPrices('FI088');
                            break;
                        case 'manual':
                            $finishPrices['assembly'] = Input::get('assembly_quantity_lift')[$key] * $this->getFinishPrices('FI086');
                            break;
                        default:
                            $finishPrices['assembly'] = 0;
                            break;
                    }
                }
                else
                    $finishPrices['assembly'] = 0;
            }
            else
                $finishPrices['assembly'] = 0;

        }
        else
            $finishPrices['assembly'] = 0;
        $total = 0;
        foreach($finishPrices as $price){
            $total += $price;
        }
        $finishPrices['total'] = $total;

        return $finishPrices;
        

        
    }

    private function getFinishPrices($code){
        $pricing = finish_prices::where('supply_code', $code)->get();
        $mostRecent = $this->getMostRecentDate($pricing);
        return finish_prices::where('supply_code', $code)->where('date_active', $mostRecent)->pluck('price')[0];
    }

    private function getFinishCategories($finishPrices){
        $totalStapled = 0;
        $totalBooklet = 0;
        $totalSpiral = 0;
        $totalBinder = 0;
        $totalDrill = 0;
        $totalCuts = 0;
        $totalPads = 0;
        $totalFolds = 0;
        $totalInsert = 0;
        $totalAssembly = 0;
        foreach($finishPrices as $finish){
            $totalStapled += $finish['staple'];
            $totalBooklet += $finish['booklet'];
            $totalSpiral += $finish['sc'];
            $totalBinder += $finish['binder'];
            $totalDrill += $finish['drill'];
            $totalCuts += $finish['cut'];
            $totalPads += $finish['pads'];
            $totalFolds += $finish['fold'];
            $totalInsert += $finish['insert'];
            $totalAssembly += $finish['assembly'];
        }

        return array($totalStapled, $totalBooklet, $totalSpiral, $totalBinder, $totalDrill, $totalCuts, $totalPads, $totalFolds, $totalInsert, $totalAssembly);
    }
    


}
