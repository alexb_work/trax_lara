<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\TaskRequest;
use App\Http\Requests;
use App\task;
use App\project;
use App\task_status;
use App\User;
use App\task_type;
use App\categorie_estimate;
use App\assigned_task;
use Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use LaravelLocalization;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(project $project)
    {
        $tasks = array();
    	$tasks = task::orderBy('id_tasks', 'desc')->get()->where('id_project', $project->id_project);

    	return view('pages.task.list_task', compact('project', 'tasks'));
    }

    public function userList(project $project){
        $tasks = array();
        $tasksTemp = task::orderBy('id_tasks', 'desc')->get()->where('id_project', $project->id_project);
        foreach($tasksTemp as $taskTemp){
            foreach($taskTemp->assignments as $assignment){
                if($assignment->id_assigned_person == Auth::user()->id_personal){
                    $tasks[] = $taskTemp;
                    break;
                }    
            }   
        }

        return view('pages.task.list_task', compact('project', 'tasks'));
    }

    public function add(project $project)
    {
        $categories = categorie_estimate::orderBy('title_en', 'ASC')->pluck('title_'.LaravelLocalization::getCurrentLocale(), 'id_categorie_estimate');
        $taskType = task_type::pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_task_type');
    	$personalsRes = User::orderBy('nom_personal')->get();
        foreach ($personalsRes as $user){
            $personals[$user->id_personal] = $user->prenom_personal.' '.$user->nom_personal;
        }
		return view('pages.task.add_task', compact('project', 'personals', 'taskType', 'categories'));
    }

    public function edit(task $task)
    {
        $id_project = Input::get("id_project") + 0;
        $categories = categorie_estimate::orderBy('title_en', 'ASC')->pluck('title_'.LaravelLocalization::getCurrentLocale(), 'id_categorie_estimate');
        $taskType = task_type::pluck('name_'.LaravelLocalization::getCurrentLocale(), 'id_task_type');
        $personalsRes = User::orderBy('nom_personal')->get();
        foreach ($personalsRes as $user){
            $personals[$user->id_personal] = $user->prenom_personal.' '.$user->nom_personal;
        }
        return view('pages.task.edit_task', compact('id_project', 'personals', 'task', 'taskType', 'categories'));
    }

    public function complete(){
        $task = task::find(Input::get('task'));
        $task->task_status = 3;
        $task->save();
    }

    public function store(TaskRequest $request)
    {
        $id_project = Input::get("id_project") + 0;

        $newTask = new task;
        $newTask->id_project = $id_project;
        $newTask->last_change = date('Y-m-d H:i:s');
        $newTask->editing_by = Input::get('editing_by');
        $newTask->brief = Input::get('brief');
        $newTask->id_task_type = Input::get('id_task_type');
        $newTask->req_date = Input::get('req_date');
        $newTask->comment = Input::get('comment');
        $newTask->task_status = 1;

        $newTask->save();
        

        $project = project::find($id_project);
        if($project->id_status == 2){
            $project->id_status = 1;
            $project->save();
        }

        $this->addFiles($newTask);

        foreach(Input::get('assigned_person') as $key => $value){
            $newAssignment = new assigned_task;
            $newAssignment->id_assigned_person = $value;
            $newAssignment->id_task = $newTask->id_tasks;
            $newAssignment->id_estimate_categorie = Input::get('assigned_categorie')[$key];
            $newAssignment->assigned_hours = Input::get('assigned_hours')[$key];
            $newAssignment->save();
        }

        return redirect()->action('TaskController@index', ['project'=>$id_project]);
    }

    public function update(TaskRequest $request)
    {
        $id_task = Input::get("id_task") + 0;

        $newTask = task::find($id_task);
        $newTask->last_change = date('Y-m-d H:i:s');
        $newTask->editing_by = Input::get('editing_by');
        $newTask->brief = Input::get('brief');
        $newTask->id_task_type = Input::get('id_task_type');
        $newTask->req_date = Input::get('req_date');
        $newTask->comment = Input::get('comment');

        $newTask->save();

        $this->addFiles($newTask);

        $oldAssigns = \App\assigned_task::where('id_task', $id_task)->get();
        $oldNewDiff = count($oldAssigns) - count(Input::get('assigned_person'));
        if(count(Input::get('assigned_person')) == 1){
            $newAssignment = new assigned_task;
            $newAssignment->id_assigned_person = Input::get('assigned_person')[0];
            $newAssignment->id_task = $id_task;
            $newAssignment->id_estimate_categorie = Input::get('assigned_categorie')[0];
            $newAssignment->assigned_hours = Input::get('assigned_hours')[0];
            if(count($oldAssigns)>1){
                foreach($oldAssigns as $oldAssign){
                    $oldAssign->delete();
                }
                $newAsignment->save();
            }
            else{
                $oldAssigns[0]->id_assigned_person = $newAssignment->id_assigned_person;
                $oldAssigns[0]->id_task = $newAssignment->id_task;
                $oldAssigns[0]->id_estimate_categorie = $newAssignment->id_estimate_categorie;
                $oldAssigns[0]->assigned_hours = $newAssignment->assigned_hours;
                $oldAssigns[0]->save();
            }
            
        }
        else if($oldNewDiff == 0){
            foreach($oldAssigns as $key => $oldAssign){
                $oldAssign->id_assigned_person = Input::get('assigned_person')[$key];
                $oldAssign->id_task = $id_task;
                $oldAssign->id_estimate_categorie = Input::get('assigned_categorie')[$key];
                $oldAssign->assigned_hours = Input::get('assigned_hours')[$key];

                $oldAssign->save();
            }
        }
        else if($oldNewDiff > 0){
            for($i=0; $i<$oldNewDiff; $i++){
                $oldAssigns[$i]->delete();
                unset($oldAssigns[$i]);
            }
            $oldAssigns2 = array_values($oldAssigns);
            
            foreach($oldAssigns2 as $key => $oldAssign){
                $oldAssign->id_assigned_person = Input::get('assigned_person')[$key];
                $oldAssign->id_task = $id_task;
                $oldAssign->id_estimate_categorie = Input::get('assigned_categorie')[$key];
                $oldAssign->assigned_hours = Input::get('assigned_hours')[$key];

                $oldAssign->save();
            }

        }
        else if($oldNewDiff < 0){
            $count = 0;
            foreach($oldAssigns as $key => $oldAssign){
                $oldAssign->id_assigned_person = Input::get('assigned_person')[$key];
                $oldAssign->id_task = $id_task;
                $oldAssign->id_estimate_categorie = Input::get('assigned_categorie')[$key];
                $oldAssign->assigned_hours = Input::get('assigned_hours')[$key];

                $oldAssign->save();
                $count++;
            }
            for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

                $newAssignment = new assigned_task;
                $newAssignment->id_assigned_person = Input::get('assigned_person')[$i];
                $newAssignment->id_task = $id_task;
                $newAssignment->id_estimate_categorie = Input::get('assigned_categorie')[$i];
                $newAssignment->assigned_hours = Input::get('assigned_hours')[$i];
                $newAssignment->save();
            }

        }

        return redirect()->action('TaskController@index', ['project'=>$newTask->id_project]);
    }

    public function addFiles($newTask)
    {
        if (Input::hasFile('project_file')){
                    

            foreach(Input::file('project_file') as $key => $file){
                //nom du fichier choisi:
                $nomFichier = $file->getClientOriginalName();
                //extension
                $extension = $file->getClientOriginalExtension();
                $newFile = new \App\file_upload;
                $newFile->id_file_upload = uniqid();
                $newFile->original_filename = $nomFichier;
                $newFile->id_task = $newTask->id_tasks;
                $newFile->extension = $extension;
                $newFile->file_desc = Input::get('file_desc')[$key];
                $fullFileName = $newFile->id_file_upload .'.'.$extension;
                $newFile->save();
                Storage::disk('local')->put($fullFileName, File::get($file));

            }
        }
    }

}
