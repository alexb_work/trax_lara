<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use LaravelLocalization;

use App\Http\Requests;
use App\Http\Requests\TimeRequest;

use App\task;
use App\project;
use App\taux_horaire;
use App\categorie_estimate;
use App\hours_per_task;
use App\assigned_task;
use App\phpgrid\lib\inc\jqgrid_dist as jqgrid;

class TimeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('taskAssign');
    }
    
    public static function saveTime($data){
        $task = task::find($data["params"]["id_task"]);
        $error = false;
        $errorMesssage = "";
        $assignments = $task->assignments->sum('assigned_hours');
        $hoursSoFar = $task->hours()->sum('hours_completed');
        if ($data["params"]["hours_completed"] + $hoursSoFar > $assignments && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
               $errorMessage .= nl2br("-You are over the allocated amount of hours for this task. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Vous dépassez le nombre d'heures prévues pour cette tâche. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }
        if ($data["params"]["tarif_horaire"] != 1 && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
               $errorMessage .= nl2br("-Your rate is higher than 1. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Votre taux horaire est plus haut que 1. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }
        if ($data["params"]["hours_non_billable"] != '0' && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
               $errorMessage .= nl2br("-You have entered non-billable hours. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Vous avez entré des heures non-facturable. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }

        if($error){
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
            die($errorMessage);
        }
        $newHours = new hours_per_task;

        $newHours->id_task = $data["params"]["id_task"];
        $newHours->id_personal = $data["params"]["id_personal"];
        $newHours->date_completed = $data["params"]["date_completed"];
        $newHours->hours_completed = $data["params"]["hours_completed"];
        $newHours->hours_non_billable = $data["params"]["hours_non_billable"];
        $newHours->hours_comment = $data["params"]["hours_comment"];
        $newHours->id_categorie_estimate = $data["params"]["category"];
        $newHours->id_taux_horaire = $data["params"]["tarif_horaire"];
        $newHours->save();

        $task->task_status = 2;
        $task->save();

    }

    public static function updateTime($data){
        $task = task::find($data["params"]["id_task"]);
        $newHours = hours_per_task::find($data["params"]["id_hours_per_task"]);
        $error = false;
        $errorMessage = "";
        $assignments = $task->assignments->sum('assigned_hours');
        $hoursSoFar = $task->hours()->sum('hours_completed');
        if ($data["params"]["hours_completed"] + $hoursSoFar - $newHours->hours_completed > $assignments && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
                $errorMessage .= nl2br("-You are over the allocated amount of hours for this task. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Vous dépassez le nombre d'heures prévues pour cette tâche. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }
        if ($data["params"]["tarif_horaire"] != 1 && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
                $errorMessage .= nl2br("-Your rate is higher than 1. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Votre taux horaire est plus haut que 1. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }
        if ($data["params"]["hours_non_billable"] != '0' && $data["params"]["hours_comment"] == ""){
            if(LaravelLocalization::getCurrentLocale() == 'en'){
                $errorMessage .= nl2br("-You have entered non-billable hours. Please add a comment detailing why.\n");
            }
            else if(LaravelLocalization::getCurrentLocale() == 'fr'){
                $errorMessage .= nl2br("-Vous avez entré des heures non-facturable. Veuillez inclure un commentaire expliquant pourquoi.\n");
            }
            $error = true;
        }

        if($error){
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
            die($errorMessage);
        }
        

        $newHours->id_task = $data["params"]["id_task"];
        $newHours->id_personal = $data["params"]["id_personal"];
        $newHours->date_completed = $data["params"]["date_completed"];
        $newHours->hours_completed = $data["params"]["hours_completed"];
        $newHours->hours_non_billable = $data["params"]["hours_non_billable"];
        $newHours->hours_comment = $data["params"]["hours_comment"];
        $newHours->id_categorie_estimate = $data["params"]["category"];
        $newHours->id_taux_horaire = $data["params"]["tarif_horaire"];

        $newHours->save();
    }

    public static function deleteTime($data){
        print_r($data["params"]["id_hours_per_task"]);
        hours_per_task::destroy($data["id_hours_per_task"]);
    }

    public function timeForm(task $task){
        $assignments = $task->assignments->sum('assigned_hours');
        $hoursSoFar = $task->hours()->sum('hours_completed');
        $project = $task->project;
        $tasksAssign = Auth::user()->assignedTasks;
        $list_project = array();
        $projectsTemp = project::latest('date_project')->get();
        foreach($projectsTemp as $projectTemp){
            foreach($projectTemp->tasks as $taskTemp){
                foreach($taskTemp->assignments as $assignment){
                    if(Auth::user()->id_personal == $assignment->id_assigned_person)
                        $list_project[] = $projectTemp->id_project;
                }
            }
        }
        $projectsTemp = project::latest('date_project')->whereIn('id_project', $list_project)->where('id_status', '<>', 10)->where('id_status', '<>', 0)->get();
        $projects = array();
        foreach($projectsTemp as $projectTemp){
            $projects[$projectTemp->tasks->first()->id_tasks] = $projectTemp->project_name;
        }

        $tasks = array();
        foreach ($tasksAssign as $assign ){
            $taskArr = task::where('id_tasks', $assign->id_task)->where('id_project', $project->id_project)->pluck('brief', 'id_tasks')->toArray();
            $tasks = $taskArr + $tasks;
        }

    	//$tasks = $project->tasks()->pluck('brief', 'id_tasks');
    	foreach($tasks as $brief){
    		$brief = substr($brief, 0, 25);
    	}
    	$ratesArr = taux_horaire::pluck('tarif_horaire', 'id_taux_horaire');
        if(LaravelLocalization::getCurrentLocale() == 'en')
            $categoriesArr = categorie_estimate::orderBy('title_en', 'ASC')->pluck('title_en', 'id_categorie_estimate');
        else if(LaravelLocalization::getCurrentLocale() == 'fr')
            $categoriesArr = categorie_estimate::orderBy('title_en', 'ASC')->pluck('title_fr', 'id_categorie_estimate');
    	$rates = "";
    	$categories = "";

    	foreach ($ratesArr as $key => $value){
    		if ($key != 1){
    			$rates .= ';';
    		}
    		$rates .= $key.':'.$value;
    		
    	}
    	foreach ($categoriesArr as $key => $value){
    		if ($key != 1){
    			$categories .= ';';
    		}
    		$categories .= $key.':'.$value;
    	}


        include_once($_SERVER['DOCUMENT_ROOT'] . "/trax_dev_env/app/phpgrid/config.php");

        // set up DB
        mysqli_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS, PHPGRID_DBNAME);
        
        // include and create object

        $db_conf = array(      
                            "type"         => PHPGRID_DBTYPE,  
                            "server"     => PHPGRID_DBHOST, 
                            "user"         => PHPGRID_DBUSER, 
                            "password"     => PHPGRID_DBPASS, 
                            "database"     => PHPGRID_DBNAME 
                        ); 
        
        $g = new jqgrid($db_conf);
        
        
        // set few params
        $grid["caption"] = "Hours";
        $grid["width"] = "1425";
        $grid["add_options"] = array('width'=>'600');
        $grid["edit_options"] = array('width'=>'600');
        $grid["view_options"] = array('width'=>'600');
        $grid['footerrow'] = true;
        $g->set_options($grid);
        
        // set database table for CRUD operations
        $g->table = "hours_per_task";
        $userID = Auth::user()->id_personal;
        if(LaravelLocalization::getCurrentLocale() == 'fr'){
        $g->select_command = "SELECT h.id_hours_per_task, h.id_task, h.id_personal, h.date_completed, h.hours_completed, h.hours_non_billable, h.hours_comment, ce.title_fr as category, th.tarif_horaire FROM hours_per_task as h, categorie_estimate as ce, taux_horaire as th WHERE id_personal = $userID AND id_task = $task->id_tasks AND h.id_categorie_estimate = ce.id_categorie_estimate AND h.id_taux_horaire = th.id_taux_horaire";
        }
        else if(LaravelLocalization::getCurrentLocale()){
            $g->select_command = "SELECT h.id_hours_per_task, h.id_task, h.id_personal, h.date_completed, h.hours_completed, h.hours_non_billable, h.hours_comment, ce.title_en as category, th.tarif_horaire FROM hours_per_task as h, categorie_estimate as ce, taux_horaire as th WHERE id_personal = $userID AND id_task = $task->id_tasks AND h.id_categorie_estimate = ce.id_categorie_estimate AND h.id_taux_horaire = th.id_taux_horaire";
        }

        
        //set grid columns
        $col = array();
        $col["name"] = "id_hours_per_task"; // grid column name, same as db field or alias from sql
        $col["hidden"] = true;
        $cols[] = $col;  
        
        $col = array();
        $col["name"] = "id_personal"; // grid column name, same as db field or alias from sql
        $col["editoptions"] = array("defaultValue"=>Auth::user()->id_personal,"readonly"=>"readonly", "style"=>"border:0");
        $col["viewable"] = false; 
        $col["hidden"] = true;
        $col["editable"] = true;
        $cols[] = $col;  
        
        $col = array();
        $col["name"] = "id_task"; // grid column name, same as db field or alias from sql
        $col["editoptions"] = array("defaultValue"=>$task->id_tasks,"readonly"=>"readonly", "style"=>"border:0");
        $col["viewable"] = false; 
        $col["hidden"] = true;
        $col["editable"] = true;
        $cols[] = $col;
        
        $col = array();
        $col["title"] = trans('time_track.date'); // caption of column, can use HTML tags too
        $col["name"] = "date_completed"; // grid column name, same as db field or alias from sql
        $col["width"] = "45"; // width on grid
        $col["formatter"] = "date";
        $col["formatoptions"] = array("srcformat"=>'Y-m-d',"newformat"=>'Y-m-d');
        $col["datefmt"] = "Y-m-d";
        $col["editrules"] = array("required"=>true, "date"=>true); 
        $col["editable"] = true;
        $cols[] = $col;   
        
        $col = array();
        $col["title"] = trans('time_track.billable'); // caption of column, can use HTML tags too
        $col["name"] = "hours_completed"; // grid column name, same as db field or alias from sql
        $col["width"] = "35"; // width on grid
        $col["editoptions"] = array("defaultValue"=>0);
        $col["editrules"] = array("number"=>true);
        $col["editable"] = true;
        $cols[] = $col;  
        
        $col = array();
        $col["title"] = trans('time_track.nonBillable'); // caption of column, can use HTML tags too
        $col["name"] = "hours_non_billable"; // grid column name, same as db field or alias from sql
        $col["width"] = "35"; // width on grid
        $col["editoptions"] = array("defaultValue"=>0);
        $col["editrules"] = array("number"=>true); 
        $col["editable"] = true;
        $cols[] = $col;  

        $col = array();
        $col["title"] = trans('time_track.category'); // caption of column, can use HTML tags too      
        $col["name"] = "category"; // grid column name, same as db field or alias from sql
        $col["width"] = "100"; // width on grid
        $col["edittype"] = "select";
        $col["editoptions"] = array("value"=>$categories, "defaultValue"=>$task->assignments->where('id_assigned_person', Auth::user()->id_personal)->first()->id_estimate_categorie); 
        $col["editable"] = true;
        $cols[] = $col; 
        
        $col = array();
        $col["title"] = trans('time_track.rate'); // caption of column, can use HTML tags too
        $col["name"] = "tarif_horaire"; // grid column name, same as db field or alias from sql
        $col["width"] = "30"; // width on grid
        $col["edittype"] = "select";
        $col["editoptions"] = array("value"=>$rates); 
        $col["editable"] = true;
        $cols[] = $col; 
        
        $col = array();
        $col["title"] = trans('time_track.comment'); // caption of column, can use HTML tags too
        $col["name"] = "hours_comment"; // grid column name, same as db field or alias from sql
        $col["width"] = "90"; // width on grid
        $col["edittype"] = "textarea"; 
        $col["editoptions"] = array("rows"=>3, "cols"=>40); 
        $col["editable"] = true;
        $cols[] = $col;   
        
        $g->set_columns($cols); 

        $e["on_insert"] = array("saveTime", "App\Http\Controllers\TimeController", false);
        $e["on_update"] = array("updateTime", "App\Http\Controllers\TimeController", false);
        $e["on_delete"] = array("deleteTime", "App\Http\Controllers\TimeController", false);
        $e['js_on_load_complete'] = "grid_onload";

        $g->set_events($e);
        
        // render grid and get html/js output
        $grid = $g->render("list1");


        
    	return view('pages.task.time_track_form', compact('tasks', 'projects', 'grid', 'task', 'project', 'hoursSoFar', 'assignments'));
    }
}
