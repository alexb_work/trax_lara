<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Requests\JobRequest;
use App\Http\Requests\TemplateRequest;
use App\Http\Requests\DeleteRequest;
use App\template;
use App\division;
use App\province;
use App\User;
use App\job_type;
use App\project;
use App\requisition_gibbon;
use Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class JobController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function add()
    {
    	if(Auth::user())
    	$templates = template::all();
    	$divisions = division::pluck('nom_division', 'id_division');
    	$provinces = province::pluck('nom', 'id_province');
    	$personalsRes = User::orderBy('nom_personal')->get()->where('categorie', 1);
    	$personals = array('0' => '---');
    	foreach ($personalsRes as $user){
    		$personals[$user->id_personal] = $user->prenom_personal.' '.$user->nom_personal;
    	}

    	$jobTypes = job_type::all();
    	return view('pages.job.add_job', compact('templates', 'divisions', 'provinces', 'personals', 'jobTypes'));

    }

    public function addWithTemplate(template $project)
    {
    	$templates = template::all();
    	$divisions = division::pluck('nom_division', 'id_division');
    	$provinces = province::pluck('nom', 'id_province');
    	$personalsRes = User::orderBy('nom_personal')->get()->where('categorie', 1);
    	$personals = array('0' => '---');
    	foreach ($personalsRes as $user){
    		$personals[$user->id_personal] = $user->prenom_personal.' '.$user->nom_personal;
    	}

    	$jobTypes = job_type::all();
    	return view('pages.job.add_job', compact('project', 'templates', 'divisions', 'provinces', 'personals', 'jobTypes'));
    }

    public function edit(project $project)
    {
    	$templates = template::all();
    	$divisions = division::pluck('nom_division', 'id_division');
    	$provinces = province::pluck('nom', 'id_province');
    	$personalsRes = User::orderBy('nom_personal')->get()->where('categorie', 1);
    	$personals = array('0' => '---');
    	foreach ($personalsRes as $user){
    		$personals[$user->id_personal] = $user->prenom_personal.' '.$user->nom_personal;
    	}

    	$jobTypes = job_type::all();
    	return view('pages.job.edit_job', compact('templates', 'divisions', 'provinces', 'personals', 'jobTypes', 'project'));
    }

    public function update(JobRequest $request)
    {
    	//dd($request);
    	//dd(Input::file('project_file'));
		$id_project = Input::get("id_project") + 0;
    	$project = project::where('id_project', $id_project)->get();
    	$req = $project[0]->req;
    	$user = Auth::user();
    	$province_chicko = Input::get("id_province");
    	if ($province_chicko == 1) { $province_chicko = "PQ";}
    	switch ( Input::get("project_type") ){
			case 'interne':
				$statut_chicko = 13;
				break;
			case 'externe':
				$statut_chicko = 7;
				break;
			case 'intersite':
				$statut_chicko = 14;
				break;
		}
		$newReqGibbon = requisition_gibbon::where('req_numero', $req)->get();
		$newReqGibbon[0]->statu_ID = $statut_chicko;
		$newReqGibbon[0]->save();

		$newProject = project::find($id_project);
		$newProject = $this->addProjAttributes($newProject);
		$newProject->save();
		if(Input::file('project_file') !== null)
			$this->addFiles($newProject);

		$advisorArray = array();
		foreach(Input::get('advisor') as $key => $value){
			if($value!=0)
				$advisorArray[] = $value;
		}
		$newProject->advisors()->sync($advisorArray);

		
		$newProject->job_types()->sync(Input::get('job_type'));

		$oldSplits = \App\cost_center_split::where('id_project', $id_project)->get();
		$oldNewDiff = count($oldSplits) - count(Input::get('cost_center_multipl'));
		if(Input::get('cost_split') == 'no'){
			$newSplit = new \App\cost_center_split;
			$newSplit->id_project = $id_project;
			$newSplit->cost_center_name = Input::get('cost_center');
			$newSplit->percentage = '100';
			if(count($oldSplits)>1){
				foreach($oldSplits as $oldSplit){
					$oldSplit->delete();
				}
				$newSplit->save();
			}
			else if(!($newSplit->cost_center_name == $oldSplits[0]->cost_center_name && $newSplit->percentage == $oldSplits[0]->percentage)){
				$oldSplit[0]->cost_center_name = $newSplit->cost_center_name;
				$oldSplit[0]->percentage = $newSplit->percentage;
				$oldSplit[0]->save();
			}
			
		}
		else if($oldNewDiff == 0){
			foreach($oldSplits as $key => $oldSplit){
				$oldSplit->id_project = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];;
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
			}
		}
		else if($oldNewDiff > 0){
			for($i=0; $i<$oldNewDiff; $i++){
				$oldSplits[$i]->delete();
				unset($oldSplits[$i]);
            }
            $oldSplits2 = array_values($oldSplits);
			
			foreach($oldSplits2 as $key => $oldSplit){
				$oldSplit->id_project = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];;
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
			}

		}
		else if($oldNewDiff < 0){
			$count = 0;
			foreach($oldSplits as $key => $oldSplit){
				$oldSplit->id_project = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
				$count++;
			}
			for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

				$newSplit = new \App\cost_center_split;
				$newSplit->id_project = $newProject->id_project;
				$newSplit->cost_center_name = Input::get('cost_center_multipl')[$i];
				$newSplit->percentage = Input::get('percent_center_multipl')[$i];
				$newSplit->save();
			}

		}

		return redirect()->action('HomeController@index');
    }

    public function storeType(JobRequest $request){
    	if(Input::get('store')){
    		$this->store($request);
    	}
    	else if(Input::get('templateSub')){
    		$this->storeTemplate($request);
    	}
    	return redirect()->action('HomeController@index');
    }

    public function store(JobRequest $request){
    	$user = Auth::user();
    	$province_chicko = Input::get("id_province");
    	if ($province_chicko == 1) { $province_chicko = "PQ";}
    	switch ( Input::get("project_type") ){
			case 'interne':
				$statut_chicko = 13;
				break;
			case 'externe':
				$statut_chicko = 7;
				break;
			case 'intersite':
				$statut_chicko = 14;
				break;

		}
			$maxReq = requisition_gibbon::max('req_ID');
			$newReq = $maxReq + 1;
			$requisition = strtoupper($user->initiales)."-M-".str_pad($newReq, 6, "0", STR_PAD_LEFT);

			$newReqGibbon = new requisition_gibbon;
			$newReqGibbon->req_numero = $requisition;
			$newReqGibbon->req_nom = Input::get('name_client');
			$newReqGibbon->req_tel = Input::get('client_phone');
			$newReqGibbon->req_exige_le = Input::get('end_date');
			$newReqGibbon->projet_nom = Input::get('project_name');
			$newReqGibbon->statu_ID = $statut_chicko;
			$newReqGibbon->req_info = Input::get('project_desc');
			$newReqGibbon->fact_cout = Input::get('cost_center');
			$newReqGibbon->fact_province = $province_chicko;
			$newReqGibbon->req_exige_heure = Input::get('end_time');
			$newReqGibbon->fact_code = Input::get('project_code');
			$newReqGibbon->req_recu_le = Input::get('req_date');

			$newReqGibbon->save();
		
		
		$newProject = new project;
		$newProject->req = $requisition;
		$newProject->date_project = date('Y-m-d H:i:s');
		$newProject = $this->addProjAttributes($newProject);
		$newProject->save();

		
		$this->addFiles($newProject);

		$advisorArray = array();
		foreach(Input::get('advisor') as $key => $value){
			if($value!=0)
				$advisorArray[] = $value;
		}
		$newProject->advisors()->sync($advisorArray);
		if(null !== Input::get('job_type')){
			$newProject->job_types()->sync(Input::get('job_type'));
		}
		else
			$newProject->job_types()->sync(array());

		if(Input::get('cost_split') == 'no'){
			$newSplit = new \App\cost_center_split;
			$newSplit->id_project = $newProject->id_project;
			$newSplit->cost_center_name = Input::get('cost_center');
			$newSplit->percentage = 100;
			$newSplit->save();
		}
		else{
			foreach(Input::get('cost_center_multipl') as $key => $value){
				$newSplit = new \App\cost_center_split;
				$newSplit->id_project = $newProject->id_project;
				$newSplit->cost_center_name = $value;
				$newSplit->percentage = Input::get('percent_center_multipl')[$key];
				$newSplit->save();
			}
		}

		return redirect()->action('HomeController@index');



    }

    public function storeTemplate(TemplateRequest $request)
    {
    	$templates = template::all();
    	foreach($templates as $template){
    		if(Input::get('project_name') == $template->project_name){
    			return $this->updateTemplate($request, $template->id_project);

    		}
    	}
    	
    	$user = Auth::user();

    	$newProject = new template;
		$newProject = $this->addProjAttributes($newProject);

		$newProject->date_project = date('Y-m-d H:i:s');
		
		$newProject->save();

		$advisorArray = array();
		foreach(Input::get('advisor') as $key => $value){
			if($value!=0)
				$advisorArray[] = $value;
		}
		$newProject->advisors()->sync($advisorArray);

		if(Input::get('job_type')!=null)
			$newProject->job_types()->sync(Input::get('job_type'));

		if(Input::get('cost_split') == 'no'){
			if(Input::get('cost_center') != ""){
			$newSplit = new \App\cost_center_split;
			$newSplit->id_template = $newProject->id_project;
			$newSplit->cost_center_name = Input::get('cost_center');
			$newSplit->percentage = 100;
			$newSplit->save();
			}
		}
		else{
			foreach(Input::get('cost_center_multipl') as $key => $value){
				$newSplit = new \App\cost_center_split;
				$newSplit->id_project = $newProject->id_project;
				$newSplit->cost_center_name = $value;
				$newSplit->percentage = Input::get('percent_center_multipl')[$key];
				$newSplit->save();
			}
		}
		return redirect()->action('HomeController@index');
    }

    public function updateTemplate(TemplateRequest $request, $id_project)
    {
    	$project = template::where('id_project', $id_project)->get();
    	$req = $project[0]->req;
    	$user = Auth::user();

		$newProject = template::find($id_project);
		$newProject = $this->addProjAttributes($newProject);
		$newProject->save();

		$this->addFiles($newProject);

		$advisorArray = array();
		foreach(Input::get('advisor') as $key => $value){
			if($value!=0)
				$advisorArray[] = $value;
		}
		$newProject->advisors()->sync($advisorArray);

		if(Input::get('job_type')!=null)
			$newProject->job_types()->sync(Input::get('job_type'));

		$oldSplits = \App\cost_center_split::where('id_template', $id_project)->get();
		$oldNewDiff = count($oldSplits) - count(Input::get('cost_center_multipl'));
		if(count(Input::get('project_file')) == 0){
            if(count($oldSplits)>=1){
                foreach($oldSplits as $oldSplit){
                    $oldSplit->delete();
                }
            }
        }
		else if(Input::get('cost_split') == 'no'){
			$newSplit = new \App\cost_center_split;
			$newSplit->id_template = $id_project;
			$newSplit->cost_center_name = Input::get('cost_center');
			$newSplit->percentage = '100';
			if(count($oldSplits)>1){
				foreach($oldSplits as $oldSplit){
					$oldSplit->delete();
				}
				$newSplit->save();
			}
			else if(!($newSplit->cost_center_name == $oldSplits[0]->cost_center_name && $newSplit->percentage == $oldSplits[0]->percentage)){
				$oldSplit[0]->cost_center_name = $newSplit->cost_center_name;
				$oldSplit[0]->percentage = $newSplit->percentage;
				$oldSplit[0]->save();
			}
			
		}
		else if($oldNewDiff == 0){
			foreach($oldSplits as $key => $oldSplit){
				$oldSplit->id_template = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];;
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
			}
		}
		else if($oldNewDiff > 0){
			for($i=0; $i<$oldNewDiff; $i++){
				$oldSplits[$i]->delete();
				unset($oldSplits[$i]);
            }
            $oldSplits2 = array_values($oldSplits);
			
			foreach($oldSplits2 as $key => $oldSplit){
				$oldSplit->id_template = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];;
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
			}

		}
		else if($oldNewDiff < 0){
			$count = 0;
			foreach($oldSplits as $key => $oldSplit){
				$oldSplit->id_template = $id_project;
				$oldSplit->cost_center_name = Input::get('cost_center_multipl')[$key];
				$oldSplit->percentage = Input::get('percent_center_multipl')[$key];

				$oldSplit->save();
				$count++;
			}
			for($i=$count; $i<abs($oldNewDiff)+$count; $i++){

				$newSplit = new \App\cost_center_split;
				$newSplit->id_template = $newProject->id_project;
				$newSplit->cost_center_name = Input::get('cost_center_multipl')[$i];
				$newSplit->percentage = Input::get('percent_center_multipl')[$i];
				$newSplit->save();
			}

		}

		return redirect()->action('HomeController@index');
    }

    public function updateStatus(Request $request)
    {
    	$newProject = project::find(Input::get('idproject'));
    	$newProject->id_status = Input::get('status');
    	$newProject->last_change = date('Y-m-d H:i:s');
    	$newProject->save();
    	$color = "#000000";
    	if($newProject->id_status == -1){$color='#CC0033';}
		if($newProject->id_status == 1 ){$color='#00CC00';}
		if($newProject->id_status == 0 ){$color='#2890D5';}
		$returnHTML = view('partials.status')->with('color', $color)->with('newProject', $newProject)->render();
		return $returnHTML;
    }

    public function deleteJob(DeleteRequest $request){

    	$project = project::find(Input::get('idproject'));
    	$project->delete();
    }

    private function addFiles($newProject){

			foreach(Input::file('project_file') as $key=>$file){
				//nom du fichier choisi:
				$nomFichier = $file->getClientOriginalName();
				//extension
				$extension = $file->getClientOriginalExtension();
				$newFile = new \App\file_upload;
				$newFile->id_file_upload = uniqid();
				$newFile->original_filename = $nomFichier;
				$newFile->id_project = $newProject->id_project;
				$newFile->extension = $extension;
				$newFile->file_desc = Input::get('file_desc')[$key];
				$fullFileName = $newFile->id_file_upload .'.'.$extension;
				$newFile->save();
				Storage::disk('local')->put($fullFileName, File::get($file));

			}
    }

    private function addProjAttributes($newProject){
    	$newProject->project_name = Input::get('project_name');
		$newProject->last_change = date('Y-m-d H:i:s');
		$newProject->id_division = Input::get('id_division');
		$newProject->id_province = Input::get('id_province');
		$newProject->end_date = Input::get('end_date');
		$newProject->end_time = Input::get('end_time');
		$newProject->delivery_date= Input::get('delivery_date');
		$newProject->req_date = Input::get('req_date');
		$newProject->requested_type = Input::get('requested_type');
		$newProject->project_type = Input::get('project_type');
		$newProject->project_desc = Input::get('project_desc');
		$newProject->project_current_situation = Input::get('project_current_situation');
		$newProject->project_target = Input::get('project_target');
		$newProject->project_objectives = Input::get('project_objectives');
		$newProject->project_code = Input::get('project_code');
		$newProject->network_act = Input::get('network_act');
		$newProject->gl = Input::get('gl');
		$newProject->name_client = Input::get('name_client');
		$newProject->client_phone = Input::get('client_phone');
		$newProject->client_address = Input::get('client_address');
		$newProject->client_other = Input::get('other');
		if(Input::get('other_job_type') == '1'){
			$newProject->other_job_type = Input::get('text_other_job_type');
		}
		else{
			$newProject->other_job_type = null;
		}
		return $newProject;
    }

    
}
