<?php
use Illuminate\Support\Facades\Input;


Route::group(['middleware' => ['web']], function () {

	Route::group(['prefix' => LaravelLocalization::setLocale()], function()
    {

    	//Authentication routes
		Route::controllers([
			'auth' 		=> 'Auth\AuthController',
			'password' 	=> 'Auth\PasswordController',
		]);
		Route::auth();
		Route::get('/logout', ['as'=>'logout', 'uses'=>'Auth\AuthController@logout']);


		//home page
		Route::get('/home', ['as'=>'home', 'uses'=>'HomeController@index']);
		Route::get('/', ['as'=>'home2', 'uses'=>'HomeController@index']);
		Route::get('/home/all', ['as'=>'all', 'uses'=>'HomeController@all']);
		Route::get('/home/archive', ['as'=>'archives', 'middleware' => 'authAdmin', 'uses'=>'HomeController@archives']);

		//download route
		Route::get('download/{filename}', function ($filename)
		{
    		$file = storage_path('app') . '/' . $filename; // or wherever you have stored your PDF files
   			return response()->download($file);
		});

		Route::post('job/store', function() { 
			if(Input::get('store')) { 
				$action = 'store'; 
			} 
			elseif(Input::get('templateSub')) { 
				$action = 'storeTemplate'; 
			} return App::make('App\Http\Controllers\JobController')->$action(); 
		});

		//job routes
		Route::get('/job/add', ['as'=>'addJob', 'middleware' => 'authAdmin', 'uses'=>'JobController@add']);
		Route::get('/job/add/{project}', ['as'=>'addWithTemplate', 'middleware' => 'authAdmin', 'uses'=>'JobController@addWithTemplate']);
		Route::post('/job/storeType', ['as'=>'storeType', 'middleware' => 'authAdmin', 'uses'=>'JobController@storeType']);
		Route::post('/job/storeJob', ['as'=>'storeJob', 'middleware' => 'authAdmin', 'uses'=>'JobController@store']);
		Route::post('/job/storeTemplate', ['as'=>'storeTemplate', 'middleware' => 'authAdmin', 'uses'=>'JobController@storeTemplate']);
		Route::post('/job/update', ['as'=>'updateJob', 'middleware' => 'authAdmin', 'uses'=>'JobController@update']);
		Route::get('/job/{project}/edit', ['as'=>'editJob', 'uses'=>'JobController@edit']);
		Route::post('/job/updateStatus', ['as'=>'updateStatus', 'middleware' => 'authAdmin', 'uses'=>'JobController@updateStatus']);
		Route::post('/job/delete', ['as'=>'deleteJob', 'middleware' => 'authAdmin', 'uses'=>'JobController@deleteJob']);

		//task routes
		Route::post('/task/time/{task}', ['as'=>'time', 'uses'=>'TimeController@timeForm']);
		Route::get('/task/time/{task}', ['as'=>'time', 'uses'=>'TimeController@timeForm']);
		Route::get('/task/{project}/add', ['as'=>'addTask', 'uses'=>'TaskController@add']);
		Route::get('/task/{task}/edit', ['as'=>'editTask', 'uses'=>'TaskController@edit']);
		Route::get('/task/{project}', ['as'=>'taskList', 'uses'=>'TaskController@userList']);
		Route::get('/task/{project}/all', ['as'=>'taskListAll', 'uses'=>'TaskController@index']);
		Route::post('/task/store', ['as'=>'storeTask', 'uses'=>'TaskController@store']);
		Route::post('/task/update', ['as'=>'updateTask', 'uses'=>'TaskController@update']);
		Route::post('/task/saveTime', ['as'=>'saveTime', 'uses'=>'TimeController@saveTime']);
		Route::post('/task/complete', ['as'=>'completeTask', 'uses'=>'TaskController@complete']);

		//estimate routes
		Route::get('/estimate/{project}', ['as'=>'estimateList', 'uses'=>'EstimateController@index']);
		Route::get('/estimate/{project}/add', ['as'=>'addEstimate', 'uses'=>'EstimateController@add']);
		Route::get('/estimate/{estimate}/edit', ['as'=>'editEstimate', 'uses'=>'EstimateController@edit']);
		Route::post('/estimate/store', ['as'=>'storeEstimate', 'uses'=>'EstimateController@store']);
		Route::post('/estimate/update', ['as'=>'updateEstimate', 'uses'=>'EstimateController@update']);
		Route::post('/estimate/updateStatus', ['as'=>'updateEstimateStatus', 'middleware' => 'authAdmin', 'uses'=>'EstimateController@updateStatus']);
		Route::post('/estimate/rejectionMessage', ['as'=>'rejectionMessage', 'middleware' => 'authAdmin', 'uses'=>'EstimateController@setRejection']);
		Route::get('/estimate/{estimate}/view', ['as'=>'viewEstimate', 'uses'=>'EstimateController@generatePDF']);
		Route::get('/estimate/{print_job}/prices', ['as'=>'viewPrintPrices', 'uses'=>'EstimateController@displayPrices']);

		//print routes
		Route::get('/print/{project}', ['as'=>'printForm', 'middleware' => 'authAdmin', 'uses'=>'PrintController@index']);
		Route::post('/print/store', ['as'=>'storePrint', 'middleware' => 'authAdmin', 'uses'=>'PrintController@store']);
		Route::post('/print/update', ['as'=>'updatePrint', 'middleware' => 'authAdmin', 'uses'=>'PrintController@update']);
		Route::post('/print/prices', ['as'=>'displayPrices', 'middleware' => 'authAdmin', 'uses'=>'PrintController@priceDisplay']);


	});
});

