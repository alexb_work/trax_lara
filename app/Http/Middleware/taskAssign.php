<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\task;

class taskAssign
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $task = $request->task;
        $assignments = $task->assignments;
        foreach($assignments as $assign){
            if(Auth::user()->id_personal == $assign->id_assigned_person)
                return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
