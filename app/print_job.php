<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class print_job extends Model
{
    protected $table = 'print_job';
    protected $primaryKey = 'id_print_job';
    public $timestamps = false;

    protected $dates = ['p_received_date', 'p_due_date', 'p_due_time', 'p_start_date', 'p_start_time', 'p_work_date', 'p_work_time'];

    public function project(){
    	return $this->belongsTo('\App\project', 'id_project', 'id_project');
    }

    public function province(){
        return $this->belongsTo('\App\province', 'id_province', 'id_province');
    }

    public function finition(){
        return $this->belongsTo('\App\finition', 'id_print_job', 'id_print_job');
    }

    public function delivery_types(){
        return $this->belongsToMany('\App\delivery_type', "print_delivery", "id_print_job", "id_delivery_type");
    }

    public function reprographies(){
        return $this->hasMany('\App\reprography', 'id_print_job', 'id_print_job');
    }

    public function outsourcings(){
        return $this->hasMany('\App\outsourcing', 'id_print_job', 'id_print_job');
    }

    public function files(){
        return $this->hasMany('\App\file_upload', 'id_print_job', 'id_print_job');
    }

    public function supplies(){
        return $this->hasMany('\App\print_job_supplies', 'id_print_job', 'id_print_job');
    }

    public function getPReceivedDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setPReceivedDateAttribute($value){
        $this->attributes['p_received_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getPDueDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setPDueDateAttribute($value){
        $this->attributes['p_due_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getPDueTimeAttribute($value){
        return date("H:i", strtotime($value));
    }

    public function setPDueTimeAttribute($value){
        $this->attributes['p_due_time'] = Carbon::createFromFormat('H:i', $value);
    }

    public function getPStartDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setPStartDateAttribute($value){
        $this->attributes['p_start_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getPStartTimeAttribute($value){
        return date("H:i", strtotime($value));
    }

    public function setPStartTimeAttribute($value){
        $this->attributes['p_start_time'] = Carbon::createFromFormat('H:i', $value);
    }

    public function getPWorkDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setPWorkDateAttribute($value){
        $this->attributes['p_work_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getPWorkTimeAttribute($value){
        return date("H:i", strtotime($value));
    }

    public function setPWorkTimeAttribute($value){
        $this->attributes['p_work_time'] = Carbon::createFromFormat('H:i', $value);
    }
}
