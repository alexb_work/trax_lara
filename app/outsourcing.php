<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class outsourcing extends Model
{
    protected $table = 'outsourcing';
    protected $primaryKey = 'id_outsourcing';
    public $timestamps = false;

    public function print_job(){
        return $this->belongsTo('\App\print_job', 'id_print_job', 'id_print_job');
    }

    public function submissions(){
    	return $this->hasMany('\App\print_management_submission', 'id_outsourcing', 'id_outsourcing');
    }

}
