<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class template extends Model
{
    protected $table = 'template';
    protected $primaryKey = 'id_project';
    public $timestamps = false;

    protected $dates = ['date_project', 'last_change', 'end_date', 'end_time', 'req_date'];

    public function job_types(){
        return $this->belongsToMany('\App\job_type', "job_type_template", "id_template", "id_job_type");
    }

    public function advisors(){
        return $this->belongsToMany('\App\User', 'advisor_template', 'id_template', 'id_advisor');
    }

    public function cost_centers(){
        return $this->hasMany('\App\cost_center_split', 'id_template', 'id_project');
    }

    public function province(){
        return $this->belongsTo('\App\province', 'id_province', 'id_province');
    }

    public function division(){
        return $this->belongsTo('\App\division', 'id_division', 'id_division');
    }

    public function getEndDateAttribute($value){
        if($value == null)
            return "";
        else
            return date("m/d/Y", strtotime($value));
    }

    public function setEndDateAttribute($value){
        if($value == "")
            $this->attributes['end_date'] = null;
        else
            $this->attributes['end_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getEndTimeAttribute($value){
        if($value == null)
            return "";
        else
            return date("H:i", strtotime($value));
    }

    public function setEndTimeAttribute($value){
        if($value == "")
            $this->attributes['end_time'] = null;
        else
            $this->attributes['end_time'] = Carbon::createFromFormat('H:i', $value);
    }

    public function getReqDateAttribute($value){
        if($value == null)
            return "";
        else
            return date("Y-m-d", strtotime($value));
    }

    public function setReqDateAttribute($value){
        if($value == "")
            $this->attributes['req_date'] = null;
        else
            $this->attributes['req_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }
}
