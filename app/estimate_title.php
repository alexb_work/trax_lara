<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estimate_title extends Model
{
    protected $table = 'estimate_title';
    protected $primaryKey = 'id_estimate_title';
    public $timestamps = false;

    public function estimate(){
        return $this->hasMany('\App\estimate', 'id_estimate', 'id_estimate');
    }
}
