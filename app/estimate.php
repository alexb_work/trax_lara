<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class estimate extends Model
{
    protected $table = 'estimate';
    protected $primaryKey = 'id_estimate';
    public $timestamps = false;

    protected $dates = ['date'];

    public function project(){
    	return $this->belongsTo('\App\project', 'id_project', 'id_project');
    }

    public function status(){
    	return $this->belongsTo('\App\estimate_status', 'validate', 'id_estimate_status');
    }

    public function rejection(){
        return $this->belongsTo('\App\estimate_rejection', 'id_rejection', 'id_rejection');
    }

    public function titles(){
        return $this->hasMany('\App\estimate_title', 'id_estimate', 'id_estimate');
    }

    public function getDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setDateAttribute($value){
        $this->attributes['date'] = Carbon::createFromFormat('Y-m-d', $value);
    }
}
