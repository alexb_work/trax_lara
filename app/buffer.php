<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buffer extends Model
{
    protected $table = 'estimate_cost_buffer';
    protected $primaryKey = 'buffer_id';
    public $timestamps = false;
}
