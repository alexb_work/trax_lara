<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assigned_task extends Model
{
    protected $table = 'assigned_task';
    protected $primaryKey = 'id_assigned_task';
    public $timestamps = false;

    public function user(){
    	return $this->belongsTo('\App\User', 'id_assigned_person', 'id_personal');
    }

    public function task(){
    	return $this->belongsTo('\App\task', 'id_task', 'id_tasks');
    }

    public function category(){
    	return $this->belongsTo('\App\categorie_estimate', 'id_estimate_cateorie', 'id_categorie_estimate');
    }
}
