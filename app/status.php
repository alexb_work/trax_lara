<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class status extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id_status';
    public $timestamps = false;

    public function projects(){
    	return $this->hasMany('\App\project', 'id_status', 'id_status');
    }
}
