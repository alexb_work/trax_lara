<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reprography extends Model
{
    protected $table = 'reprography';
    protected $primaryKey = 'id_repro';
    public $timestamps = false;

    public function print_job(){
        return $this->belongsTo('\App\print_job', 'id_print_job', 'id_print_job');
    }

    public function finition(){
        return $this->belongsTo('\App\finition', 'id_repro', 'id_repro');
    }

    public function paper(){
        return $this->belongsTo('\App\paper', 'supply_code', 'supply_code');
    }

    public function color_type(){
    	return $this->belongsTo('\App\color_type', 'id_color_type', 'id_color_type');
    }
}
