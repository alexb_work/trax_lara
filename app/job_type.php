<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class job_type extends Model
{
    protected $table = 'job_type';
    protected $primaryKey = 'id_job_type';
    public $timestamps = false;

    public function projects(){
    	return $this->belongsToMany('\App\project', "job_type_project", "id_job_type", "id_project");
    }

    public function templates(){
    	return $this->belongsToMany('\App\template', "job_type_template", "id_job_type", "id_template");
    }


}
