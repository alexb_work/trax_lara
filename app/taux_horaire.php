<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taux_horaire extends Model
{
    protected $table = 'taux_horaire';
    protected $primaryKey = 'id_taux_horaire';
    public $timestamps = false;

    public function hours(){
        return $this->hasMany('\App\hours_per_task', 'id_taux_horaire', 'id_taux_horaire');
    }
}
