<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class hours_per_task extends Model
{
    protected $table = 'hours_per_task';
    protected $primaryKey = 'id_hours_per_task';
    public $timestamps = false;

    protected $dates = ['date_completed'];

    public function task(){
    	return $this->belongsTo('\App\Task', 'id_task', 'id_hours_per_task');
    }

    public function User(){
    	return $this->belongsTo('\App\User', 'id_personal', 'id_hours_per_task');
    }

	public function categorieEstimate(){
    	return $this->belongsTo('\App\categorie_estimate', 'id_categorie_estimate', 'id_hours_per_task');
    }

    public function tauxHoraire(){
    	return $this->belongsTo('\App\taux_horaire', 'id_taux_horaire', 'id_hours_per_task');
    }

    public function getDateCompletedAttribute($value){
        return date("Y-m-d", strtotime($value));
    }
}
