<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requisition_gibbon extends Model
{
    protected $table = 'requisition';
    protected $primaryKey = 'req_id';
    public $timestamps = false;
    protected $connection = 'mysql2';

    protected $fillable = [
     				'req_numero',
     				'req_nom',
     				'req_tel',
     				'req_exige_le',
     				'projet_nom',
     				'statu_ID',
     				'req_info',
     				'fact_cout',
     				'fact_province',
     				'req_exige_heure',
     				'fact_code',
     				'req_recu_le'

	];
}
