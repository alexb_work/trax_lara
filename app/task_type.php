<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task_type extends Model
{
    protected $table = 'task_type';
    protected $primaryKey = 'id_task_type';
    public $timestamps = false;

    public function tasks(){
    	return $this->hasMany('\App\task', "id_task_type", "id_task_type");
    }

}
