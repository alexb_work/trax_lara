<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class services_list extends Model
{
    protected $table = 'services_list';
    protected $primaryKey = 'id_services_list';
    public $timestamps = false;
}
