<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class outsource_type extends Model
{
    protected $table = 'outsource_type';
    protected $primaryKey = 'id_outsource_type';
    public $timestamps = false;
}
