<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class print_category_prices extends Model
{
    protected $table = 'print_category_prices';
    protected $primaryKey = 'id_print_category_prices';
    public $timestamps = false;

    public function print_category(){
        return $this->hasOne('\App\print_category', 'id_print_category', 'id_print_category');
    }
}
