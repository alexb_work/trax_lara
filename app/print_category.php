<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class print_category extends Model
{
    protected $table = 'print_category';
    protected $primaryKey = 'id_print_category';
    public $timestamps = false;

    public function print_category_price(){
        return $this->belongsTo('\App\print_category_price', 'id_print_category', 'id_print_category');
    }

    public function color(){
    	return $this->belongsTo('\App\color_type', 'id_color_type', 'id_color_type');
    }
}
