<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class province extends Model
{
    protected $table = 'province';
    protected $primaryKey = 'id_province';
    public $timestamps = false;

    public function projects(){
    	return $this->hasMany('\App\project', 'id_province', 'id_province');
    }

    public function print_jobs(){
    	return $this->hasMany('\App\print_job', 'id_province', 'id_province');
    }
}
