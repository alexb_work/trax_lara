<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class version extends Model
{
    protected $table = 'version';
    protected $primaryKey = 'id_version';
    public $timestamps = false;
}
