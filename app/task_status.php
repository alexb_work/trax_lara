<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task_status extends Model
{
    protected $table = 'task_status';
    protected $primaryKey = 'id_task_status';
    public $timestamps = false;
}
