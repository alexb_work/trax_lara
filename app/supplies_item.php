<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplies_item extends Model
{
    protected $table = 'supplies_item';
    protected $primaryKey = 'supply_code';
    public $timestamps = false;
    public $incrementing = false;

    public function print_jobs(){
        return $this->hasMany('\App\print_job_supplies', 'supply_code', 'supply_code');
    }
}
