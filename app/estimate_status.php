<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estimate_status extends Model
{
    protected $table = 'estimate_status';
    protected $primaryKey = 'id_estimate_status';
    public $timestamps = false;

    public function estimate(){
    	return $this->hasMany('\App\estimate', 'validate', 'id_estimate_status');
    }
}
