<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stapled_type extends Model
{
    protected $table = 'stapled_type';
    protected $primaryKey = 'id_stapled_type';
    public $timestamps = false;

    public function finition(){
        return $this->belongsTo('\App\finition', 'id_print_job', 'id_print_job');
    }
}
