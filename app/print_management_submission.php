<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class print_management_submission extends Model
{
    protected $table = 'print_management_submission';
    protected $primaryKey = 'id_print_management_submission';
    public $timestamps = false;

    public function outsourcing(){
        return $this->belongsTo('\App\outsourcing', 'id_outsourcing', 'id_outsourcing');
    }

    public function files(){
        return $this->hasMany('\App\file_upload', 'id_print_management_submission', 'id_print_management_submission');
    }

}
