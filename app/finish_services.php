<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class finish_services extends Model
{
    protected $table = 'finish_services';
    protected $primaryKey = 'supply_code';
    public $timestamps = false;
    public $incrementing = false;

}
