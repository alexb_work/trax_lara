<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class finish_prices extends Model
{
    protected $table = 'finish_prices';
    protected $primaryKey = 'id_finish_prices';
    public $timestamps = false;

    public function finish_services(){
        return $this->hasMany('\App\supplies_item', 'supply_code', 'supply_code');
    }
}
