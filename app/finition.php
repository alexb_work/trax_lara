<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class finition extends Model
{
    protected $table = 'finition';
    protected $primaryKey = 'id_finition';
    public $timestamps = false;

    public function print_job(){
        return $this->hasOne('\App\print_job', 'id_print_job', 'id_print_job');
    }

    public function reprography(){
        return $this->hasOne('\App\reprography', 'id_repro', 'id_repro');
    }

    public function stapled_type(){
        return $this->belongsTo('\App\stapled_type', 'id_stapled_type', 'id_stapled_type');
    }

}
