<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cost_center_split extends Model
{
    protected $table = 'cost_center_split';
    protected $primaryKey = 'id_cost_center_split';
    public $timestamps = false;

    public function project(){
    	return $this->belongsTo('\App\project', 'id_project', 'id_cost_center_split');
    }
}
