<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class delivery_type extends Model
{
    protected $table = 'delivery_type';
    protected $primaryKey = 'id_delivery_type';
    public $timestamps = false;

    public function print_jobs(){
    	return $this->belongsToMany('\App\print_job', "print_delivery", "id_delivery_type", "id_print_job");
    }
}
