<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class color_type extends Model
{
    protected $table = 'color_type';
    protected $primaryKey = 'id_color_type';
    public $timestamps = false;

    public function reprographies(){
        return $this->hasMany('\App\reprography', 'id_color_type', 'id_color_type');
    }

    public function print_category(){
    	return $this->hasMany('\App\print_category', 'id_color_type', 'id_color_type');
    }
}
