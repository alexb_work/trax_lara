<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paper extends Model
{
    protected $table = 'paper';
    protected $primaryKey = 'supply_code';
    public $timestamps = false;
    public $incrementing = false;

    public function reprographies(){
        return $this->hasMany('\App\reprography', 'supply_code', 'supply_code');
    }
}
