<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplies_price extends Model
{
    protected $table = 'supplies_price';
    protected $primaryKey = 'id_supply_prices';
    public $timestamps = false;

    public function supplies_item(){
        return $this->hasMany('\App\supplies_item', 'supply_code', 'supply_code');
    }
}
