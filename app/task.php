<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class task extends Model
{
    protected $table = 'tasks';
    protected $primaryKey = 'id_tasks';
    public $timestamps = false;

    protected $dates = ['last_change', 'req_date'];

    public function hours(){
        return $this->hasMany('\App\hours_per_task', 'id_task', 'id_tasks');
    }

     public function file_uploads(){
        return $this->hasMany('\App\file_upload', 'id_task', 'id_tasks');
    }

    public function assignments(){
        return $this->hasMany('\App\assigned_task', 'id_task', 'id_tasks');
    }

    public function project(){
        return $this->belongsTo('\App\project', 'id_project', 'id_project');
    }

    public function taskStatus(){
        return $this->belongsTo('\App\task_status', 'id_task_status', 'id_task_status');
    }

    public function taskType(){
        return $this->belongsTo('\App\task_type', 'id_task_type', 'id_task_type');
    }

    public function editor(){
        return $this->belongsTo('\App\User', 'editing_by', 'id_personal');
    }

    public function getReqDateAttribute($value){
        return date("Y-m-d", strtotime($value));
    }

    public function setReqDateAttribute($value){
        $this->attributes['req_date'] = Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getLastChangeAttribute($value){
        return date("Y-m-d", strtotime($value));
    }
}
