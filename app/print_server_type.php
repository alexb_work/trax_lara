<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class print_server_type extends Model
{
    protected $table = 'print_server_type';
    protected $primaryKey = 'id_print_server_type';
    public $timestamps = false;
}
