<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class file_upload extends Model
{
    protected $table = 'file_upload';
    protected $primaryKey = 'id_file_upload';
    public $timestamps = false;

    public function project(){
        return $this->belongsTo('\App\project', 'id_project', 'id_project');
    }

    public function task(){
        return $this->belongsTo('\App\task', 'id_task', 'id_tasks');
    }

    public function print_job(){
        return $this->belongsTo('\App\print_job', 'id_print_job', 'id_print_job');
    }

    public function submission(){
        return $this->belongsTo('\App\print_management_submission', 'id_print_management_submission', 'id_print_management_submission');
    }

    public function getIdFileUploadAttribute($value){
    	return $value;
    }
}
