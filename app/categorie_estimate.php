<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categorie_estimate extends Model
{
    protected $table = 'categorie_estimate';
    protected $primaryKey = 'id_categorie_estimate';
    protected $orderBy = 'title_en';
    public $timestamps = false;

    public function hours(){
        return $this->hasMany('\App\hours_per_task', 'id_categorie_estimate', 'id_categorie_estimate');
    }

    public function users(){
    	return $this->hasMany('\App\User', 'id_personal', 'id_personal');
    }

    public function assigned_tasks(){
    	return $this->hasMany('\App\assigned_task', 'id_estimate_categorie', 'id_categorie_estimate');
    }

}
