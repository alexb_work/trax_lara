curmonth = -1;
curyear = -1;
calTarget = "";
var Calendar;
var calEvent;
var allowingPast = false;

function getPos( oElement )
{
	var iReturnValue = new Array (0, 0);
	
	while( oElement != null ) {
		iReturnValue[0] += oElement.offsetTop;
		iReturnValue[1] += oElement.offsetLeft;
		oElement = oElement.offsetParent;
	}
	return iReturnValue;
}

function selectDate(day, targetid) {
	var result = "";
	document.body.removeChild(Calendar);
	Calendar = undefined;
	/*
	if (day < 10)
		document.getElementById ("jour"+targetid).value = "0"+day;
	else
		document.getElementById ("jour"+targetid).value = day;
	if (curmonth < 9)
		document.getElementById ("mois"+targetid).value = "0"+(curmonth+1);
	else
		document.getElementById ("mois"+targetid).value = curmonth+1;
	document.getElementById ("annee"+targetid).value = curyear;*/	
	result += "" + curyear + "-"; 
	
	if (curmonth < 9)
		result += "0"+(parseInt(curmonth)+1);
	else
		result += ""+(parseInt(curmonth)+1);
		
	result += "-";
	
	if (day < 10)
		result += "0"+day;
	else
		result += ""+day;
	
	document.getElementById (targetid).value = result;
	
	if (calEvent)
		eval (calEvent);
}

function getMonthName (month) {
	switch (month) {
		case 1:
			return "January";
		case 2:
			return "February";
		case 3:
			return "March";
		case 4:
			return "April";
		case 5:
			return "May";
		case 6:
			return "June";
		case 7:
			return "July";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "October";
		case 11:
			return "November";
		case 12:
			return "December";
		default:
			return "Wrong month";
	}
}

function getYearDropdown() {
	return '<select id="yeardrop" onchange="javascript:setCuryear();return true"> \
	<option value="2000" '+(curyear!=2000?'':'selected="selected"')+'>2000</option> \
	<option value="2001" '+(curyear!=2001?'':'selected="selected"')+'>2001</option> \
	<option value="2002" '+(curyear!=2002?'':'selected="selected"')+'>2002</option> \
	<option value="2003" '+(curyear!=2003?'':'selected="selected"')+'>2003</option> \
	<option value="2004" '+(curyear!=2004?'':'selected="selected"')+'>2004</option> \
	<option value="2005" '+(curyear!=2005?'':'selected="selected"')+'>2005</option> \
	<option value="2006" '+(curyear!=2006?'':'selected="selected"')+'>2006</option> \
	<option value="2007" '+(curyear!=2007?'':'selected="selected"')+'>2007</option> \
	<option value="2008" '+(curyear!=2008?'':'selected="selected"')+'>2008</option> \
	<option value="2009" '+(curyear!=2009?'':'selected="selected"')+'>2009</option> \
	<option value="2010" '+(curyear!=2010?'':'selected="selected"')+'>2010</option> \
	<option value="2011" '+(curyear!=2011?'':'selected="selected"')+'>2011</option> \
	<option value="2012" '+(curyear!=2012?'':'selected="selected"')+'>2012</option> \
	<option value="2013" '+(curyear!=2013?'':'selected="selected"')+'>2013</option> \
	<option value="2014" '+(curyear!=2014?'':'selected="selected"')+'>2014</option> \
	<option value="2015" '+(curyear!=2015?'':'selected="selected"')+'>2015</option> \
	</select>';
}

function getMonthDropdown() {
	
	return '<select id="monthdrop" onchange="javascript:setCurmonth(); return true"> \
	<option value="0" '+(curmonth!=0?'':'selected="selected"')+'>January</option> \
	<option value="1" '+(curmonth!=1?'':'selected="selected"')+'>February</option> \
	<option value="2" '+(curmonth!=2?'':'selected="selected"')+'>March</option> \
	<option value="3" '+(curmonth!=3?'':'selected="selected"')+'>April</option> \
	<option value="4" '+(curmonth!=4?'':'selected="selected"')+'>May</option> \
	<option value="5" '+(curmonth!=5?'':'selected="selected"')+'>June</option> \
	<option value="6" '+(curmonth!=6?'':'selected="selected"')+'>July</option> \
	<option value="7" '+(curmonth!=7?'':'selected="selected"')+'>August</option> \
	<option value="8" '+(curmonth!=8?'':'selected="selected"')+'>September</option> \
	<option value="9" '+(curmonth!=9?'':'selected="selected"')+'>October</option> \
	<option value="10" '+(curmonth!=10?'':'selected="selected"')+'>November</option> \
	<option value="11" '+(curmonth!=11?'':'selected="selected"')+'>December</option> \
	</select>';
}

function refreshCal () {
	var now = new Date ();
	var Cal = new Date();
	var startDay;
	var HTMLResult = "";
	
	if (curmonth == -1) 
		curmonth = now.getMonth();
	if (curyear == -1)
		curyear = now.getFullYear();

	Cal.setDate (1);
	Cal.setMonth (curmonth);
	Cal.setFullYear (curyear);
	startDay = Cal.getDay ();	
	t = 0;
	
	HTMLResult  = '<table cellpadding="0" cellspacing="0" border="1" width="200">';	
	HTMLResult  += '<tr><td align="center"><a href="javascript:void(0)" onclick="javascript:setCuryear(curyear-1)">&lt;&lt;</a></td><td colspan="5" align="center">'+getYearDropdown()+'</td><td align="center"><a href="javascript:void(0)" onclick="javascript:setCuryear(curyear+1)">&gt;&gt;</a></td></tr>';	
	HTMLResult  += '<tr><td align="center"><a href="javascript:void(0)" onclick="javascript:setCurmonth(curmonth-1)">&lt;&lt;</a></td><td colspan="5" align="center">'+getMonthDropdown()+'</td><td align="center"><a href="javascript:void(0)" onclick="javascript:setCurmonth(curmonth+1)">&gt;&gt;</a></td></tr>';	
	HTMLResult  += '<tr><td align="center">S</td><td align="center">M</td><td align="center">T</td><td align="center">W</td><td align="center">T</td><td align="center">F</td><td align="center">S</td></tr>';	
	for (w=0;w<6;w++) {
		HTMLResult += '<tr>';
		for (d=0;d<7;d++) {
			if ( (d < startDay) || (t > 31) ) {
				HTMLResult += '<td align="center" valign="middle">&nbsp;</td>';
				continue;
			}
			startDay = 0;
			t++;
			Cal.setDate (t);
			if (Cal.getMonth() != curmonth) {
				HTMLResult += '<td align="center" valign="middle">&nbsp;</td>';
				continue;
			}
			HTMLResult += '<td align="center" valign="middle"><a href="javascript:void(0)" onclick="javascript:selectDate('+t+', \''+calTarget+'\')" style="color: #000000; text-decoration: none">';
			if ( (t == now.getDate()) && (curmonth == now.getMonth()) && (curyear == now.getFullYear())) {
				HTMLResult += "<b>";
				HTMLResult += t;
				HTMLResult += "</b>";
			} else {
				HTMLResult += t;
			}
			HTMLResult += '</a></td>';
		}
		HTMLResult += '</tr>';
	}
	HTMLResult += '</table>';
	
	Calendar.innerHTML = HTMLResult;
}

function setCurmonth (month) {
	if (!month) 
		month = document.getElementById ("monthdrop").value;
	if (month < 1)
		month = 12;
	if (month > 12)
		month = 1;
	curmonth = month;
	
	refreshCal ();	
}

function setCuryear (year) {
	if (!year) 
		year = document.getElementById ("yeardrop").value;
	if (year < 2000)
		year = 2015;
	if (year > 2015)
		year = 2000;
	curyear = year;
	
	refreshCal ();	
}

function showCal (targetid, anchorlink, setmonth, allowPast, align) {
	if (Calendar != undefined) {
		document.body.removeChild(Calendar);
		Calendar = undefined;
	}
	if (!align)
		align = "right";

	calTarget = targetid;
	allowingPast = allowPast;

	pos = getPos (document.getElementById (anchorlink));
	Calendar = document.createElement ("div");
	document.body.appendChild(Calendar);
	
	Calendar.style.top = pos[0]+"px";
	if (align == "right")
		Calendar.style.left = pos[1]+120+"px";
	else
		Calendar.style.left = pos[1]-205+"px";
	Calendar.style.position = "absolute";
	Calendar.style.backgroundColor = "#ffffff";
	Calendar.style.display = "block";
	
	if (setmonth != undefined)
		setCurmonth (setmonth);
	else
		refreshCal ();
};