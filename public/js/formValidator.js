function validate_Checkbox (name) {
	var checkboxes = document.getElementsByName (name);
	var valid = false;
	
	for (var i = 0; i < checkboxes.length; i++ ) {
		if (checkboxes[i].checked)
			valid = true;
	}
	
	return valid?false:true;
}

function validate_Radio (name) {
	var radio = document.getElementsByName (name);
	var valid = false;
	
	for (var i = 0; i < radio.length; i++ ) {
		if (radio[i].checked)
			valid = true;
	}
	
	return valid?false:true;
}

function validate_Dropdown (name, firstValid) {
	var index = document.getElementsByName(name)[0].selectedIndex;
	
	if ( (index == 0) && (!firstValid) )
		return true;
	else if (index == -1)
		return true;
		
	return false;
}

var TF_NONE = 0;
var TF_PHONE = 1;
var TF_EMAIL = 2;

function validate_Textfield (name, type) {
	var text = document.getElementById(name).value;
	
	if (!text) return true;
	
	if (text.length <= 0)
		return true;
	
	switch (type) {
		case 0:
		default:
			break;
		case 1: //Phone
			if (text.search(/[0-9]{3}-?[0-9]{3}-?[0-9]{4}/) == -1)
				return false;
			break;
		case 2: //Email
			if (text.search(/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/) == -1)
				return false;
			break;
	}

	return false;
}