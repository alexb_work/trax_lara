// JavaScript Document
var fadedBG;
var popupWindow;

//constants
var TYPE_ERROR = 0;
var TYPE_WARNING = 1;
var TYPE_OK = 2;

function popup_show (type, title, text)
{	
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	icon = document.createElement ("img");
	titleSpan = document.createElement ("span");
	textSpan = document.createElement ("span");
	
	document.body.appendChild(fadedBG);
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.6"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=60)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#ffffff";
	fadedBG.onclick = popup_close;

	document.body.appendChild(popupWindow);
	popupWindow.style.height = "40px";
	popupWindow.style.width = "370px";
	popupWindow.style.position = "absolute";
	popupWindow.style.display = "block";
	popupWindow.style.top = viewHeight - (70/2) + "px";
	popupWindow.style.left = viewWidth - (400/2) + "px";
	popupWindow.style.padding = "15px";
	popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.backgroundImage = "url('/require/images/popup_errorBG.png')";
	popupWindow.style.backgroundRepeat = "repeat-x";
	popupWindow.style.border = "solid 1px #dddddd";
	popupWindow.style.lineHeight = "1.5em";
	popupWindow.style.opacity = "1";
	popupWindow.style.filter = "alpha(opacity=100)";
	popupWindow.onclick = popup_close;
	
	popupWindow.appendChild(icon);
	switch (type) {
		case TYPE_ERROR:
			icon.src = "/require/images/popup_ErrorIcon.gif";
			break;
		case TYPE_WARNING:
			icon.src = "/require/images/popup_WarningIcon.gif";
			break;
		case TYPE_OK:
			icon.src = "/require/images/popup_OkIcon.gif";
			break;
	}
	
	icon.border = "0";
	icon.style.marginRight = "10px";
	icon.style.display = "block";
	icon.align = "left";
	icon.style.width = "30px";
	icon.style.height = "30px";
	
	popupWindow.appendChild (titleSpan);
	titleSpan.style.fontFamily = "arial, helvetica, sans-serif";
	titleSpan.style.fontWeight = "bold";
	titleSpan.innerHTML = title + "<br />";
	titleSpan.style.display = "inline";
	titleSpan.position = "relative";
	
	popupWindow.appendChild (textSpan);
	textSpan.style.fontFamily = "arial, helvetica, sans-serif";
	textSpan.style.fontWeight = "normal";
	textSpan.innerHTML = text + "<br />";
	textSpan.style.display = "inline";
	textSpan.position = "relative";
}

function popup_form (width, height)
{
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	
	document.body.appendChild(fadedBG);	
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.6"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=60)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#ffffff";
	
	document.body.appendChild(popupWindow);	
	popupWindow.style.height = height + "px";
	popupWindow.style.width = width + "px";
	popupWindow.style.verticalAlign = "middle";
	popupWindow.style.overflow = "auto";
	popupWindow.style.top = (winHeight/2) - (height/2) + "px";
	popupWindow.style.left = (winWidth/2) - (width/2) + "px";
	popupWindow.style.position = "absolute";
	popupWindow.style.display = "block";
	popupWindow.style.opacity = "1.0"; // 100% for CSS
	popupWindow.style.filter = "alpha(opacity=100)"; //100% for IE6+
	popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.border = "solid 1px #dddddd";
	popupWindow.innerHTML = document.getElementById ("custompopup").innerHTML;
}

function popup_iframe (src, width, height)
{
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	iframe = document.createElement("iframe");
	
	document.body.appendChild(fadedBG);	
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.6"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=60)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#ffffff";
	fadedBG.onclick = popup_close
	
	document.body.appendChild(popupWindow);	
	popupWindow.style.height = height + "px";
	popupWindow.style.width = width + "px";
	popupWindow.style.verticalAlign = "middle";
	popupWindow.style.overflow = "auto";
	popupWindow.style.top = viewHeight - (height/2) + "px";
	popupWindow.style.left = viewWidth - (width/2) + "px";
	popupWindow.style.position = "absolute";
	popupWindow.style.opacity = "1.0"; // 100% for CSS
	popupWindow.style.filter = "alpha(opacity=100)"; //100% for IE6+
	popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.border = "solid 1px #dddddd";
	popupWindow.innerHTML = "<div align=\"right\" style=\"padding: 0px; margin: 5px; width: "+(width-20)+"px;\"><a href=\"javascript:void(0)\" onclick=\"popup_close()\"><b>X</b></a></div>";
	
	popupWindow.appendChild(iframe);
	iframe.style.height = height-30 + "px";
	iframe.style.width = width + "px";
	iframe.style.top = "0px";
	iframe.style.left = "0px";
	iframe.src = src;
	iframe.style.border = "none";
	iframe.style.display = "block";
	
	popupWindow.style.display = "block";
}

function popup_close ()
{
	if (fadedBG) {
		document.body.removeChild(fadedBG);
		fadedBG = undefined;
	}
	
	if (popupWindow) {
		document.body.removeChild(popupWindow);
		popupWindow = undefined;
	}
}