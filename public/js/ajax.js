function sendRequest()
{
	try {
		this.XmlHttp.open("GET",this.page,false);
		this.XmlHttp.send(null);
	
		return this.XmlHttp.responseText;
	} catch (e) {
		alert(e.description );
		return "";
	}
}

function ajaxClass (page)
{
	this.page = page;
	
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		this.XmlHttp = new XMLHttpRequest();
	}

	if (window.ActiveXObject) {
		// code for IE6, IE5
		this.XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	this.sendRequest = sendRequest;
}