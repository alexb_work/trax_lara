// JavaScript Document
var fadedBG;
var popupWindow;
var iefixiframe;
var oncloseFunc;

//constants
var TYPE_ERROR = 0;
var TYPE_WARNING = 1;
var TYPE_OK = 2;

function popup_show (type, title, text)
{	
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	icon = document.createElement ("img");
	titleSpan = document.createElement ("span");
	textSpan = document.createElement ("span");
	
	document.body.appendChild(fadedBG);
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.6"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=60)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#ffffff";
	fadedBG.onclick = popup_close;

	document.body.appendChild(popupWindow);
	popupWindow.style.height = "40px";
	popupWindow.style.width = "370px";
	popupWindow.style.position = "absolute";
	popupWindow.style.display = "block";
	popupWindow.style.top = viewHeight - (70/2) + "px";
	popupWindow.style.left = viewWidth - (400/2) + "px";
	popupWindow.style.padding = "15px";
	popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.backgroundImage = "url('/require/images/popup_errorBG.png')";
	popupWindow.style.backgroundRepeat = "repeat-x";
	popupWindow.style.border = "solid 1px #dddddd";
	popupWindow.style.lineHeight = "1.5em";
	popupWindow.style.opacity = "1";
	popupWindow.style.filter = "alpha(opacity=100)";
	popupWindow.onclick = popup_close;
	
	popupWindow.appendChild(icon);
	switch (type) {
		case TYPE_ERROR:
			icon.src = "/require/images/popup_ErrorIcon.gif";
			break;
		case TYPE_WARNING:
			icon.src = "/require/images/popup_WarningIcon.gif";
			break;
		case TYPE_OK:
			icon.src = "/require/images/popup_OkIcon.gif";
			break;
	}
	
	icon.border = "0";
	icon.style.marginRight = "10px";
	icon.style.display = "block";
	icon.align = "left";
	icon.style.width = "30px";
	icon.style.height = "30px";
	
	popupWindow.appendChild (titleSpan);
	titleSpan.style.fontFamily = "arial, helvetica, sans-serif";
	titleSpan.style.fontWeight = "bold";
	titleSpan.innerHTML = title + "<br />";
	titleSpan.style.display = "inline";
	titleSpan.position = "relative";
	
	popupWindow.appendChild (textSpan);
	textSpan.style.fontFamily = "arial, helvetica, sans-serif";
	textSpan.style.fontWeight = "normal";
	textSpan.innerHTML = text + "<br />";
	textSpan.style.display = "inline";
	textSpan.position = "relative";
}

function popup_form (width, height)
{
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	
	document.body.appendChild(fadedBG);	
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.6"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=60)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#ffffff";
	
	document.body.appendChild(popupWindow);	
	popupWindow.style.height = height + "px";
	popupWindow.style.width = width + "px";
	popupWindow.style.verticalAlign = "middle";
	popupWindow.style.overflow = "auto";
	popupWindow.style.top = (winHeight/2) - (height/2) + "px";
	popupWindow.style.left = (winWidth/2) - (width/2) + "px";
	popupWindow.style.position = "absolute";
	popupWindow.style.display = "block";
	popupWindow.style.opacity = "1.0"; // 100% for CSS
	popupWindow.style.filter = "alpha(opacity=100)"; //100% for IE6+
	popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.border = "solid 1px #dddddd";
	popupWindow.innerHTML = document.getElementById ("custompopup").innerHTML;
}

function popup_iframe (src, width, height, onclose)
{
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	iefixiframe = document.createElement("iframe");
	iefixiframe.src = "/require/blank.html";
	
	document.body.appendChild(iefixiframe);		
	iefixiframe.style.width = winWidth + "px";
	iefixiframe.style.height = winHeight + "px";
	iefixiframe.style.position = "absolute";
	iefixiframe.style.display = "block";
	iefixiframe.style.top = "0";
	iefixiframe.style.left = "0";
	iefixiframe.style.opacity = "0.0"; // 60% for CSS
	iefixiframe.style.filter = "alpha(opacity=0)"; //60% for IE6+
	iefixiframe.style.backgroundColor = "#000000";
	iefixiframe.style.zIndex = 10;
	
	document.body.appendChild(fadedBG);	
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.4"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=40)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#000000";
	fadedBG.onclick = popup_close
	fadedBG.style.zIndex = 11;
	
	document.body.appendChild(popupWindow);	
	popupWindow.style.height = height + "px";
	popupWindow.style.width = width + "px";
	popupWindow.style.verticalAlign = "middle";
	popupWindow.style.overflow = "hidden";
	popupWindow.style.top = viewHeight - (height/2) + "px";
	popupWindow.style.left = viewWidth - (width/2) + "px";
	popupWindow.style.position = "absolute";
	popupWindow.style.opacity = "1.0"; // 100% for CSS
	popupWindow.style.filter = "alpha(opacity=100)"; //100% for IE6+
	//popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.border = "none";
	popupWindow.style.zIndex = 12;
	//popupWindow.innerHTML = "<div align=\"right\" style=\"padding: 0px; margin: 5px; width: "+(width-5)+"px;\"><a href=\"javascript:void(0)\" onclick=\"popup_close()\"><img src=\"/require/images/bt_close.gif\" border=\"\" alt=\"\" /></a></div>";
	
	if (onclose == undefined)
		onclose = "";

	popupWindow.innerHTML = 
		// Top left
		'<div style="width: 28px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_topleft.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_topleft.png\'); background-repeat: none;"></div>' +
		// Top
		'<div style="width: ' + (width - 56) + 'px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_top.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_top.png\'); background-repeat: repeat-x;"></div>' +
		// Top Right
		'<div style="width: 28px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_topright.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_topright.png\'); background-repeat: none;"></div>' +
		// Left
		'<div style="width: 28px; height: ' + (height - 45) + 'px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_left.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_left.png\'); background-repeat: repeat-y;"></div>' +
		// X
		'<div align="right" style="float: left; padding: 0px; margin: 0px; width: '+(width-56)+'px;">' +
			'<div style="height: ' + (height - 45) + 'px; background-color: #ffffff;"><a href="javascript:void(0)" onclick="popup_close();'+onclose+'"><img src="/require/images/bt_close.gif" border="0" alt="" /></a>' +
			'<iframe id="popup_wnd" frameborder="0" style="height: ' + (height-80) + 'px; width: ' + (width-56) + 'px; top: 0px; left: 0px; border: none; background-color: #ffffff" src="'+src+'"></iframe></div>' +
		'</div>' + 
		// Right
		'<div style="width: 28px; height: ' + (height - 45) + 'px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_right.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_right.png\'); background-repeat: repeat-y;"></div>' +
		// Bottom left
		'<div style="width: 28px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottomleft.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottomleft.png\'); background-repeat: none;"></div>' +
		// Bottom
		'<div style="width: ' + (width - 56) + 'px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottom.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottom.png\'); background-repeat: repeat-x;"></div>' +
		// Bottom Right
		'<div style="width: 28px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottomright.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottomright.png\'); background-repeat: none;"></div>';
	
	popupWindow.style.display = "block";
}

var popup_animating = false;

function popup_animate (finalwidth, finalheight)
{
	var changed = false;
	
	if (!popupWindow)
		return;
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	if (popupWindow.offsetWidth < finalwidth) {
		if (popupWindow.offsetWidth + 40 > finalwidth)
			add = finalwidth;
		else
			add = popupWindow.offsetWidth + 40;
			
		popupWindow.style.width = add + "px";
		
		if (viewWidth - (add/2) <= 0)
			popupWindow.style.left = "0px";
		else
			popupWindow.style.left = viewWidth - (add/2) + "px";
			
		width = add;
		changed = true;
	} else if (popupWindow.offsetWidth > finalwidth) {
		if (popupWindow.offsetWidth - 40 < finalwidth)
			add = finalwidth;
		else
			add = popupWindow.offsetWidth - 40;
			
		popupWindow.style.width = add + "px";
		
		if (viewWidth - (add/2) <= 0)
			popupWindow.style.left = "0px";
		else
			popupWindow.style.left = viewWidth - (add/2) + "px";
			
		width = add;
		changed = true;
	} else {
		width = finalwidth;
	}
	
	if (popupWindow.offsetHeight < finalheight) {
		if (popupWindow.offsetHeight + 40 > finalheight)
			add = finalheight;
		else
			add = popupWindow.offsetHeight + 40;
		popupWindow.style.height = add + "px";
		
		if (viewHeight - (add/2) <= 0)
			popupWindow.style.top = "0px";
		else
			popupWindow.style.top = viewHeight - (add/2) + "px";
			
		height = add;
		changed = true;
	} else if (popupWindow.offsetHeight > finalheight) {
		if (popupWindow.offsetHeight - 40 < finalheight)
			add = finalheight;
		else
			add = popupWindow.offsetHeight - 40;
		popupWindow.style.height = add + "px";
		
		if (viewHeight - (add/2) <= 0)
			popupWindow.style.top = "0px";
		else
			popupWindow.style.top = viewHeight - (add/2) + "px";
			
		height = add;
		changed = true;
	} else {
		height = finalheight;
	}
	
	var newwidth = (width - 56);
	if (newwidth < 2)
		newwidth = 2;
	var newheight = (height - 45);
	if (newheight < 0)
		newheight = 0;
		
	document.getElementById("popup_top").style.width = newwidth+"px";
	document.getElementById("popup_left").style.height = newheight+"px";
	document.getElementById("popup_right").style.height = newheight+"px";
	document.getElementById("popup_bottom").style.width = newwidth+"px";
	document.getElementById("popup_box").style.width = newwidth+"px";
	document.getElementById("popup_boxtop").style.height = newheight+"px";
	document.getElementById("popup_boxtop").style.width = newwidth+"px";
	
	if (changed) {
		setTimeout("popup_animate(" + finalwidth + ", " + finalheight + ")", 50);
		popup_animating = true;
	} else {
		popup_animating = false;
	}
		
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (finalheight > winHeight)
		winHeight = finalheight;
	if (finalwidth > winWidth)
		winWidth = finalwidth;
		
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.width = winWidth + "px";
	iefixiframe.style.width = winWidth + "px";
	iefixiframe.style.height = winHeight + "px";
}

function popup_animated (content, finalwidth, finalheight, onclose)
{
	height = 1;
	width = 1;
	oncloseFunc = onclose;
	
	if (document.documentElement.clientHeight > document.body.clientHeight) {
		winHeight = document.documentElement.clientHeight;
		winWidth = document.documentElement.clientWidth;
	} else {
		if (document.documentElement.clientHeight == 0)
			winHeight = document.body.scrollHeight;
		else
			winHeight = document.body.clientHeight;
		winWidth = document.body.clientWidth;
	}
	
	if (window.pageYOffset) {
		viewHeight = window.pageYOffset + window.innerHeight/2;
		viewWidth = window.pageXOffset + window.innerWidth/2;
	} else if (document.documentElement.clientHeight != 0) {
		viewHeight = document.documentElement.scrollTop + document.documentElement.clientHeight/2;
		viewWidth = document.documentElement.scrollLeft + document.documentElement.clientWidth/2;
	} else {
		viewHeight = document.body.scrollTop + document.body.clientHeight/2;
		viewWidth = document.body.scrollLeft + document.body.clientWidth/2;
	}
	
	if (finalheight > winHeight)
		winHeight = finalheight;
	if (finalwidth > winWidth)
		winWidth = finalwidth;
	
	fadedBG = document.createElement ("div");
	popupWindow = document.createElement ("div");
	iefixiframe = document.createElement("iframe");
	iefixiframe.src = "/require/blank.html";
	
	document.body.appendChild(iefixiframe);		
	iefixiframe.style.width = winWidth + "px";
	iefixiframe.style.height = winHeight + "px";
	iefixiframe.style.position = "absolute";
	iefixiframe.style.display = "block";
	iefixiframe.style.top = "0";
	iefixiframe.style.left = "0";
	iefixiframe.style.opacity = "0.0"; // 60% for CSS
	iefixiframe.style.filter = "alpha(opacity=0)"; //60% for IE6+
	iefixiframe.style.backgroundColor = "#000000";
	iefixiframe.style.zIndex = 10;
	
	document.body.appendChild(fadedBG);	
	fadedBG.style.height = winHeight + "px";
	fadedBG.style.width = winWidth + "px";
	fadedBG.style.position = "absolute";
	fadedBG.style.display = "block";
	fadedBG.style.top = "0";
	fadedBG.style.left = "0";
	fadedBG.style.opacity = "0.4"; // 60% for CSS
	fadedBG.style.filter = "alpha(opacity=40)"; //60% for IE6+
	fadedBG.style.backgroundColor = "#000000";
	fadedBG.onclick = popup_close
	fadedBG.style.zIndex = 11;
	
	document.body.appendChild(popupWindow);	
	popupWindow.style.height = height + "px";
	popupWindow.style.width = width + "px";
	popupWindow.style.verticalAlign = "middle";
	popupWindow.style.overflow = "hidden";
	popupWindow.style.top = viewHeight - (height/2) + "px";
	popupWindow.style.left = viewWidth - (width/2) + "px";
	popupWindow.style.position = "absolute";
	popupWindow.style.opacity = "1.0"; // 100% for CSS
	popupWindow.style.filter = "alpha(opacity=100)"; //100% for IE6+
	//popupWindow.style.backgroundColor = "#ffffff";
	popupWindow.style.border = "none";
	popupWindow.style.zIndex = 12;
	//popupWindow.innerHTML = "<div align=\"right\" style=\"padding: 0px; margin: 5px; width: "+(width-5)+"px;\"><a href=\"javascript:void(0)\" onclick=\"popup_close()\"><img src=\"/require/images/bt_close.gif\" border=\"\" alt=\"\" /></a></div>";

	popupWindow.innerHTML = 
		// Top left
		'<div style="width: 28px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_topleft.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_topleft.png\'); background-repeat: none;"></div>' +
		// Top
		'<div id="popup_top" style="width: ' + (width - 56) + 'px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_top.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_top.png\'); background-repeat: repeat-x;"></div>' +
		// Top Right
		'<div style="width: 28px; height: 15px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_topright.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_topright.png\'); background-repeat: none;"></div>' +
		// Left
		'<div id="popup_left" style="width: 28px; height: ' + (height - 45) + 'px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_left.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_left.png\'); background-repeat: repeat-y;"></div>' +
		// X
		'<div  id="popup_box" align="right" style="float: left; padding: 0px; margin: 0px; width: '+(width-56)+'px;">' +
			'<div id="popup_boxtop" style="height: ' + (height - 45) + 'px; background-color: #ffffff; overflow: hidden;">' + 
				'<a href="javascript:void(0)" onclick="popup_close();"><img src="/require/images/bt_close.gif" border="0" alt="" /></a><br />' +
				content +
			'</div>' +
		'</div>' + 
		// Right
		'<div id="popup_right" style="width: 28px; height: ' + (height - 45) + 'px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_right.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_right.png\'); background-repeat: repeat-y;"></div>' +
		// Bottom left
		'<div style="width: 28px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottomleft.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottomleft.png\'); background-repeat: none;"></div>' +
		// Bottom
		'<div id="popup_bottom" style="width: ' + (width - 56) + 'px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottom.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottom.png\'); background-repeat: repeat-x;"></div>' +
		// Bottom Right
		'<div style="width: 28px; height: 30px; float: left; font-size: 1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/require/images/popup_bottomright.png\', sizingMethod=\'scale\'); background-image: url(\'/require/images/popup_bottomright.png\'); background-repeat: none;"></div>';
	
	popupWindow.style.display = "block";
	
	popup_animate (finalwidth, finalheight, content, onclose)
}

function popup_close ()
{
	if (fadedBG) {
		document.body.removeChild(fadedBG);
		fadedBG = undefined;
	}
	
	if (iefixiframe) {
		document.body.removeChild(iefixiframe);
		iefixiframe = undefined;
	}
	
	if (popupWindow) {
		document.body.removeChild(popupWindow);
		popupWindow = undefined;
	}
	
	if (oncloseFunc)
		eval(oncloseFunc);
}