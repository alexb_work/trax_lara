@extends('layouts.app')

@section('content')
<!-- TODO php block in user/index.php-->
    <!-- bootstrap widget theme -->
    <link rel="stylesheet" href="{{ URL::asset('css/theme.bootstrap.css') }}">
<?php
    $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());
 ?>

    <!-- tablesorter plugin -->
    <script src="{{ URL::asset('js/jquery.tablesorter.js') }}"></script>
    <!-- tablesorter widget file - loaded after the plugin -->
    <script src="{{ URL::asset('js/jquery.tablesorter.widgets.js') }}"></script>
    
    <!-- pager plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.tablesorter.pager.css') }}">
    <script src="{{ URL::asset('js/jquery.tablesorter.pager.js') }}"></script>  
<?php
    use \App\status;
?>
<script type="text/javascript">
    function change_status(idproject){
        var edit_check = $('#checkbox_status_'+idproject).prop('checked')
        if(edit_check == true){
            $("#status_"+idproject).hide();
            $("#select_status_"+idproject).show();
        
        }
        else{
            $("#status_"+idproject).show();
            $("#select_status_"+idproject).hide();
        }

    }
    
    function new_status(idproject,status_project){
        //console.log(idproject);
        //console.log(status_project);
        var str = "status="+status_project+"&idproject="+idproject;
        var edit_check = $('#checkbox_status_'+idproject).prop('checked');
        var $_token = $('#token').val();
        $.ajax({
            type: 'post',
            cache: false,
            headers: { 'X-XSRF-TOKEN' : $_token }, 
            url:"{{URL::route('updateStatus')}}",
            data: str, 
            dataType: 'html',
            success: function(msg){
                //console.log(edit_check);
                $("#status_"+idproject).empty();
                $("#status_"+idproject).html(msg);
                $("#status_"+idproject).show();
                $("#select_status_"+idproject).hide();
                $('input[name=checkbox_status_'+idproject+']').attr('checked', false);
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });

    }
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    
    function delete_job(idproject){

        var str = "idproject="+idproject;
        var $_token = $('#token').val();

        $( "#delete_job" ).dialog({
            resizable: false,
            height:220,
            modal: true,
            buttons: [
            {
                text:  "{{ trans('home.projectDelete') }}",
                click: function() {
                    $.ajax({
                        type: 'post',
                        cache: false,
                        headers: { 'X-XSRF-TOKEN' : $_token }, 
                        url: "{{URL::route('deleteJob')}}",
                        data: str, 
                        dataType: 'html',
                        success: function(msg){
                            location.reload();
                        },
                        error: function(data){
                            var errors = $.parseJSON(data.responseText);
                            console.log(errors);
                            $.each(errors, function(index, value) {
                                $("#response ul").append('<li>'+value+'</li>');

                                $("#response").attr("hidden", false);

                            });
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "{{ trans('home.cancel') }}",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
            ]
        });
    }
    
    $(function() {  

        // call the tablesorter plugin and apply the uitheme widget
        $("table").tablesorter({
        // this will apply the bootstrap theme if "uitheme" widget is included
        // the widgetOptions.uitheme is no longer required to be set
            theme : "bootstrap",
            widthFixed: true,
            headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!
            // widget code contained in the jquery.tablesorter.widgets.js file
            // use the zebra stripe widget if you plan on hiding any rows (filter widget)
            widgets : [ "uitheme", "filter", "zebra" ],
            //debug: true,

            widgetOptions : {
              // using the default zebra striping class name, so it actually isn't included in the theme variable above
              // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
              zebra : ["even", "odd"],

              // reset filters button
              filter_reset : ".reset"

              // set the uitheme widget to use the bootstrap theme class names
              // this is no longer required, if theme is set
              // ,uitheme : "bootstrap"

            }
        })
        
        
        .tablesorterPager({
            size : 20,
            // target the pager markup - see the HTML block below
            container: $(".pager"),
            // target the pager page select dropdown - choose a page
            cssGoto  : ".pagenum",
            // remove rows from the table to speed up the sort of large tables.
            // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
            removeRows: false,
            // output string - default is '{page}/{totalPages}';
            // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
            output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

        });


    }); 
    
    
</script>
<input id="token" type="hidden" value="{{$encrypted_token}}">
<div id="xrx_page">
    <div class="row" style="margin-right: 0px; margin-left: 0px;">
        @include('partials.menu')
            
        <div class="col-md-10 xrx_content">
        @if (Auth::user()->categorie == 1 && Route::getCurrentRoute()->getPath() != 'home/archive')
            <a href="{{ URL::route('addJob') }}">
                <button class="remove_input sqrButton float_r" type="button"  style="padding-right:12px; margin-bottom:30px; ">
                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                </button>
            </a>
                <!--<a href="{{ URL::route('addJob') }}" class="sqrButton btn btn-primary float_r " style="padding-right:12px; font-size:2em; margin-bottom:30px; ">+</a>-->
            @endif
            <div id="response" hidden=true class="alert alert-danger">
                <ul>   
                
                </ul>
            </div>
            
            <div id="dashboard">
            
                <table class="table table-striped table-bordered table-condensed table-hover tablesorter">
                      <thead>
                        <tr>
                          <th>
                            {{ trans('home.requisition') }}
                            <p class="text-info">{{ trans('home.clickEdit') }}</p>
                          </th>
                        <th>
                            {{ trans('home.name') }}
                            <p class="text-info">{{ trans('home.clickManage') }}</p> 
                        </th>
                            <th>
                                {{ trans('home.clientName') }} 
                            </th>
                          <th>{{ trans('home.requestCompDate') }}</th>
                          <th>{{ trans('home.totalTasks') }}</th>
                          <th>{{ trans('home.projectType') }}</th>
                          <th>{{ trans('home.status') }}</th>
                        </tr>
                      </thead>
                      
                      
                    <tfoot>
                        <tr>
                            <th>
                            {{ trans('home.requisition') }}
                            <p class="text-info">{{ trans('home.clickEdit') }}</p>
                            </th>
                            <th>
                            {{ trans('home.name') }}
                            <p class="text-info">{{ trans('home.clickManage') }}</p> 
                            </th>
                            <th>
                                {{ trans('home.clientName') }}
                            </th>
                            <th>{{ trans('home.requestCompDate') }}</th>
                            <th>{{ trans('home.totalTasks') }}</th>
                            <th>{{ trans('home.lastChange') }}</th>
                            <th>{{ trans('home.status') }}</th>
                        </tr>
                        <tr>
                            <th colspan="8" class="pager form-horizontal">
                                <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                                <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                                <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                                <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                                <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
                                <select class="pagesize input-mini" title="Select page size">
                                    <option selected="selected" value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                </select>
                                <select class="pagenum input-mini" title="Select page number"></select>
                            </th>
                        </tr>
                    </tfoot>                          
                      
                      
                      
                      <tbody>                          

                        @foreach ($projects as $project)
                            <tr>
                              <td>
                                  <a href="{{ URL::route('editJob', $project->id_project) }}"> {{ $project->req }} </a>
                                                                  
                                     
                                   
                                    <?php 
                                        $overtimeAlert = false;
                                        $tasksCompleted = true; 
                                    ?>
                                    @foreach($project->tasks as $task)
                                        @if($task->task_status!=3)
                                            <?php $tasksCompleted = false; ?>
                                            @if($task->hours->sum('hours_completed') > $task->assignments->sum('assigned_hours'))

                                                <?php $overtimeAlert = true; ?>
                                                
                                                
                                            @endif
                                        @endif
                                    @endforeach
   
                                        @if ($project->id_status == 2 && Auth::user()->categorie == 1)
                                            | 
                                            <a href="javascript:delete_job({{ $project->id_project }})" data-toggle="tooltip" title="{{ trans('home.clickDelete') }}">
                                                <img width="25" src="{{ URL::asset('images/delete_icon.png') }}" />
                                            </a>
                                            
                                        @endif
                                         @if ( in_array("2",$project->job_types->lists('id_job_type')->toArray()) )
                                            | 
                                            <a href="{{ URL::route('printForm', ['project'=>$project->id_project]) }}" >
                                                <img width="25" src="{{ URL::asset('images/print_icon.png') }}" />
                                            </a>
                                        @endif
                                        @if(Auth::user()->categorie == 1 && $project->id_status != 10 && $overtimeAlert)
                                            |
                                            <a  data-toggle="tooltip" title="{{ trans('home.alertOvertime') }}">
                                                <img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                            </a>
                                        @endif
                                                                 
                                  
                              </td>
                              <td>
                                <a href="{{ URL::route('taskList', ['project'=>$project->id_project]) }}">
                                    {{ $project->project_name }}
                                </a>
                                    
                                     @if (Auth::user()->categorie == 1 )
                                            <a href="{{URL::route('estimateList', ['project'=>$project->id_project])}}" data-toggle="tooltip" title="{{ trans('home.clickSubmission') }}">
                                                <img width="25" src="{{ URL::asset('images/submission_icon.jpg') }}" />
                                            </a>
                                            
                                    @endif
                              
                              </td>
                              
                              <td>
                                {{ $project->name_client }}
                              </td>
                              
                              <td> 
                                      
                                {{ $project->end_date }} {{ $project->end_time }}
                              
                              </td>
                              <td>
                                
                                  {{ $project->nbTasks }}
                              </td>

                              <td>
                              <?php $count = 0; ?>
                              @foreach($project->job_types as $key => $job_type)
                                @unless($key == 0)
                                    |
                                @endunless
                                @if(LaravelLocalization::getCurrentLocale() == 'en')
                                    {{ $job_type->nom_en }}
                                @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                                    {{ $job_type->nom_fr }}
                                @endif
                                <?php $count++; ?>
                            @endforeach
                            @unless($project->other_job_type == null)
                                @if($count != 0)
                                | 
                                @endif
                                {{$project->other_job_type}}
                            @endunless



                              </td>
                              <td>
                              @if(Auth::user()->categorie == 1)
                                    <input type="checkbox" name="checkbox_status_{{ $project->id_project }}" id="checkbox_status_{{ $project->id_project }}" onclick="change_status({{ $project->id_project }})"> 
                                    @endif  
                                <div id="status_{{ $project->id_project }}" style="display:inline;"> <span style="color:{{ $project->color }}">
                                @if(LaravelLocalization::getCurrentLocale() == 'en')
                                    {{ $project->status->title_status_en }}
                                @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                                    {{ $project->status->title_status_fr }}
                                @endif
                                </span></div>
                                </select>  

                                {!! Form::select("project_status", $status, $project->id_status, ['onchange' => "new_status($project->id_project,this.value)", 'id' => "select_status_".$project->id_project, 'class' => "select_status" ]) !!}                               
                                    
                            </td>
    
                            </tr>
                            @endforeach
                      </tbody>
                </table>            
            
            </div><!-- end dashboard -->

        </div>
        
        <div class="clear"> &nbsp;</div>
    </div><!-- end #row -->
    
    <!-- *** Notifications *** -->
        <!-- Modal -->
        <div class="modal fade" id="notask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                         <h4 class="modal-title">{{ trans('home.changeStatus') }}</h4>

                    </div>
                    <div class="modal-body">
                        {{ trans('home.statusError') }}
                    </div>

                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('home.close') }}</button>
                    </div>                  
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    <!-- *** Fin de notifications *** -->
    
    
    
<div id="delete_job" title="{{ trans('home.projectDelete') }}?" style="display:none;">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        {{ trans('home.deleteConfirm') }}
        
    </p>
</div>  
    

</div><!-- end #xrx_page -->
    
    
@endsection
