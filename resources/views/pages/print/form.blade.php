@extends('layouts.app')

@section('content')
<?php
	use Illuminate\Support\Facades\Input;
?>
@include('pages.print.javascript')
<div id="xrx_page">

	<div class="row" style="margin-right: 0px;">
		@include('partials.menu')
		<div class="col-md-10 xrx_content">
			<p>
			  <a href="{{URL::route('home')}}" class="btn btn-primary">{{ trans('print.returnDash') }} </a>
			</p>		
			
			<h4 style="font-weight:bold;">{{ trans('print.printMgmt') }} {{ $project->project_name }}</h4>
			@if(isset($print_job))
				<?php $isEdit = true; ?>
				{!! Form::model($print_job, ['id' => 'add_project', 'name' => 'add_project', 'action' => ['PrintController@update'], 'files'=>true])!!}
				<input type="hidden" name="id_print_job" value="{{ $print_job->id_print_job }}" />
			@else
				<?php $isEdit = false; ?>
				{!! Form::open(['action' => ['PrintController@store'], 'id' => 'add_project', 'method' => 'post', 'name' => 'add_project', 'files'=>true])!!}
			@endif
			@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
				<input type="hidden" name="id_project" value="{{ $project->id_project }}" />

				<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							{{ trans('print.genInfo') }}
						</a>
					  </h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
					  <div class="panel-body">
						
						  <div class="form-group">
							<label for="inputEmail3" class="col-sm-3 control-label">{{ trans('print.projName') }}</label>
							<div class="col-sm-7">
							  <input type="text" name="project_name" value="{{ $project->project_name}}"  class="form-control" placeholder="{{ $project->project_name }}" required disabled>
							</div>
						  </div>


						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.projOwn') }}</label>
							<div class="col-sm-7">
							  <input type="text" name="name_client" class="form-control" placeholder="{{ trans('print.projOwn') }}" value="{{ $project->name_client }}" disabled>
							</div>
						  </div>
						  
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.province') }}</label>
							<div class="col-sm-7">
							  {!! Form::select('id_province', $provinces, null) !!}
							</div>
						  </div>


						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.recOn') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_received_date", null, ['id' => 'received_date', 'required' => 'true'])!!}
							</div>
						  </div>
						  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.infoDue') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_due_date", null, ['id' => 'p_due_date', 'required' => 'true'])!!}
								| 
								{!! Form::text("p_due_time", null, ['id' => 'p_due_time', 'required' => 'true'])!!}
							</div>
						  </div>
						  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.startPrint') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_start_date", null, ['id' => 'p_start_date', 'required' => 'true'])!!}
								| 
								{!! Form::text("p_start_time", null, ['id' => 'p_start_time', 'required' => 'true'])!!}
							</div>
						  </div>
						  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.workReq') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_work_date", null, ['id' => 'p_work_date', 'required' => 'true'])!!}
								| 
								{!! Form::text("p_work_time", null, ['id' => 'p_work_time', 'required' => 'true'])!!}
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.tel') }}</label>
							<div class="col-sm-7">
								{!! Form::tel("p_tel", null, ['id' => 'p_tel'])!!}
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.other') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_other", null, ['id' => 'p_other'])!!}
							</div>
						  </div>						  						  
						  						  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.address') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_adress", null, ['id' => 'p_adress', 'required' => 'true'])!!}
							</div>
						  </div>
						  


						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.delivery') }}</label>
							<div class="col-sm-7">
								@foreach($deliveries as $key=>$delivery)

										<label class="label_margin"> {!! Form::checkbox('delivery_type[]', $key, array_search($key, isset($print_job)?$print_job->delivery_types->lists('id_delivery_type')->toArray():array()) !== false ? true : false, ['id'=>$key]) !!}<label for="{{ $key }}"></label> {{$delivery}}
										</label>
								@endforeach

							</div>
						  </div>
						  
						  
						  <div class="form-group" id="p_purolator">
							<label class="col-sm-3 control-label">{{ trans('print.puroNum') }}</label>
							<div class="col-sm-7">
								{!! Form::text("p_purolator", null)!!}
							</div>
						  </div>
						  						  

					  </div>
					</div>
				  </div>
				  
				  
				  
				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						  {{ trans('print.billInfo') }}
						</a>
					  </h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
					  <div class="panel-body">
						  @if(count($project->cost_centers)<=1)
						 
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('print.costCenter') }}</label>
							<div class="col-sm-7">
							  <input type="text" id="cost_center" class="form-control" placeholder="{{ trans('print.costCenter') }}" value="{{ $project->cost_centers[0]->cost_center_name }}" disabled>
							</div>
						  </div>
						  
						  @else
							  <div class="form-group cost_split_multiple" id="cost_split_multiple_1">
								<label class="col-sm-3 control-label">{{ trans('print.costCenter') }}</label>
								<div class="col-sm-3">
								  <input type="text" name="cost_center_multipl[]" class="form-control" value="{{ $project->cost_centers[0]->cost_center_name }}" disabled>
								</div>
								<div class="col-sm-2">
								  <input type="text" name="percent_cc_multipl[]" class="form-control" value="{{ $project->cost_centers[0]->percentage }}" disabled>
								</div>
								<!--
								<div class="col-sm-2" style="text-align:center;padding-top: 5px;">
									<input id="add_cost_centre" type="button" value="+" onclick="addCostCentre();" class="remove_input" >
								</div>
								-->
							  </div>						   

						  
						 	@foreach($project->cost_centers as $key => $value)
						 	@if($key != 0)
						 		<?php $new_key = $key + 1; ?>
							
								
									  <div class="form-group cost_split_multiple" id="cost_split_multiple_{{ $new_key }}">
										<label class="col-sm-3 control-label">{{ trans('print.costCenter') }}</label>
										<div class="col-sm-3">
										  <input type="text" name="cost_center_multipl[]" class="form-control" value="{{ $project->cost_centers[$key]->cost_center_name }}" disabled>
										</div>
										<div class="col-sm-2">
										  <input type="text" name="percent_cc_multipl[]" class="form-control" value="{{ $project->cost_centers[$key]->percentage }}" disabled>
										</div>

									  </div>
									  @endif
									  @endforeach								
								
								
							@endif
						  

						  <div class="form-group" id="cost_split_single">
							<label class="col-sm-3 control-label">{{ trans('add_job.projCode') }}</label>
							<div class="col-sm-7">
							  <input type="text" class="form-control" placeholder="{{ trans('add_job.projCode') }}" value="{{ $project->project_code }}" disabled>
							  
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.networkAct') }}</label>
							<div class="col-sm-7">
							  <input type="text" class="form-control" placeholder="{{ trans('add_job.networkAct') }}" value="{{ $project->network_act }}" disabled>
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.gl') }}</label>
							<div class="col-sm-7">
							  <input type="text" class="form-control" placeholder="{{ trans('add_job.gl') }}" value="{{ $project->gl }}" disabled>
							</div>
						  </div>
						  
					  
					  </div>
					</div>
				  </div>

				@include('pages.print.repro')
				  
				  
				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a class="vAlign" data-toggle="collapse" data-parent="#accordion" href="#collapseGlobal">
						  {{ trans('print.global') }}
						</a>
						@if(Input::old('id_stapled_type')!==null)
							@if(array_key_exists('global', Input::old('id_stapled_type')))
							<button onclick="add_global_finition('global');"  class="remove_input sqrButton float_r" type="button">
								<span aria-hidden="true" id="add_finish_button" class="glyphicon glyphicon-minus"></span>
							</button>
						<!--<input type="button" id="add_finish_button" onclick="add_global_finition();" class="remove_input sqrButton" value="-">-->
							@endif
						@elseif($isEdit && $print_job->finition !== null)
						<button onclick="add_global_finition('global');"  class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" id="add_finish_button" class="glyphicon glyphicon-minus"></span>
						</button>
						<!--<input type="button" id="add_finish_button" onclick="add_global_finition();" class="remove_input sqrButton" value="-">-->
						@else
						<button onclick="add_global_finition('global');" class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" id="add_finish_button" class="glyphicon glyphicon-plus"></span>
						</button>
						<!--<input type="button" id="add_finish_button" onclick="add_global_finition();" class="remove_input sqrButton" value="+">-->
						@endif
						
					  </h4>
					</div>
					<div id="collapseGlobal" class="panel-collapse collapse">
						<div class="panel-body"> 
							<div  class="panel-group" aria-multiselectable="true" role="tablist">
								<table class="table" id="global_finition">
								@if(Input::old('id_stapled_type')!==null)
									@if(array_key_exists('global', Input::old('id_stapled_type')))
										<?php $fIndex = 'global'; 
											$fNum = 'global'; 
										?>
										@include('pages.print.finish')
									@endif
								@elseif($isEdit && $print_job->finition !== null)

									<?php $fIndex = 'global'; 
										$fNum = 'global'; 
										if($isEdit)
										$finish = $print_job->finition;
									?>
									@include('pages.print.finish')

								@endif
								</table>
								
							</div>
						</div>
					</div>
				  </div>
				  <!-- -->
				  
				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseFinition">
						  {{ trans('print.finition') }}
						</a>
												
					  </h4>
					</div>
					<div id="collapseFinition" class="panel-collapse collapse">
						<div class="panel-body">

							<div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('print.finiSpecs') }}</label>
								<div class="col-sm-7">
									<textarea name="finition_spec" style="width: 100%;height: 80px;"></textarea>
								</div>
							</div>

						</div>
					</div>
				  </div>
				  <!-- -->


				  @include('pages.print.supplies')
				  <!-- -->
				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a class="vAlign" data-toggle="collapse" data-parent="#accordion" href="#collapseDocs">
						  {{ trans('print.docs') }}
						</a>
						
						<button onclick="add_documents();" class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
						</button>
						<!--<input type="button" value="+" onclick="add_documents();" class="remove_input sqrButton">-->
												
					  </h4>
					</div>
					<div id="collapseDocs" class="panel-collapse collapse">
						<div class="panel-body">
							
							<div class="form-group" id="new_documents_1">
								<label class="col-sm-3 control-label">{{ trans('print.docs') }}</label>
								<div class="col-sm-7">
								@if($isEdit && $print_job->file_uploads !== null)
									@foreach($print_job->file_uploads as $file)
										<a href="http://xeroxweb.ca/laravel/trax_lara/public/index.php/download/{{$file->id_file_upload}}.{{$file->extension}}">{{$file->original_filename}}</a>
										@if($file->file_desc != "")
										<a  data-toggle="tooltip" title="{{ $file->file_desc }}" style="text-decoration:none">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                        </a>
                                        @endif<br>
									@endforeach
									
								@else
									{!! Form::file('project_file[0]') !!}
									{!! Form::text("file_desc[0]", null, ['class' => 'form-control space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
								@endif
									
								</div>
							</div>
							
							<div id="add_new_document"> </div>

						</div>
					</div>
				  </div>
				  <!-- -->
				  
				  
				  @include('pages.print.outsourcing')
				  <!-- -->
				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseInformation">
						  {{ trans('print.info') }}
						</a>						
					  </h4>
					</div>
					<div id="collapseInformation" class="panel-collapse collapse">
						<div class="panel-body">
							
							<div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('print.type') }}</label>
								<div class="col-sm-7">
									<label><input type="checkbox" value="1" name="info_type"> {{ trans('print.redo') }}</label>									
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('print.explain') }}</label>
								<div class="col-sm-7">
									<textarea name="info_explication" style="width: 100%;height: 80px;"></textarea>
								</div>
							</div>
							
							
						</div>
					</div>
				  </div>
				  <!-- -->				  

				</div>



				@if (Auth::user()->categorie == 1)
					  <div class="form-group">
						<div class="col-sm-offset-3 col-sm-7" style="margin-bottom: 10px;">
							<!--<input name ="edit_delivery" type="hidden" value="1" />-->
							@unless($isEdit)
							<button type="submit" class="btn btn-default xrx_arrow_link_right" onclick="form_add(document.getElementById('add_project'));" value="store" name="store">{{ trans('add_job.submit') }}<span ></span></button>
							@else
							<button type="submit" class="btn btn-default xrx_arrow_link_right" onclick="form_update(document.getElementById('add_project'));" value="store" name="store">{{ trans('add_job.submit') }}<span ></span></button>
							@endif
							<button type="submit" class="btn btn-default" onclick="target_popup(document.getElementById('add_project'));" value="template" name="templateSub" formnovalidate>{{ trans('print.viewPrice') }}</button>

						</div>
					  </div>
				@endif
				

			{!! Form::close()!!}
			
			
			
		
		
		</div>
		<div class="clear"> </div>
	
	
	</div><!-- end #row -->

</div><!-- end #xrx_page -->

	
@endsection