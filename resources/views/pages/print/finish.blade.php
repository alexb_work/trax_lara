<tr class="repro_finishing repro" id="repro_finishing_{{$fNum}}">
			<td colspan="9">
				<div class="row">
					<div class="col-md-12">
					@unless($fNum == "global")
					<div class="panel panel-default">
						<div class="panel-heading" id="headingReproFinish_{{$fNum}}" role="tab">
						    <h4 class="panel-title">	
								<a role="button" data-toggle="collapse" data-parent="#repro_finishing_{{$fNum}}" href="#accordion_finishing_{{$fNum}}" aria-controls="#accordion_finishing_{{$fNum}}" aria-expanded="false" class="collapsed">
						    		{{ trans('print.finishing') }}
								</a>
							</h4>
						</div>
						@endunless
						
						<div class="panel-group {{($fNum != 'global')?'collapse':''}}" id="accordion_finishing_{{$fNum}}" role="tablist" aria-multiselectable="true">

							<div class="panel panel-default">
								<div class="panel-heading headingStapled" id="headingStapled_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_1" data-parent="accordion_1_{{$fNum}}" href="#collapseStapled_{{$fNum}}" aria-controls="collapseStapled_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.stapled') }}
										</a>
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseStapled" id="collapseStapled_{{$fNum}}" aria-labelledby="headingStapled_{{$fNum}}" role="tabpanel" >
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.type') }}</th>
												<th>Machine</th>
												<th>{{ trans('print.79sheets') }}</th>
												<th>{{ trans('print.quantity') }}</th>
											</tr>
						
											<tr>
												<td>
												{!! Form::select("id_stapled_type[$fIndex]", $stapledTypes, isset($finish)?$finish->id_stapled_type:null, ['class' => 'stapled_type']) !!}
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("s_machine[$fIndex]", 1, isset($finish)?$finish->s_machine==1:null, ['id' => "stapled_machine_num_$fNum", 'class' => 'stapled_machine']) !!} 
														<label for="stapled_machine_num_{{$fNum}}" class="stapled_machine_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("s_over79[$fIndex]", 1, isset($finish)?$finish->s_over79==1:null, ['id' => "stapled_79sheets_num_$fNum", 'class' => 'stapled_79sheets']) !!} 
														<label for="stapled_79sheets_num_{{$fNum}}" class="stapled_79sheets_label"></label>
                               						</label>
												</td>
												<td>{!! Form::number("s_quantity[$fIndex]", isset($finish)?$finish->s_quantity:null, ['class' => 'stapled_qty'])!!}</td>
											</tr>
										</table>

									</div>
								
								</div>
							</div>
						
						
						
							<div class="panel panel-default">
								<div class="panel-heading headingBooklet" id="headingBooklet_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_2" data-parent="accordion_2_{{$fNum}}" href="#collapseBooklet_{{$fNum}}" aria-controls="collapseBooklet_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.booklet') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseBooklet" id="collapseBooklet_{{$fNum}}" aria-labelledby="headingBooklet_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.qtyBooks') }}</th>
												<th colspan="2">{{ trans('print.qtySheets') }}</th>
												<th>{{ trans('print.sizeClose') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">{!! Form::number("book_qty[$fIndex]", isset($finish)?$finish->book_qty:null, ['class' => 'booklet_qty'])!!} </td>
												<td colspan="2">{!! Form::number("book_sheets[$fIndex]", isset($finish)?$finish->book_sheets:null, ['class' => 'booklet_sheet_qty'])!!}</td>
												<td>{!! Form::text("book_size[$fIndex]", isset($finish)?$finish->book_size:null, ['class' => 'booklet_size'])!!}</td>
											</tr>
											
										</table>										

									</div>
								
								</div>
							</div>
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingSpiral" id="headingSpiral_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_3" href="#collapseSpiral_{{$fNum}}" aria-controls="collapseSpiral_{{$fNum}}" data-parent="accordion_3_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.spiral') }}/{{ trans('print.cerlox') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseSpiral" id="collapseSpiral_{{$fNum}}" aria-labelledby="headingSpiral_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.type') }}</th>
												<th>{{ trans('print.qtyBooks') }}</th>
												<th>{{ trans('print.qtySheets') }}</th>
												<th>{{ trans('print.color') }}</th>
												<th>{{ trans('print.cover') }}</th>
												<th>{{ trans('print.back') }}</th>
											</tr>
						
											<tr>
												<td>
													{!! Form::select("sc_type[$fIndex]", array(
														0 => '---',
														'spiral' => trans('print.spiral'),
														'cerlox' => trans('print.cerlox')), 
														isset($finish)?$finish->sc_type:null, ['class'=>'spiral_type']) 
													!!}													
												</td>
												<td>{!! Form::number("sc_book_qty[$fIndex]", isset($finish)?$finish->sc_book_qty:null, ['class' => 'spiral_qty'])!!}</td>
												<td>{!! Form::number("sc_book_sheets[$fIndex]", isset($finish)?$finish->sc_book_sheets:null, ['class' => 'spiral_sheet_qty'])!!}</td>
												<td>{!! Form::text("sc_color[$fIndex]", isset($finish)?$finish->sc_color:null, ['class' => 'spiral_color'])!!}</td>
						
												<td>{!! Form::text("sc_cover[$fIndex]", isset($finish)?$finish->sc_cover:null, ['class' => 'spiral_cover'])!!}</td>
												<td>{!! Form::text("sc_back[$fIndex]", isset($finish)?$finish->sc_back:null, ['class' => 'spiral_back'])!!}</td>
											</tr>
											
										</table>									

									</div>
								
								</div>
							</div>
							

							
							
												
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingBinder" id="headingBinder_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_5" href="#collapseBinder_{{$fNum}}" aria-controls="collapseBinder_{{$fNum}}" data-parent="accordion_5_{{$fNum}}" aria-expanded="true" >
										  {{ trans('print.binder') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseBinder" id="collapseBinder_{{$fNum}}" aria-labelledby="headingBinder_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.suppliedByCust') }}</th>
												<th>{{ trans('print.cover') }}</th>
												<th>{{ trans('print.spine') }}</th>
												<th>{{ trans('print.insert') }}</th>
												<th>{{ trans('print.comment') }}</th>
											</tr>
						
											<tr>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("bind_cust[$fIndex]", 1, isset($finish)?$finish->bind_cust == 1:null, ['id' => "binder_customer_num_$fNum", 'class' => 'binder_customer']) !!} 
														<label for="binder_customer_num_{{$fNum}}" class="binder_customer_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("bind_cover[$fIndex]", 1, isset($finish)?$finish->bind_cover == 1:null, ['id' => "binder_cover_num_$fNum", 'class' => 'binder_cover']) !!} 
														<label for="binder_cover_num_{{$fNum}}" class="binder_cover_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("bind_spine[$fIndex]", 1, isset($finish)?$finish->bind_spine == 1:null, ['id' => "binder_spine_num_$fNum", 'class' => 'binder_spine']) !!} 
														<label for="" class="binder_spine_label"></label>
                               						</label>
												</td>
												<td>
													{!! Form::number("bind_ins_qty[$fIndex]", isset($finish)?$finish->bind_ins_qty:null, ['class' => 'binder_qty'])!!}
												</td>
												<td>
													{!! Form::textarea("bind_comment[$fIndex]", isset($finish)?$finish->bind_comment:null, ['class' => 'binder_comment', 'style' => 'width: 100%;height: 60px;'])!!}
												</td>
											</tr>
											
										</table>											


									</div>
								</div>
							</div>




							<div class="panel panel-default">
								<div class="panel-heading headingDrilling" id="headingDrilling_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_6" href="#collapseDrilling_{{$fNum}}" aria-controls="collapseDrilling_{{$fNum}}" data-parent="accordion_6_{{$fNum}}" aria-expanded="true" >
										  {{ trans('print.drill') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseDrilling" id="collapseDrilling_{{$fNum}}" aria-labelledby="headingDrilling_{{$fNum}}" role="tabpanel" >
									<div class="panel-body">

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.holes') }}</th>
												<th colspan="2">{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::number("drill_holes[$fIndex]", isset($finish)?$finish->drill_holes:null, ['class' => 'drilling_holes'])!!}
												</td>
												<td colspan="2">
													{!! Form::number("drill_sheets[$fIndex]", isset($finish)?$finish->drill_sheets:null, ['class' => 'drilling_sheets'])!!}
												</td>
											</tr>
										</table>


									</div>
								</div>
							</div>
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingCuts" id="headingCuts_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_7" href="#collapseCuts_{{$fNum}}" aria-controls="collapseCuts_{{$fNum}}" data-parent="accordion_7_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.cuts') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelheadingCuts" id="collapseCuts_{{$fNum}}" aria-labelledby="headingCuts_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.finalCutSize') }}</th>
												<th colspan="">{{ trans('print.cutsQty') }}</th>
												<th colspan="">{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::text("cut_final_size[$fIndex]", isset($finish)?$finish->cut_final_size:null, ['class' => 'cuts_size'])!!}
												</td>
												<td>
													{!! Form::number("cut_qty[$fIndex]", isset($finish)?$finish->cut_qty:null, ['class' => 'cuts_qty'])!!}
												</td>
												<td>
													{!! Form::number("cut_sheet_qty[$fIndex]", isset($finish)?$finish->cut_sheet_qty:null, ['class' => 'cuts_sheets_qty'])!!}
												</td>
											</tr>
						
										</table>



									</div>
								</div>
							</div>
							
							

							<div class="panel panel-default">
								<div class="panel-heading headingPads" id="headingPads_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_8" href="#collapsePads_{{$fNum}}" aria-controls="collapsePads_{{$fNum}}" data-parent="accordion_8_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.pads') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelPads" id="collapsePads_{{$fNum}}" aria-labelledby="headingPads_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.size') }}</th>
												<th>{{ trans('print.padsQty') }}</th>
												<th>{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::select("pad_size[$fIndex]", array(
														0 => '---',
														'4.5 X 5.5' => '4.5 X 5.5',
														'8.5 X 11' => '8.5 X 11',
														'8.5 X 14' => '8.5 X 14',
														'8.5 X 5.5' => '8.5 X 5.5'),
														isset($finish)?$finish->pad_size:null, ['class'=>'pad_size']) 
													!!}
												</td>
												<td>
													{!! Form::number("pad_qty[$fIndex]", isset($finish)?$finish->pad_qty:null, ['class' => 'pads_qty'])!!}
												</td>
						                        <td>
						                        	{!! Form::number("pad_sheet_qty[$fIndex]", isset($finish)?$finish->pad_sheet_qty:null, ['class' => 'pads_sheets_qty'])!!}
												</td>
											</tr>
										</table>


									</div>
								</div>
							</div>
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingFolds" id="headingFolds_{{$fNum}}" role="tab" >
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_9" href="#collapseFolds_{{$fNum}}" aria-controls="collapseFolds_{{$fNum}}" data-parent="accordion_9_{{$fNum}}" aria-expanded="true" >
										  {{ trans('print.folds') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelFolds" id="collapseFolds_{{$fNum}}" aria-labelledby="headingFolds_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.foldsQty') }}</th>
												<th>{{ trans('print.sheetsQty') }}</th>
												<th>{{ trans('print.method') }}</th>
												<th>{{ trans('print.shape') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::number("fold_qty[$fIndex]", isset($finish)?$finish->fold_qty:null, ['class' => 'folds_qty'])!!}
												</td>
												<td>
													{!! Form::number("fold_sheet_qty[$fIndex]", isset($finish)?$finish->fold_sheet_qty:null, ['class' => 'folds_sheets_qty'])!!}
												</td>
						                        
						                        <td>
						                        <label style="margin-right:25px;" class="label_margin"> 
						                        {!! Form::radio("fold_method[$fIndex]", 'machine', isset($finish)?$finish->fold_method=='machine':null, ['id' => "folds_method1_num_$fNum", 'class' => 'folds_method1']) !!}
												<label for="folds_method1_num_{{$fNum}}" class="folds_method_label1"></label> Machine				
												</label>|
												<label style="margin-right:25px;" class="label_margin"> 
						                        {!! Form::radio("fold_method[$fIndex]", 'manual', isset($finish)?$finish->fold_method=='manual':null, ['id' => "folds_method2_num_$fNum", 'class' => 'folds_method2']) !!}
												<label for="folds_method2_num_{{$fNum}}" class="folds_method_label2"></label> {{ trans('print.manual') }}				
												</label>

												</td>
						                        <td>
						                        	{!! Form::text("fold_shape[$fIndex]", isset($finish)?$finish->fold_shape:null, ['class' => 'folds_shape'])!!}
												</td>
											</tr>
										</table>
										
									</div>
								</div>
							</div>
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingDistribution" id="headingDistribution_{{$fNum}}" role="tab" >
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_10" href="#collapseDistribution_{{$fNum}}" aria-controls="collapseDistribution_{{$fNum}}" data-parent="accordion_10_{{$fNum}}" aria-expanded="true">
										  {{ trans('print.insertion') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelDistribution" id="collapseDistribution_{{$fNum}}" aria-labelledby="headingDistribution_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
											<tr>
												<th >{{ trans('print.envSize') }}</th>
												<th >{{ trans('print.(qty)') }}</th>
												<th >{{ trans('print.method') }}</th>
												<th >{{ trans('print.labelQty') }}</th>
												<th >{{ trans('print.manSeal') }}</th>
												<th >{{ trans('print.canadaPost') }}</th>
						                        <th >{{ trans('print.internalMail') }}</th>
											</tr>
						
											<tr>
						                        <td>
						                        	{!! Form::text("insert_env_size[$fIndex]", isset($finish)?$finish->insert_env_size:null, ['class' => 'distribution_env_size'])!!}
												</td>
												<td>
													{!! Form::number("insert_qty[$fIndex]", isset($finish)?$finish->insert_qty:null, ['class' => 'distribution_qty'])!!}
												</td>
												 <td>
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio("insert_method[$fIndex]", 'machine', isset($finish)?$finish->insert_method=='machine':null, ['id' => "distribution_method1_num_$fNum", 'class' => 'distribution_method1']) !!}
													<label for="distribution_method1_num_{{$fNum}}" class="distribution_method_label1"></label> Machine				
													</label>|
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio("insert_method[$fIndex]", 'manual', isset($finish)?$finish->insert_method=='manual':null, ['id' => "distribution_method2_num_$fNum", 'class' => 'distribution_method2']) !!}
													<label for="distribution_method2_num_{{$fNum}}" class="distribution_method_label2"></label> {{ trans('print.manual') }}				
													</label>
												</td>
												 <td>
													<label class="label_margin">  
														{!! Form::checkbox("insert_label_qty[$fIndex]", 1, isset($finish)?$finish->insert_label_qty==1:null, ['id' => "distribution_labeling_num_$fNum", 'class' => 'distribution_labeling']) !!} 
														<label for="distribution_labeling_num_{{$fNum}}" class="distribution_labeling_label"></label>
                               						</label>
												</td>
												 <td>
												 	<label class="label_margin">  
														{!! Form::checkbox("insert_man_seal[$fIndex]", 1, isset($finish)?$finish->insert_man_seal==1:null, ['id' => "distribution_manual_sealing_num_$fNum", 'class' => 'distribution_manual_sealing']) !!} 
														<label for="distribution_manual_sealing_num_{{$fNum}}" class="distribution_manual_sealing_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("insert_canada_post[$fIndex]", 1, isset($finish)?$finish->insert_canada_post==1:null, ['id' => "distribution_canpost_num_$fNum", 'class' => 'distribution_canpost']) !!} 
														<label for="distribution_canpost_num_{{$fNum}}" class="distribution_canpost_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox("insert_internal_mail[$fIndex]", 1, isset($finish)?$finish->insert_internal_mail==1:null, ['id' => "distribution_intmail_num_$fNum", 'class' => 'distribution_intmail']) !!} 
														<label for="distribution_intmail_num_{{$fNum}}" class="distribution_intmail_label"></label>
                               						</label>
												</td>
											</tr>
										</table>
										
									</div>
								</div>
							</div>
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingAssembly" id="headingAssembly_{{$fNum}}" role="tab">
								  <h4 class="panel-title">	
										<a role="button" class="indentLeft" data-toggle="collapse" class="accordion_11" href="#collapseAssembly_{{$fNum}}" aria-controls="collapseAssembly_{{$fNum}}" data-parent="accordion_11_{{$fNum}}" aria-expanded="true" >
										  {{ trans('print.assembly') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelAssembly" id="collapseAssembly_{{$fNum}}" aria-labelledby="headingAssembly_{{$fNum}}" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
												<tr>
													<th colspan="2">{{ trans('print.collating') }}</th>
													<th>{{ trans('print.qtyLift') }}</th>
												</tr>
						
												<tr>
													<td colspan="2">
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio("assembly_collat_method[$fIndex]", 'machine', isset($finish)?$finish->assembly_collat_method=='machine':null, ['id' => "assembly_method1_num_$fNum", 'class' => 'assembly_method1']) !!}
													<label for="assembly_method1_num_{{$fNum}}" class="assembly_method_label1"></label> Machine				
													</label>|
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio("assembly_collat_method[$fIndex]", 'manual', isset($finish)?$finish->assembly_collat_method=='manual':null, ['id' => "assembly_method2_num_$fNum", 'class' => 'assembly_method2']) !!}
													<label for="assembly_method2_num_{{$fNum}}" class="assembly_method_label2"></label> {{ trans('print.manual') }}				
													</label>										
													</td>
						                               <td>
						                               	{!! Form::number("assembly_quantity_lift[$fIndex]", isset($finish)?$finish->assembly_quantity_lift:null, ['class' => 'assembly_qty'])!!}
													</td>
												</tr>
										</table>
										
									</div>
								</div>
							</div>

						</div><!-- end accordion_finishing -->
						@unless($fNum == "global")
						</div>
						@endunless
						

					</div>
				</div>
			</td>
		</tr>