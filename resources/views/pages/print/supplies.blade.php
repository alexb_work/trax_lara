<?php
	use Illuminate\Support\Facades\Input;
?>
<div class="panel panel-default">
	<div class="panel-heading">
	  	<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseSupplies">
		  		{{ trans('print.supplies') }}
			</a>
								
		</h4>
	</div>
	<div id="collapseSupplies" class="panel-collapse collapse">
		<div class="panel-body">
			<div class="col-md-12">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover" id="supply">
				<tr id="supply_0">
													
					<th style="width:70%">{{ trans('print.item') }} </th>
					<th>{{ trans('print.qty') }} </th>
					<th class="text-center buttonHeader">
						<button onclick="add_supply()" class="remove_input sqrButton" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
						</button>
					</th>	
				</tr>
				<?php $supCount = 1; 
					$suppliesArray = array();?>
				@if($isEdit)
				<?php $suppliesArray = $print_job->supplies; ?>
				@elseif(Input::old('supply_item')!==null)
				<?php $suppliesArray = Input::old('supply_item'); ?>
				@endif
				
				@foreach($suppliesArray as $key=>$supply)

				<tr id ="supply_{{$supCount}}">
					<td>
						{!! Form::select("supply_item[$key]", $supplies, $isEdit?$supply->supply_code:null, ['class'=>'supply_item']) !!}
					</td>
					<td>
						{!! Form::number("supply_qty[$key]", $isEdit?$supply->qty:null, ['class'=>'supply_qty'])!!}
					</td>
					<td class="text-center">
						<button onclick="remove_item('supply', {{$supCount}}, '')" class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
						</button>
									
										
					</td>
					
		
				</tr>
				<?php $supCount++; ?>
				@endforeach
			</table>
			</div>
			</div>
		</div>
	</div>
</div>