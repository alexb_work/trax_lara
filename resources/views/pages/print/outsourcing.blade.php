<?php
	use Illuminate\Support\Facades\Input;

	use App\print_management_submission as submission;
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseOutsourcing">
				{{ trans('print.outsource') }}
			</a>						
		</h4>
	</div>
	<div id="collapseOutsourcing" class="panel-collapse collapse">
		<div class="panel-body">
		<div class="col-md-12">
			<table class="table table-striped table-bordered table-condensed table-hover" id="outsourcing">
				<tr id="outsourcing_0">
					
					<th style="width:23.75%">{{ trans('print.type') }}</th>
					<th style="width:23.75%">{{ trans('print.name') }} </th>
					<th style="width:23.75%">#</th>
					<th style="width:23.75%">{{ trans('print.cost') }}</th>
					<th class="text-center buttonHeader">
						<button onclick="add_outsourcing();" class="remove_input sqrButton" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
						</button>
					</th>
				</tr>

				<?php $outCount = 1; 
					$outsourceArray = array();
					$isOld = false;?>
				@if(Input::old('outsource_type')!==null)
				<?php $outsourceArray = Input::old('outsource_name'); 
					$isOld = true;?>
				@elseif($isEdit)
				<?php $outsourceArray = $print_job->outsourcings; ?>
				@endif

				@foreach($outsourceArray as $key=>$outsource)
				<tr id="outsourcing_{{$outCount}}">
				
					<td>
						{!! Form::select("outsource_type[$key]", $outsources, !$isOld?$outsource->id_outsource_type:null, ['class' => 'outsource_type', 'onchange' => 'change_outsource_type(this, '.$outCount.', true)']) !!}
					</td>
					<td>
						{!! Form::text("outsource_name[$key]", !$isOld?$outsource->name:null, ['class' => 'outsource_name'])!!}
					</td>
		
					<td>
						{!! Form::text("outsource_code[$key]", !$isOld?$outsource->code:null, ['class' => 'outsource_code'])!!}
					</td>
					
					<td>
						{!! Form::number("outsource_cost[$key]", !$isOld?$outsource->cost:null, ['step'=>'any', 'class' => 'outsource_cost'])!!}
		
					</td>
					<td class="text-center">
						<button onclick="remove_item('outsourcing', {{$outCount}}, '')" class="remove_input sqrButton" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
						</button>
									<!--<input type="button" class="remove_input remove_item" onclick="remove_item('outsourcing', {{$outCount}}, '')" value="x">-->
										
					</td>
				
				</tr>
				<?php $subArray = array(); 
					$isOld = false; ?>
				@if(Input::old('print_mngt_supplier')[$key]!==null)
					@if($isEdit)

						@foreach(Input::old('subPID')[$key] as $subKey=>$value)
							@if($value != 'new')
								<?php $subArray[$subKey] = submission::where('id_print_management_submission', $value)->get()[0]; ?>
							@else
								<?php $subArray[$subKey] = Input::old('print_mngt_supplier')[$key][$subKey]; ?>
							@endif
						@endforeach
					@else
						<?php $subArray = Input::old('print_mngt_supplier')[$key]; ?>
					@endif
						<?php $isOld = true; ?>
					

				@elseif($isEdit)
					@if($outsource->submissions !== null)
						<?php $subArray = $outsource->submissions; ?>
					@endif
				@endif
				
				@if(!empty($subArray) && $subArray !== null)
					@foreach($subArray as $subKey => $submission)

						<tr id="submission_{{$outCount}}_{{$subKey + 1}}" class="print_mngt">
							<td colspan="9" style="padding-right: 1.35%">
								<div class="form-group">
									<div class="col-sm-1">
										{!! Form::hidden("subPID[".$key."][".$subKey."]", !$isOld?$submission->id_print_management_submission:null) !!}
									</div>
									<div class="col-sm-3">
										<label class="control-label" style="margin-bottom:0px">{{trans('print.supplier')}}</label>
										{!! Form::text("print_mngt_supplier[".$key."][".$subKey."]", !$isOld?$submission->supplier:null, ['class' => 'form-control print_mngt_supplier', 'required' => 'true', 'onchange' => 'text_change(this)'])!!}
									</div>
									<div class="col-sm-3">
										<label class="control-label" style="margin-bottom:0px">{{trans('print.price')}}</label>
										{!! Form::number("print_mngt_price[".$key."][".$subKey."]", !$isOld?$submission->price:null, ['class' => 'form-control print_mngt_price', 'step'=>'any', 'required' => 'true', 'onchange' =>'text_change(this)'])!!}
									</div>
									<div class="col-sm-1">
										<label class="control-label" style="margin-bottom:0px">{{trans('print.winner')}}</label><br>
										{!! Form::radio("print_mngt_winner[".$key."]", $subKey, !$isOld?$submission->winner:null, ['class' => 'print_mngt_winner', 'onchange' => 'winner_change(this)'])!!}
									</div>
									<div class="col-sm-3">
										<label class="control-label" style="margin-bottom:0px">{{trans('print.comment')}}</label>
										{!! Form::text("print_mngt_comment[".$key."][".$subKey."]", !$isOld?$submission->comments:null, ['class' => 'form-control print_mngt_comment'])!!}
									</div>
									
									@if($subKey == 0)
									<button onclick="add_print_mngt({{$outCount}}, false)" class="remove_input sqrButton float_r" type="button">
										<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
									</button>
										<!--<input type="button" class="remove_input remove_item" value="+" onclick="add_print_mngt({{$outCount}}, false)">-->
									@else
									<button onclick="remove_item('submission', '{{$outCount}}_{{$subKey + 1}}', '')" class="remove_input sqrButton float_r" type="button">
										<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
									</button>
										<!--<input type="button" class="remove_input remove_item" value="x" onclick="remove_item('submission', '{{$outCount}}_{{$subKey + 1}}', '')">-->
									@endif
									
								</div><br><br>
								<div class="form-group" id="sub_documents_{{$outCount}}_{{$subKey + 1}}">
								<div class="col-sm-1">
									</div>
								<div class="col-sm-7">
								@if($isEdit && ((!is_string($submission))?(!$submission->files->isEmpty()):false))

									@foreach($submission->files as $file)


										<a href="http://xeroxweb.ca/laravel/trax_lara/public/index.php/download/{{$file->id_file_upload}}.{{$file->extension}}">{{$file->original_filename}}</a>
										@if($file->file_desc != "")
										<a  data-toggle="tooltip" title="{{ $file->file_desc }}" style="text-decoration:none">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                        </a>
                                        @endif<br>
									@endforeach
									
								@else
									{!! Form::file("sub_file[".$key."][".$subKey."]") !!}
									{!! Form::text("sub_file_desc[".$key."][".$subKey."]", null, ['class' => 'form-control space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
								@endif
									
								</div>
							</div>
							
							<div id="add_new_document"> </div>
							</td>
						</tr>
					@endforeach
				@endif
				<?php $outCount++; ?>
				@endforeach
				
				
				
			</table>
			</div>
		</div>
	</div>
	 </div>