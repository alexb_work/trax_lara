<style>
div.krumo-element {background-color: gray;}
a.ui-state-default {
    box-sizing: unset;
}
.ui-widget-header {background: #4764a5;border: 1px solid #4764a5;}

.radio-inline {
	margin-bottom: 5px !important;
	margin-left: 0px !important;
}

.btn-default.disabled, .btn-default[disabled], fieldset[disabled] .btn-default, .btn-default.disabled:hover, .btn-default[disabled]:hover, fieldset[disabled] .btn-default:hover, .btn-default.disabled:focus, .btn-default[disabled]:focus, fieldset[disabled] .btn-default:focus, .btn-default.disabled:active, .btn-default[disabled]:active, fieldset[disabled] .btn-default:active, .btn-default.disabled.active, .btn-default.active[disabled], fieldset[disabled] .btn-default.active {
	background-color: #4764a5;
}
</style>

    <script type="text/javascript">
    $(function() {
		$('[data-toggle="tooltip"]').tooltip()
	});
    	function target_popup(form) {
    		add_project.action="{{URL::route('displayPrices')}}";
    		window.open('', 'formpopup', 'type=fullWindow,fulscreen,resizeable,scrollbars');
    		form.target = 'formpopup';
		}
		function form_add(form) {
    		add_project.action="{{URL::route('storePrint')}}";
    		form.target = '_self';
		}
		function form_update(form) {
    		add_project.action="{{URL::route('updatePrint')}}";
    		form.target = '_self';
		}
		function set_form_attributes(){
			add_project.onsubmit="target_popup(document.getElementById('add_project'))"; 
			add_project.action="{{URL::route('displayPrices')}}"; 
			return true;
		}

		$( document ).ready(function() {

			$('#p_due_time').timepicker({
			   hours: { starts: 7, ends: 17 },
			   minutes: { interval: 30 },
			   rows: 3,
			   showPeriodLabels: true,
			   minuteText: 'Min'
			});

			$('#p_start_time').timepicker({
			   hours: { starts: 7, ends: 17 },
			   minutes: { interval: 30 },
			   rows: 3,
			   showPeriodLabels: true,
			   minuteText: 'Min'
			});

			$('#p_work_time').timepicker({
			   hours: { starts: 7, ends: 17 },
			   minutes: { interval: 30 },
			   rows: 3,
			   showPeriodLabels: true,
			   minuteText: 'Min'
			});
			
			$( "#received_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			$( "#p_due_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			$( "#p_start_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			$( "#p_work_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});

			var $outsourceTypeTags = $('[name^="outsource_type"]');
			var $outsourceNameTags = $('[name^="outsource_name"]');
			var $outsourceCostTags = $('[name^="outsource_cost"]');

			for(var i = 0; i < $outsourceNameTags.length; i++){
				if($outsourceTypeTags[i].value == 1){
					$outsourceNameTags[i].readOnly = true;
					$outsourceCostTags[i].readOnly = true;
				}
			}
			jQuery(function ($) {        
  				$('form').bind('submit', function () {
    				$(this).find('input:disabled').prop('disabled', false);
  				});
			});
			
			//$.noConflict();
			
		});
		
		
    	function add_reprodoc(){

			var count = parseInt( $('.repro_tr').last().attr('id').split('_')[1] );
			var num = count + 1;

			var $html = $( $('#new_reprography').html() );
				$html.attr('id', 'repro_'+num+'');
				$html.find('.remove_input').attr('onclick','remove_item("repro",'+num+',"repro_finishing")');
				$html.find('.add_finishing').attr('onclick','add_finishing('+num+')');
				// Variables
				$html.find('.p_doc_name').attr('name','p_doc_name['+count+']');
				$html.find('.p_one_sided').attr('name','p_one_sided['+count+']');
				$html.find('.p_double_sided').attr('name','p_double_sided['+count+']');
				$html.find('.p_nb_copies').attr('name','p_nb_copies['+count+']');
				$html.find('.p_supply_desc').attr('name','p_supply_desc['+count+']');
				$html.find('.p_doc_color_1').attr('name','p_doc_color['+count+']');
				$html.find('.p_doc_color_1').attr('id','doc_color_1_'+num+'');
				$html.find('.p_doc_color_1_label').attr('for','doc_color_1_'+num+'');
				$html.find('.p_doc_color_2').attr('name','p_doc_color['+count+']');
				$html.find('.p_doc_color_2').attr('id','doc_color_2_'+num+'');
				$html.find('.p_doc_color_2_label').attr('for','doc_color_2_'+num+'');
				$html.find('.supply_price').attr('name','id_supply_price['+count+']');

				//docdata_
				
				//$html.insertAfter($('#repro tr').last());
				$('#repro > tbody:last').append($html);
		}


    	function add_supply(){
			var count = parseInt( $('#supply tr').last().attr('id').split('_')[1] );
			var num = count + 1;

			var $html = $( $('#new_supply').html() );
				$html.attr('id', 'supply_'+num+'');
				$html.find('.remove_input').attr('onclick','remove_item("supply",'+num+', "")');
				$html.find('.supply_item').attr('name','supply_item['+count+']');	
				$html.find('.supply_qty').attr('name','supply_qty['+count+']');	
				$html.insertAfter($('#supply tr').last());
		}
		
		
		function add_outsourcing(){
			var count = parseInt( $('#outsourcing tr').last().attr('id').split('_')[1] );
			var num = count + 1;

			var $html = $( $('#new_outsourcing').html() );
				$html.attr('id', 'outsourcing_'+num+'');
				$html.find('.remove_input').attr('onclick','remove_item("outsourcing",'+num+', "")');
				$html.find('.outsource_type').attr('name','outsource_type['+count+']');
				$html.find('.outsource_type').attr('onchange','change_outsource_type(this, '+num+', true)');
				$html.find('.outsource_name').attr('name','outsource_name['+count+']');
				$html.find('.outsource_code').attr('name','outsource_code['+count+']');
				$html.find('.outsource_cost').attr('name','outsource_cost['+count+']');
				$html.insertAfter($('#outsourcing tr').last());
		}

		function add_print_mngt(outsourceNum, first){
			if(first){
				var count = 0;
			}
			else{
				var count = parseInt( $('[id^="submission_'+outsourceNum+'"').last().attr('id').split('_')[2] );
			}
			var outsourceCount = outsourceNum -1;
			var num = count + 1;

			var $html = $( $('#new_print_mngt').html() );
				$html.attr('id', 'submission_'+outsourceNum+'_'+num);
			if(first){
				$html.find('.remove_input').attr('onclick','add_print_mngt('+outsourceNum+', false)');
				$html.find('.glyphicon').addClass('glyphicon-plus');
			}
			else{
				$html.find('.remove_input').attr('onclick','remove_item("submission", "'+outsourceNum+'_'+num+'", "")');
				$html.find('.glyphicon').addClass('glyphicon-remove');
			}

				$html.find('.subPID').attr('name','subPID['+outsourceCount+']['+count+']');
				$html.find('.print_mngt_supplier').attr('name','print_mngt_supplier['+outsourceCount+']['+count+']');
				$html.find('.print_mngt_price').attr('name','print_mngt_price['+outsourceCount+']['+count+']');
				$html.find('.print_mngt_winner').attr('name','print_mngt_winner['+outsourceCount+']');
				$html.find('.print_mngt_winner').attr('value', count);
				$html.find('.print_mngt_comment').attr('name','print_mngt_comment['+outsourceCount+']['+count+']');
				$html.find('.sub_file').attr('name','sub_file['+outsourceCount+']['+count+']');
				$html.find('.sub_file_desc').attr('name','sub_file_desc['+outsourceCount+']['+count+']');
			if(first){
				$html.insertAfter($('#outsourcing_'+outsourceNum+''));
			}
			else{
				$html.insertAfter($('#submission_'+outsourceNum+'_'+count).last());
			}
		}

		function change_outsource_type(tag, outsourceNum, first){
			var key = parseInt(outsourceNum) -1;
			if(tag.value == '1'){
				add_print_mngt(outsourceNum, first);
				$('[name="outsource_name['+key+']"').val('');
				$('[name="outsource_cost['+key+']"').val('');
				$('[name="outsource_name['+key+']"').prop("readonly", true);
				$('[name="outsource_cost['+key+']"').prop("readonly", true);

			}
			else{
				$('[id^="submission_'+outsourceNum+'"').remove();
				$('[name="outsource_name['+key+']"').prop("readonly", false);
				$('[name="outsource_cost['+key+']"').prop("readonly", false);
			}
		}

		function winner_change(radio){
			var name = radio.getAttribute("name");
			var outIndex = name.slice(-2).substr(0, 1);
			var subIndex = radio.value;
			
			
			$('[name="outsource_name['+outIndex+']"]').val($('[name="print_mngt_supplier['+outIndex+']['+subIndex+']"]').val());
			$('[name="outsource_cost['+outIndex+']"]').val($('[name="print_mngt_price['+outIndex+']['+subIndex+']"]').val());
		}

		function text_change(textbox){
			var name = textbox.getAttribute("name");
			var outIndex = parseInt(name.slice(-5).substr(0, 1));
			var subIndex = parseInt(name.slice(-2).substr(0, 1));
			if($('[name="print_mngt_winner['+outIndex+']"]:checked').val() == subIndex){
				var suffix = name.substr(11, 1);
	
				if(suffix == 's'){
					$('[name="outsource_name['+outIndex+']"]').val($('[name="print_mngt_supplier['+outIndex+']['+subIndex+']"]').val());
				}
				else if(suffix == 'p'){
					$('[name="outsource_cost['+outIndex+']"]').val($('[name="print_mngt_price['+outIndex+']['+subIndex+']"]').val());
				}
			}


		}


		
		
		function add_documents(){
				//var count = $('#files_block .form-group').size();
				var count = parseInt( $('#collapseDocs .form-group').last().attr('id').split('_')[2] );
				var num = count + 1;
				var $html = $( $('#new_document').html() );
					$html.attr('id', 'new_documents_'+num+'');
					$html.find('input.file-upload').attr('name', 'project_file['+count+']');
					$html.find('input.file-desc').attr('name', 'project_desc['+count+']');
	
					$html.find('input.remove_input').attr('onclick','remove_item("new_documents",'+num+')');
					$('#add_new_document').append($html);
			}					


		function remove_item(leid,num,otherdiv){
			var html = $('#'+leid+'_'+num+'');
			html.remove();
			if(leid == "outsourcing"){
				$('[id^="submission_'+num+'"').remove();
			}

			if ( $("#"+otherdiv+"_"+num+"").length > 0 ){
				$("#"+otherdiv+"_"+num+"").remove();
			}

		}		
			
		function add_finishing(num){
			var $html = $( $('#new_finishing').html() );
			var condition = false;
			if(num == 'global'){
				var repro_finishing_length = -1;
				var count = num;
				if($('#add_finish_button').hasClass('glyphicon-plus'))
					condition = true;
			}
			else{
				var repro_finishing_length = $("#repro_finishing_"+num+"").length;
				var count = num-1;
				$html.find(".col-md-12").prepend('<div class="panel panel-default"><div class="panel-heading" id="headingReproFinish_'+num+'" role="tab"><h4 class="panel-title">	<a role="button" data-toggle="collapse" data-parent="#repro_finishing_'+num+'" href="#accordion_finishing_'+num+'" aria-controls="#accordion_finishing_'+num+'" aria-expanded="true">Finishing</a></h4></div>');
				$html.find(".panel-group").after("</div>");
				if(repro_finishing_length == 0)
					condition = true;
			}
			
			
			if (condition){
				//Add finishing
				//console.log('add');
				$html.attr('id', 'repro_finishing_'+num+'');
				//variable
				
				$html.find('.panel-group').attr('id','accordion_finishing_'+num+'');
				
				$html.find('.accordion_1').attr('data-parent','accordion_1_'+num+'');
				$html.find('.accordion_2').attr('data-parent','accordion_2_'+num+'');
				$html.find('.accordion_3').attr('data-parent','accordion_3_'+num+'');
				$html.find('.accordion_4').attr('data-parent','accordion_4_'+num+'');
				$html.find('.accordion_5').attr('data-parent','accordion_5_'+num+'');
				$html.find('.accordion_6').attr('data-parent','accordion_6_'+num+'');
				$html.find('.accordion_7').attr('data-parent','accordion_7_'+num+'');
				$html.find('.accordion_8').attr('data-parent','accordion_8_'+num+'');
				$html.find('.accordion_9').attr('data-parent','accordion_9_'+num+'');
				$html.find('.accordion_10').attr('data-parent','accordion_10_'+num+'');
				$html.find('.accordion_11').attr('data-parent','accordion_11_'+num+'');
				$html.find('.accordion_12').attr('data-parent','accordion_12_'+num+'');
				
				
				$html.find('.headingStapled').attr('id','headingStapled_'+num+'');
				$html.find('.accordion_1').attr('href','#collapseStapled_'+num+'');
				$html.find('.accordion_1').attr('aria-controls','collapseStapled_'+num+'');
				$html.find('.panelCollapseStapled').attr('aria-labelledby','headingStapled_'+num+'');
				$html.find('.panelCollapseStapled').attr('id','collapseStapled_'+num+'');


				$html.find('.headingBooklet').attr('id','headingBooklet_'+num+'');
				$html.find('.accordion_2').attr('href','#collapseBooklet_'+num+'');
				$html.find('.accordion_2').attr('aria-controls','collapseBooklet_'+num+'');
				$html.find('.panelCollapseBooklet').attr('aria-labelledby','headingBooklet_'+num+'');
				$html.find('.panelCollapseBooklet').attr('id','collapseBooklet_'+num+'');


				$html.find('.headingSpiral').attr('id','headingSpiral_'+num+'');
				$html.find('.accordion_3').attr('href','#collapseSpiral_'+num+'');
				$html.find('.accordion_3').attr('aria-controls','collapseSpiral_'+num+'');
				$html.find('.panelCollapseSpiral').attr('aria-labelledby','headingSpiral_'+num+'');
				$html.find('.panelCollapseSpiral').attr('id','collapseSpiral_'+num+'');
			
				$html.find('.headingBinder').attr('id','headingBinder_'+num+'');
				$html.find('.accordion_5').attr('href','#collapseBinder_'+num+'');
				$html.find('.accordion_5').attr('aria-controls','collapseBinder_'+num+'');
				$html.find('.panelCollapseBinder').attr('aria-labelledby','headingBinder_'+num+'');
				$html.find('.panelCollapseBinder').attr('id','collapseBinder_'+num+'');


				$html.find('.headingDrilling').attr('id','headingDrilling_'+num+'');
				$html.find('.accordion_6').attr('href','#collapseDrilling_'+num+'');
				$html.find('.accordion_6').attr('aria-controls','collapseDrilling_'+num+'');
				$html.find('.panelCollapseDrilling').attr('aria-labelledby','headingDrilling_'+num+'');
				$html.find('.panelCollapseDrilling').attr('id','collapseDrilling_'+num+'');


				$html.find('.headingCuts').attr('id','headingCuts_'+num+'');
				$html.find('.accordion_7').attr('href','#collapseCuts_'+num+'');
				$html.find('.accordion_7').attr('aria-controls','collapseCuts_'+num+'');
				$html.find('.panelheadingCuts').attr('aria-labelledby','headingCuts_'+num+'');
				$html.find('.panelheadingCuts').attr('id','collapseCuts_'+num+'');


				$html.find('.headingPads').attr('id','headingPads_'+num+'');
				$html.find('.accordion_8').attr('href','#collapsePads_'+num+'');
				$html.find('.accordion_8').attr('aria-controls','collapsePads_'+num+'');
				$html.find('.panelPads').attr('aria-labelledby','headingPads_'+num+'');
				$html.find('.panelPads').attr('id','collapsePads_'+num+'');


				$html.find('.headingFolds').attr('id','headingFolds_'+num+'');
				$html.find('.accordion_9').attr('href','#collapseFolds_'+num+'');
				$html.find('.accordion_9').attr('aria-controls','collapseFolds_'+num+'');
				$html.find('.panelFolds').attr('aria-labelledby','headingFolds_'+num+'');
				$html.find('.panelFolds').attr('id','collapseFolds_'+num+'');


				$html.find('.headingDistribution').attr('id','headingDistribution_'+num+'');
				$html.find('.accordion_10').attr('href','#collapseDistribution_'+num+'');
				$html.find('.accordion_10').attr('aria-controls','collapseDistribution_'+num+'');
				$html.find('.panelDistribution').attr('aria-labelledby','headingDistribution_'+num+'');
				$html.find('.panelDistribution').attr('id','collapseDistribution_'+num+'');


				$html.find('.headingAssembly').attr('id','headingAssembly_'+num+'');
				$html.find('.accordion_11').attr('href','#collapseAssembly_'+num+'');
				$html.find('.accordion_11').attr('aria-controls','collapseAssembly_'+num+'');
				$html.find('.panelAssembly').attr('aria-labelledby','headingAssembly_'+num+'');
				$html.find('.panelAssembly').attr('id','collapseAssembly_'+num+'');


				//Stapled
				$html.find('.stapled_type').attr('name','id_stapled_type['+count+']');				
				$html.find('.stapled_79sheets').attr('name','s_over79['+count+']');
				$html.find('.stapled_79sheets').attr('id','stapled_79sheets_num_'+num+'');
				$html.find('.stapled_79sheets_label').attr('for','stapled_79sheets_num_'+num+'');
				$html.find('.stapled_machine').attr('name','s_machine['+count+']');
				$html.find('.stapled_machine').attr('id','stapled_machine_num_'+num+'');
				$html.find('.stapled_machine_label').attr('for','stapled_machine_num_'+num+'');
				$html.find('.stapled_qty').attr('name','s_quantity['+count+']');				


				//Booklet
				$html.find('.booklet_qty').attr('name','book_qty['+count+']');				
				$html.find('.booklet_sheet_qty').attr('name','book_sheets['+count+']');
				$html.find('.booklet_size').attr('name','book_size['+count+']');				

				//Spiral / Cerlox
				$html.find('.spiral_type').attr('name','sc_type['+count+']');	
				$html.find('.spiral_qty').attr('name','sc_book_qty['+count+']');				
				$html.find('.spiral_sheet_qty').attr('name','sc_book_sheets['+count+']');
				$html.find('.spiral_color').attr('name','sc_color['+count+']');				
				$html.find('.spiral_cover').attr('name','sc_cover['+count+']');
				$html.find('.spiral_back').attr('name','sc_back['+count+']');				

				
				//Binder
				$html.find('.binder_customer').attr('name','bind_cust['+count+']');
				$html.find('.binder_customer').attr('id','binder_customer_num_'+num+'');
				$html.find('.binder_customer_label').attr('for','binder_customer_num_'+num+'');
				$html.find('.binder_cover').attr('name','bind_cover['+count+']');
				$html.find('.binder_cover').attr('id','binder_cover_num_'+num+'');
				$html.find('.binder_cover_label').attr('for','binder_cover_num_'+num+'');				
				$html.find('.binder_spine').attr('name','bind_spine['+count+']');
				$html.find('.binder_spine').attr('id','binder_spine_num_'+num+'');
				$html.find('.binder_spine_label').attr('for','binder_spine_num_'+num+'');
				$html.find('.binder_qty').attr('name','bind_ins_qty['+count+']');
				$html.find('.binder_comment').attr('name','bind_comment['+count+']');


				//Drilling
				$html.find('.drilling_holes').attr('name','drill_holes['+count+']');
				$html.find('.drilling_sheets').attr('name','drill_sheets['+count+']');				


				//Cuts
				$html.find('.cuts_size').attr('name','cut_final_size['+count+']');
				$html.find('.cuts_qty').attr('name','cut_qty['+count+']');		
				$html.find('.cuts_sheets_qty').attr('name','cut_sheet_qty['+count+']');



				//Pads
				$html.find('.pads_size').attr('name','pad_size['+count+']');
				$html.find('.pads_qty').attr('name','pad_qty['+count+']');		
				$html.find('.pads_sheets_qty').attr('name','pad_sheet_qty['+count+']');


				//Folds
				$html.find('.folds_qty').attr('name','fold_qty['+count+']');
				$html.find('.folds_sheets_qty').attr('name','fold_sheet_qty['+count+']');		
				$html.find('.folds_method1').attr('name','fold_method['+count+']');
				$html.find('.folds_method1').attr('id','folds_method1_num_'+num+'');
				$html.find('.folds_method1_label').attr('for','folds_method1_num_'+num+'');
				$html.find('.folds_method2').attr('name','fold_method['+count+']');
				$html.find('.folds_method2').attr('id','folds_method2_num_'+num+'');
				$html.find('.folds_method2_label').attr('for','folds_method2_num_'+num+'');
				$html.find('.folds_shape').attr('name','fold_shape['+count+']');


				//Distribution
				$html.find('.distribution_env_size').attr('name','insert_env_size['+count+']');
				$html.find('.distribution_qty').attr('name','insert_qty['+count+']');		
				$html.find('.distribution_method1').attr('name','insert_method['+count+']');
				$html.find('.distribution_method1').attr('id','distribution_method1_num_'+num+'');
				$html.find('.distribution_method1_label').attr('for','distribution_method1_num_'+num+'');
				$html.find('.distribution_method2').attr('name','insert_method['+count+']');
				$html.find('.distribution_method2').attr('id','distribution_method2_num_'+num+'');
				$html.find('.distribution_method2_label').attr('for','distribution_method2_num_'+num+'');
				$html.find('.distribution_labeling').attr('name','insert_label_qty['+count+']');
				$html.find('.distribution_labeling').attr('id','distribution_labeling_num_'+num+'');
				$html.find('.distribution_labeling_label').attr('for','distribution_labeling_num_'+num+'');				
				$html.find('.distribution_manual_sealing').attr('name','insert_man_seal['+count+']');
				$html.find('.distribution_manual_sealing').attr('id','distribution_manual_sealing_num_'+num+'');
				$html.find('.distribution_manual_sealing_label').attr('for','distribution_manual_sealing_num_'+num+'');			
				$html.find('.distribution_canpost').attr('name','insert_canada_post['+count+']');
				$html.find('.distribution_canpost').attr('id','distribution_canpost_num_'+num+'');
				$html.find('.distribution_canpost_label').attr('for','distribution_canpost_num_'+num+'');	
				$html.find('.distribution_intmail').attr('name','insert_internal_mail['+count+']');
				$html.find('.distribution_intmail').attr('id','distribution_intmail_num_'+num+'');
				$html.find('.distribution_intmail_label').attr('for','distribution_intmail_num_'+num+'');
				


				//Assembly
				$html.find('.assembly_method1').attr('name','assembly_collat_method['+count+']');
				$html.find('.assembly_method1').attr('id','assembly_method1_num_'+num+'');
				$html.find('.assembly_method1_label').attr('for','assembly_method1_num_'+num+'');
				$html.find('.assembly_method2').attr('name','assembly_collat_method['+count+']');
				$html.find('.assembly_method2').attr('id','assembly_method2_num_'+num+'');
				$html.find('.assembly_method2_label').attr('for','assembly_method2_num_'+num+'');
				$html.find('.assembly_qty').attr('name','assembly_quantity_lift['+count+']');

				if(num == 'global'){
					$("#global_finition").html($html);
					$('#add_finish_button').removeClass('glyphicon-plus');
					$('#add_finish_button').addClass('glyphicon-minus');
				}
				else{
					$html.insertAfter($('#repro_'+num+'').last());
				}
							

			}
			else{
				//Remove finishing
				//console.log('remove');	
				if(num == 'global'){
					$("#global_finition").empty();
					$('#add_finish_button').removeClass('glyphicon-minus');
					$('#add_finish_button').addClass('glyphicon-plus');
				}	
				else{		
					var html = $('#repro_finishing_'+num+'');
					html.remove();
				}
			}
			
		}
		
		function add_global_finition(){
			add_finishing('global');
			/*var $html = $( $('#new_global_finishing').html() );
			
			if ($('#add_finish_button').val() == '-'){
				$("#global_finition").empty();
				$('#add_finish_button').val('+')
			}
			else{
				$("#global_finition").html($html);
				$('#add_finish_button').val('-')
			}*/

			
		}
		
		
	</script>	

	<script type="text/template" id="new_print_mngt">
		<tr class="print_mngt">
			<td colspan="9"  style="padding-right: 1.35%">
				<div class="form-group">
					<div class="col-sm-1">
						{!! Form::hidden("subPID_name", 'new', ['class' => 'subPID']) !!}
					</div>
						
					<div class="col-sm-3">
						<label class="control-label" style="margin-bottom:0px">{{trans('print.supplier')}}</label>
						{!! Form::text("print_mngt_supplier_name", null, ['class' => 'form-control print_mngt_supplier', 'required' => 'true', 'onchange' => 'text_change(this)'])!!}
					</div>
					<div class="col-sm-3">
						<label class="control-label" style="margin-bottom:0px">{{trans('print.price')}}</label>
						{!! Form::number("print_mngt_price_name", null, ['class' => 'form-control print_mngt_price', 'step'=>'any', 'required' => 'true', 'onchange' => 'text_change(this)'])!!}
					</div>
					<div class="col-sm-1">
						<label class="control-label" style="margin-bottom:0px">{{trans('print.winner')}}</label><br>
						{!! Form::radio("print_mngt_winner_name", 'true', false, ['class' => 'print_mngt_winner', 'onchange' => 'winner_change(this)'])!!}
					</div>
					<div class="col-sm-3">
						<label class="control-label" style="margin-bottom:0px">{{trans('print.comment')}}</label>
						{!! Form::text("print_mngt_comment_name", null, ['class' => 'form-control print_mngt_comment'])!!}
					</div>
					<button class="remove_input sqrButton float_r" type="button">
						<span aria-hidden="true" class="glyphicon"></span>
					</button>
				</div><br><br>
				<div class="form-group">
				<div class="col-sm-1">
									</div>
				<div class="col-sm-7">
					{!! Form::file('sub_file_name', ['class' => 'sub_file']) !!}
					{!! Form::text("sub_file_desc_name", null, ['class' => 'form-control space-bottom sub_file_desc', 'placeholder'=>trans('add_task.fileDesc')])!!}
				</div>
				</div>
			</td>
		</tr>
	</script>

	<script type="text/template" id="new_document">
	
		<div class="form-group"><br>
			<label class="col-sm-3 control-label">
				<input class="remove_input" type="button" value="X">
			</label>
			<div class="col-sm-7">
				{!! Form::file('project_file_name', ['class' => 'file-upload']) !!}
				{!! Form::text("project_desc_name", null, ['class' => 'form-control file-desc space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
	
			</div><br>
		  </div>
  	
  	</script>
	
	
	<script type="text/template" id="new_supply">
		<tr>
			<td>
				{!! Form::select('supply_item_name', $supplies, null, ['class'=>'supply_item']) !!}
			</td>
			<td>
				{!! Form::number("supply_qty_name", null, ['class'=>'supply_qty'])!!}
			</td>
			<td class="text-center">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
										
			</td>
		</tr>
	</script>


	<script type="text/template" id="new_outsourcing">
		<tr>
		
			<td>
				{!! Form::select('outsource_type_name', $outsources, null, ['class' => 'outsource_type']) !!}
			</td>
			<td>
				{!! Form::text("outsource_name_name", null, ['class' => 'outsource_name'])!!}
			</td>

			<td>
				{!! Form::text("outsource_code_name", null, ['class' => 'outsource_code'])!!}
			</td>
			
			<td>
				{!! Form::text("outsource_cost_name", null, ['class' => 'outsource_cost'])!!}

			</td>
			<td class="text-center">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
			</td>
		
		</tr>
	</script>
		
	
	<script type="text/template" id="new_reprography">

		<tr class="repro_tr repro">
			<td>
				{!! Form::text("p_doc_name_name", null, ['class' => 'form-control p_doc_name'])!!}
			</td>
			<td>
				{!! Form::number("p_one_sided_name", null, ['class' => 'form-control p_one_sided'])!!}
			</td>
			<td>
				{!! Form::number("p_double_sided_name", null, ['class' => 'form-control p_double_sided'])!!}
			</td>
			<td>
				{!! Form::number("p_nb_copies_name", null, ['class' => 'form-control p_nb_copies'])!!}
			</td>
			<td>
			{!! Form::select('supply_price_name[]', $paper, null, ['class'=>'form-control supply_price']) !!}
												
			</td>

			<td>
				@foreach ($colors as $key => $color)

									<label class="label_margin">  {!! Form::radio('color_name[]', $key, false, ['class' => "p_doc_color_$key", 'id'=>"doc_color_$key"]) !!} <label for="doc_color_{{$key}}" class="p_doc_color_label"></label>
										{{$color}}
                               		</label>
								@endforeach		
				
			</td>


			<td class="text-center">
				<button type="button" class="btn btn-default add_finishing">
					{{ trans('print.addDelFinish') }}
				</button>				
			</td>
			<td class="text-center">
				<button type="button" class="remove_input sqrButton">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
					
			</td>			
		</tr>
				
  	</script>	
  	
  	<script type="text/template" id="new_finishing">
  	
		<!-- Finition -->	
		<tr class="repro_finishing repro">
			<td colspan="9">
				<div class="row">
					<div class="col-md-12">
						
						<div class="panel-group" role="tablist" aria-multiselectable="true">

							<div class="panel panel-default">
								<div class="panel-heading headingStapled" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_1" aria-expanded="true">
										  {{ trans('print.stapled') }}
										</a>
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseStapled" role="tabpanel" >
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.type') }}</th>
												<th>Machine</th>
												<th>{{ trans('print.79sheets') }}</th>
												<th>{{ trans('print.quantity') }}</th>
											</tr>
						
											<tr>
												<td>
												{!! Form::select('stapled_type_name_name[]', $stapledTypes, null, ['class' => 'stapled_type']) !!}
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('stapled_machine_name', 1, false, ['class' => 'stapled_machine']) !!} 
														<label for="" class="stapled_machine_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('stapled_79sheets_name', 1, false, ['class' => 'stapled_79sheets']) !!} 
														<label for="" class="stapled_79sheets_label"></label>
                               						</label>
												</td>
												<td>{!! Form::number("stapled_qty_name", null, ['class' => 'stapled_qty'])!!}</td>
											</tr>
										</table>

									</div>
								
								</div>
							</div>
						
						
						
							<div class="panel panel-default">
								<div class="panel-heading headingBooklet" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_2" aria-expanded="true" >
										  {{ trans('print.booklet') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseBooklet" role="tabpanel">
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.qtyBooks') }}</th>
												<th colspan="2">{{ trans('print.qtySheets') }}</th>
												<th>{{ trans('print.sizeClose') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">{!! Form::number("booklet_qty_name", null, ['class' => 'booklet_qty'])!!} </td>
												<td colspan="2">{!! Form::number("booklet_sheet_qty_name", null, ['class' => 'booklet_sheet_qty'])!!}</td>
												<td>{!! Form::text("booklet_size_name", null, ['class' => 'booklet_size'])!!}</td>
											</tr>
											
										</table>										

									</div>
								
								</div>
							</div>
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingSpiral" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_3"  aria-expanded="true">
										  {{ trans('print.spiral') }}/{{ trans('print.cerlox') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseSpiral" role="tabpanel">
									<div class="panel-body"> 
										
										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.type') }}</th>
												<th>{{ trans('print.qtyBooks') }}</th>
												<th>{{ trans('print.qtySheets') }}</th>
												<th>{{ trans('print.color') }}</th>
												<th>{{ trans('print.cover') }}</th>
												<th>{{ trans('print.back') }}</th>
											</tr>
						
											<tr>
												<td>
													{!! Form::select('spiral_type_name', array(
														0 => '---',
														'spiral' => trans('print.spiral'),
														'cerlox' => trans('print.cerlox')), 
														"", ['class'=>'spiral_type']) 
													!!}													
												</td>
												<td>{!! Form::number("spiral_qty_name", null, ['class' => 'spiral_qty'])!!}</td>
												<td>{!! Form::number("spiral_sheet_qty_name", null, ['class' => 'spiral_sheet_qty'])!!}</td>
												<td>{!! Form::text("spiral_color_name", null, ['class' => 'spiral_color'])!!}</td>
						
												<td>{!! Form::text("spiral_cover_name", null, ['class' => 'spiral_cover'])!!}</td>
												<td>{!! Form::text("spiral_back_name", null, ['class' => 'spiral_back'])!!}</td>
											</tr>
											
										</table>									

									</div>
								
								</div>
							</div>
							

							
							
											
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingBinder" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_5" aria-expanded="true" >
										  {{ trans('print.binder') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseBinder" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th>{{ trans('print.suppliedByCust') }}</th>
												<th>{{ trans('print.cover') }}</th>
												<th>{{ trans('print.spine') }}</th>
												<th>{{ trans('print.insert') }}</th>
												<th>{{ trans('print.comment') }}</th>
											</tr>
						
											<tr>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('binder_customer_name', 1, false, ['class' => 'binder_customer']) !!} 
														<label for="" class="binder_customer_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('binder_cover_name', 1, false, ['class' => 'binder_cover']) !!} 
														<label for="" class="binder_cover_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('binder_spine_name', 1, false, ['class' => 'binder_spine']) !!} 
														<label for="" class="binder_spine_label"></label>
                               						</label>
												</td>
												<td>
													{!! Form::number("binder_qty_name", null, ['class' => 'binder_qty'])!!}
												</td>
												<td>
													{!! Form::textarea("binder_comment_name", null, ['class' => 'binder_comment', 'style' => 'width: 100%;height: 60px;'])!!}
												</td>
											</tr>
											
										</table>											


									</div>
								</div>
							</div>




							<div class="panel panel-default">
								<div class="panel-heading headingDrilling" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_6" aria-expanded="true" >
										  {{ trans('print.drill') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelCollapseDrilling" role="tabpanel" >
									<div class="panel-body">

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.holes') }}</th>
												<th colspan="2">{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::number("drilling_holes_name", null, ['class' => 'drilling_holes'])!!}
												</td>
												<td colspan="2">
													{!! Form::number("drilling_sheets_name", null, ['class' => 'drilling_sheets'])!!}
												</td>
											</tr>
										</table>


									</div>
								</div>
							</div>
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingCuts" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_7"  aria-expanded="true">
										  {{ trans('print.cuts') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelheadingCuts" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.finalCutSize') }}</th>
												<th colspan="">{{ trans('print.cutsQty') }}</th>
												<th colspan="">{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::text("cuts_size_name", null, ['class' => 'cuts_size'])!!}
												</td>
												<td>
													{!! Form::number("cuts_qty_name", null, ['class' => 'cuts_qty'])!!}
												</td>
												<td>
													{!! Form::number("cuts_sheets_qty_name", null, ['class' => 'cuts_sheets_qty'])!!}
												</td>
											</tr>
						
										</table>



									</div>
								</div>
							</div>
							
							

							<div class="panel panel-default">
								<div class="panel-heading headingPads" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_8"  aria-expanded="true">
										  {{ trans('print.pads') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelPads" role="tabpanel">
									<div class="panel-body"> 

										<table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.size') }}</th>
												<th>{{ trans('print.padsQty') }}</th>
												<th>{{ trans('print.sheetsQty') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::select("pads_size_name", array(
														0 => '---',
														'4.5 X 5.5' => '4.5 X 5.5',
														'8.5 X 11' => '8.5 X 11',
														'8.5 X 14' => '8.5 X 14',
														'8.5 X 5.5' => '8.5 X 5.5'),
														"", ['class'=>'pads_size']) 
													!!}
												</td>
												<td>
													{!! Form::number("pads_qty_name", null, ['class' => 'pads_qty'])!!}
												</td>
						                        <td>
						                        	{!! Form::number("pads_sheets_qty_name", null, ['class' => 'pads_sheets_qty'])!!}
												</td>
											</tr>
										</table>


									</div>
								</div>
							</div>
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingFolds" role="tab" >
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_9"  aria-expanded="true" >
										  {{ trans('print.folds') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelFolds" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
											<tr>
												<th colspan="2">{{ trans('print.foldsQty') }}</th>
												<th>{{ trans('print.sheetsQty') }}</th>
												<th>{{ trans('print.method') }}</th>
												<th>{{ trans('print.shape') }}</th>
											</tr>
						
											<tr>
												<td colspan="2">
													{!! Form::number("folds_qty_name", null, ['class' => 'folds_qty'])!!}
												</td>
												<td>
													{!! Form::number("folds_sheets_qty_name", null, ['class' => 'folds_sheets_qty'])!!}
												</td>
						                        
						                        <td>
						                        <label style="margin-right:25px;" class="label_margin"> 
						                        {!! Form::radio('folds_method1_name', 'machine', false, ['class' => 'folds_method1']) !!}
												<label for="" class="folds_method_label1"></label> Machine				
												</label>|
												<label style="margin-right:25px;" class="label_margin"> 
						                        {!! Form::radio('folds_method2_name', 'manual', false, ['class' => 'folds_method2']) !!}
												<label for="" class="folds_method_label2"></label> {{ trans('print.manual') }}				
												</label>

												</td>
						                        <td>
						                        	{!! Form::text("folds_shape_name", null, ['class' => 'folds_shape'])!!}
												</td>
											</tr>
										</table>
										
									</div>
								</div>
							</div>
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingDistribution" role="tab" >
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_10"  aria-expanded="true">
										  {{ trans('print.insertion') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelDistribution" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
											<tr>
												<th >{{ trans('print.envSize') }}</th>
												<th >{{ trans('print.(qty)') }}</th>
												<th >{{ trans('print.method') }}</th>
												<th >{{ trans('print.labelQty') }}</th>
												<th >{{ trans('print.manSeal') }}</th>
												<th >{{ trans('print.canadaPost') }}</th>
						                        <th >{{ trans('print.internalMail') }}</th>
											</tr>
						
											<tr>
						                        <td>
						                        	{!! Form::text("distribution_env_size_name", null, ['class' => 'distribution_env_size'])!!}
												</td>
												<td>
													{!! Form::number("distribution_qty_name", null, ['class' => 'distribution_qty'])!!}
												</td>
												 <td>
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio('distribution_method1_name', 'machine', false, ['class' => 'distribution_method1']) !!}
													<label for="" class="distribution_method_label1"></label> Machine				
													</label>|
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio('distribution_method2_name', 'manual', false, ['class' => 'distribution_method2']) !!}
													<label for="" class="distribution_method_label2"></label> {{ trans('print.manual') }}				
													</label>
												</td>
												 <td>
													<label class="label_margin">  
														{!! Form::checkbox('distribution_labeling_name', 1, false, ['class' => 'distribution_labeling']) !!} 
														<label for="" class="distribution_labeling_label"></label>
                               						</label>
												</td>
												 <td>
												 	<label class="label_margin">  
														{!! Form::checkbox('distribution_manual_sealing_name', 1, false, ['class' => 'distribution_manual_sealing']) !!} 
														<label for="" class="distribution_manual_sealing_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('distribution_canpost_name', 1, false, ['class' => 'distribution_canpost']) !!} 
														<label for="" class="distribution_canpost_label"></label>
                               						</label>
												</td>
												<td>
													<label class="label_margin">  
														{!! Form::checkbox('distribution_intmail_name', 1, false, ['class' => 'distribution_intmail']) !!} 
														<label for="" class="distribution_intmail_label"></label>
                               						</label>
												</td>
											</tr>
										</table>
										
									</div>
								</div>
							</div>
							
							
							
							<div class="panel panel-default">
								<div class="panel-heading headingAssembly" role="tab">
								  <h4 class="panel-title">	
										<a role="button" data-toggle="collapse" class="accordion_11"  aria-expanded="true" >
										  {{ trans('print.assembly') }}
										</a>									
								  </h4>
								</div>
								
								<div class="panel-collapse collapse panelAssembly" role="tabpanel">
									<div class="panel-body"> 
										
						                <table class="table table-condensed">
												<tr>
													<th colspan="2">{{ trans('print.collating') }}</th>
													<th>{{ trans('print.qtyLift') }}</th>
												</tr>
						
												<tr>
													<td colspan="2">
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio('assembly_method1_name', 'machine', false, ['class' => 'assembly_method1']) !!}
													<label for="" class="assembly_method_label1"></label> Machine				
													</label>|
													<label style="margin-right:25px;" class="label_margin"> 
						                       	 	{!! Form::radio('assembly_method2_name', 'manual', false, ['class' => 'assembly_method2']) !!}
													<label for="" class="assembly_method_label2"></label> {{ trans('print.manual') }}				
													</label>										
													</td>
						                               <td>
						                               	{!! Form::number("assembly_qty_name", null, ['class' => 'assembly_qty'])!!}
													</td>
												</tr>
										</table>
										
									</div>
								</div>
							</div>
							
							
							
							
							

						</div><!-- end accordion_finishing -->
						

					</div>
				</div>
			</td>
		</tr>
		<!-- Fin Finition -->
  	</script>
  	
  	