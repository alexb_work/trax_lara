<?php
	use Illuminate\Support\Facades\Input;
?>
<div class="panel panel-default">
	<div class="panel-heading">
	  <h4 class="panel-title">
		<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		  {{ trans('print.repro') }}
		</a>
	  </h4>
	</div>
	<div id="collapseThree" class="panel-collapse collapse">
	  <div class="panel-body">
	  
	  
		  <div class="form-group">
			<label class="col-sm-3 control-label">{{ trans('print.scan') }}</label>
			<div class="col-sm-7">
				<label><input name="p_scanning" value="1" type="checkbox" /> {{ trans('print.yes') }} </label>
			</div>
		  </div>						  
		  				  
	
	
		  <div class="form-group">
			  <div class="col-md-12">
				  <div class="table-responsive">
				  
					<table class="table table-striped table-bordered table-condensed table-hover" id="repro">
						
						<tr id="repro_0" class="repro_tr repro">
							<th style="width:12%">{{ trans('print.docName') }}</th>
							<th style="width:12%">{{ trans('print.oneSided') }}</th>
							<th style="width:12%">{{ trans('print.dblSided') }}</th>
							<th style="width:12%">{{ trans('print.copies') }}</th>
							<th style="width:25%">{{ trans('print.paperSupport') }}</th>
							<th style="width:10%">{{ trans('print.type') }}</th>
							<th style="width:12%">{{ trans('print.action') }}</th>
							<th class="text-center buttonHeader">
								<button type="button" class="remove_input sqrButton" onclick="add_reprodoc();">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								</button>
							</th>
						</tr>
						<?php $reproCount = 1; 
							$reproArray = array();?>
						@if($isEdit)
						<?php $reproArray = $print_job->reprographies; ?>
						@elseif(Input::old('p_doc_name')!==null)
						<?php $reproArray = Input::old('p_doc_name'); ?>
						@endif
						@foreach($reproArray as $key=>$repro)

							<tr class="repro_tr repro" id="repro_{{$reproCount}}">
								<td>
									{!! Form::text("p_doc_name[$key]", $isEdit?$repro->doc_name:null, ['class' => 'form-control p_doc_name'])!!}
								</td>
								<td>
									{!! Form::number("p_one_sided[$key]", $isEdit?$repro->oneSided:null, ['class' => 'form-control p_one_sided'])!!}
								</td>
								<td>
									{!! Form::number("p_double_sided[$key]", $isEdit?$repro->dblSided:null, ['class' => 'form-control p_double_sided'])!!}
								</td>
								<td>
									{!! Form::number("p_nb_copies[$key]", $isEdit?$repro->quantity:null, ['class' => 'form-control p_nb_copies'])!!}
								</td>
								<td>
									{!! Form::select("id_supply_price[$key]", $paper, $isEdit?$repro->supply_code:null, ['class'=>'form-control']) !!}
																	
								</td>
					
								<td>
									@foreach ($colors as $index => $color)
					
										<label class="label_margin">  {!! Form::radio("p_doc_color[$key]", $index, $isEdit?$repro->id_color_type==$index:null , ['class' => 'p_doc_color', 'id'=>"doc_color_".$index."_$key"]) !!} <label for="doc_color_{{$index}}_{{$key}}"></label>
											{{$color}}
            					        </label>
									@endforeach		
									
								</td>
					
					
								<td class="text-center">
									<button type="button" onclick="add_finishing({{$reproCount}})" class="btn btn-default add_finishing">
										{{ trans('print.addDelFinish') }}
									</button>				
								</td>
								<td class="text-center">
									<button type="button" class="remove_input sqrButton" onclick="remove_item('repro', {{$reproCount}}, 'repro_finishing')">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
									
										
								</td>	
							</tr>
							@if(Input::old('id_stapled_type')!==null)
								@if(array_key_exists($key, Input::old('id_stapled_type')))
									<?php $fIndex = $reproCount - 1; 
										$fNum = $reproCount; 
									?>
									@include('pages.print.finish')
								@endif
							@elseif($repro->finition !== null)
								<?php $fIndex = $reproCount - 1; 
									$fNum = $reproCount; 
									$finish = $repro->finition;
								?>
								@include('pages.print.finish')
							@endif

							<?php $reproCount++; ?>
						@endforeach
					</table>
					</div><!--end table-responsive -->
				</div>
			</div>
	  	</div>
	</div>				
</div>