@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?php
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Storage;
?>
<?php
  	if(isset($project)){
  		$count = count($project->cost_centers);
  		$otherJob = $project->other_job_type;
  	}
  	else{
  		$count = 0;
  		$otherJob = null;
  	}
?>


<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					  <h4 class="panel-title">
							{{ trans('add_job.general') }}
					  </h4>
					  </a>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
					  <div class="panel-body">
						@unless($isEdit)
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.importTemp') }}</label>
							<div class="col-sm-7">
								<input class="import_template" id="import_template" type="checkbox" value="1" name="import_template" >
								
								<select id="select_template" name="select_template" onchange="template_fill();" style="display:none">
									<option value="0"> --- </option>
								@foreach ($templates as $template)
									<option value="{{$template->id_project}}">{{$template->project_name}}</option>
								
								@endforeach
							  
								</select>
								
							</div>
						  </div>
						  @endunless
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projectName') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" id="project_name" name="project_name"  value = "{{Input::old('project_name')}}" class="form-control" placeholder="{{ trans('add_job.projectName') }}" required>-->
							  {!! Form::text("project_name", null, ['id' => 'project_name', 'name' => 'project_name', 'required' => 'true', 'class' => 'form-control', 'placeholder' => trans('add_job.projectName')])!!}
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projectType') }}</label>
							<div class="col-sm-7">
							 <!-- <select name="project_type" selected="{{Input::old('project_type')}}" id="project_type">
								<option value="interne">Xerox - {{ trans('add_job.internal') }}</option>
								<option value="externe">{{ trans('add_job.bell') }}</option>
								<option value="intersite">{{ trans('add_job.intersite') }}</option>
							  </select>-->
							{!! Form::select('project_type', array(
							  	'interne' => 'Xerox - '.trans('add_job.internal'),
							  	'externe' => trans('add_job.bell'),
							  	'intersite' => trans('add_job.intersite')), 
							  	isset($project)?$project->project_type:"") 
							 !!}
							</div>
						  </div>	
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.division') }}</label>
							<div class="col-sm-7">

							{!! Form::select('id_division', $divisions, null) !!}


							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.province') }}</label>
							<div class="col-sm-7">
							{!! Form::select('id_province', $provinces, null) !!}

							
								
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.reqCompDate') }}</label>
							<div class="col-sm-7">
								<!--<input name="end_date" value = "{{Input::old('end_date')}}" type="text" id="end_date" required /> | <input type="text" value = "{{Input::old('end_time')}}" name="end_time" id="end_time" required />-->
								{!! Form::text("end_date", null, ['id' => 'end_date', 'name' => 'end_date', 'required' => 'true'])!!}
								{!! Form::text("end_time", null, ['id' => 'end_time', 'name' => 'end_time', 'required' => 'true'])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.delDate') }}</label>
							<div class="col-sm-7">
								<!--<input name="req_date" value = "{{Input::old('req_date')}}" type="text" id="req_date" />-->
								{!! Form::text("delivery_date", null, ['id' => 'delivery_date', 'name' => 'delivery_date'])!!}
								
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.reqDate') }}</label>
							<div class="col-sm-7">
								<!--<input name="req_date" value = "{{Input::old('req_date')}}" type="text" id="req_date" />-->
								{!! Form::text("req_date", null, ['id' => 'req_date', 'name' => 'req_date'])!!}
								
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.reqType') }}</label>
							<div class="col-sm-7">
								<label style="margin-right:25px;" class="label_margin"> {!! Form::radio('requested_type', 'Submit request', true, ['id' => 'submit_request']) !!}
								<label for="submit_request"></label> {{ trans('add_job.submitReq') }}					
								</label>
								<label style="margin-right:25px;" class="label_margin"> {!! Form::radio('requested_type', 'Request quote', isset($project)?$project->requested_type:"" == "Request quote", ['id' => 'request_quote']) !!} <label for="request_quote"></label> {{ trans('add_job.reqQuote') }}</label>
								<label class="label_margin"> {!! Form::radio('requested_type', 'Prospect', isset($project)?$project->requested_type:"" == "Prospect", ['id' => 'prospect']) !!} <label for="prospect"></label> {{ trans('add_job.prospect') }}</label>
								
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.csc') }}</label>
							<div class="col-sm-7">
							{!! Form::select('advisor[0]', $personals, isset($project)?array_key_exists(0, $project->advisors->toArray())?$project->advisors[0]->id_personal:null:"", ['id' => 'advisor1']) !!}
							  
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.csc2') }}</label>
							<div class="col-sm-7">
							{!! Form::select('advisor[1]', $personals, isset($project)?array_key_exists(1, $project->advisors->toArray())?$project->advisors[1]->id_personal:null:"", ['id' => 'advisor2']) !!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.jobType') }}</label>
							<div class="col-sm-7">
								@foreach ($jobTypes as $key => $jobType)

									<label class="label_margin">  {!! Form::checkbox('job_type[]', $jobType->id_job_type, array_search($jobType->id_job_type, isset($project)?$project->job_types->lists('id_job_type')->toArray():array()) !== false ? true : false, ['id' => $jobType->code_id, 'class' => 'job_type']) !!} <label for="{{ $jobType->code_id }}"></label>
										@if(LaravelLocalization::getCurrentLocale() == 'en')
                                    		{{ $jobType->nom_en }}
                                		@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                                   			{{ $jobType->nom_fr }}
                               			@endif 
                               		</label>
								@endforeach					
								<label class="label_margin"> 
								{!! Form::checkbox('other_job_type', '1', null, ['id' => 'other_job', 'class' => 'job_type', 'onclick' => 'show_input();']) !!} 
								<label for="other_job"></label>
								 {{ trans('add_job.other') }}
								</label>
								
								
								{!! Form::text('text_other_job_type', $otherJob, ['id' => 'text_other_job_type', 'style' => 'display:none;']) !!}
								@if(Input::old('other_job_type')=="1" || ($otherJob != null && null===Input::old('other_job_type')))
						   		<script type="text/javascript">
						   			var other = $('#other_job').prop('checked');
									if(other == true ){
										$("#text_other_job_type").show();
									}
									else{
										$("#text_other_job_type").hide();
									}
								</script>
							@endif
							</div>
						  </div>
						  
					  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projDesc') }}</label>
							<div class="col-sm-7 space_bottom">
								<!--<textarea class="form-control" rows="3" name="project_desc" id="project_desc" required>{{Input::old('project_desc')}}</textarea>-->
								{!! Form::textarea("project_desc", null, ['id' => 'project_desc', 'rows' => '3', 'class' => 'form-control', 'required' => 'true'])!!}
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.currSit') }}</label>
							<div class="col-sm-7 space_bottom">
								<!--<textarea class="form-control" rows="3" name="project_current_situation" id="project_current_situation">{{Input::old('project_current_situation')}}</textarea>-->
								{!! Form::textarea("project_current_situation", null, ['id' => 'project_current_situation', 'rows' => '3', 'class' => 'form-control'])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.targMark') }}</label>
							<div class="col-sm-7 space_bottom">
								<!--<textarea class="form-control" rows="3" name="project_target" id="project_target">{{Input::old('project_target')}}</textarea>-->
								{!! Form::textarea("project_target", null, ['id' => 'project_target', 'rows' => '3', 'class' => 'form-control'])!!}
							</div>
						  </div>
						  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projObj') }}</label>
							<div class="col-sm-7 space_bottom">
								<!--<textarea class="form-control" rows="3" name="project_objectives" id="project_objectives">{{Input::old('project_objectives')}}</textarea>-->
								{!! Form::textarea("project_objectives", null, ['id' => 'project_objectives', 'rows' => '3', 'class' => 'form-control'])!!}
							</div>
						  </div>
						  
						  
  
					  </div>
					</div>
				  </div>
				  
				  <div id="files_block" class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title plus_title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						  {{ trans('add_job.files') }}
						</a>
							<button onclick="add_file();"  class="remove_input sqrButton float_r" type="button">
								<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
							</button>
					  </h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse in">
					  <div class="panel-body">
						
						  <div id="newfile_1" class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.attachments') }}</label>
							<div class="col-sm-7">

								@if($isEdit)
									@foreach($project->file_uploads as $file)
										<a href="http://xeroxweb.ca/laravel/trax_lara/public/index.php/download/{{$file->id_file_upload}}.{{$file->extension}}">{{$file->original_filename}}</a>
										@if($file->file_desc != "")
										<a  data-toggle="tooltip" title="{{ $file->file_desc }}" style="text-decoration:none">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                        </a>
                                        @endif<br>
									@endforeach
									
								@else
									{!! Form::file('project_file[0]') !!}
									{!! Form::text("file_desc[0]", null, ['class' => 'form-control space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
								@endif
								

							</div>
						  </div>
						  
						  <div id="add_new_file" /></div>
					  
					  </div>
				  </div>
				
				
				</div>
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
						  {{ trans('add_job.billInfo') }}
						</a>
					  </h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse in">
					  <div class="panel-body">
					  
						  <div class="form-group" id="cost_split_group">

							<label class="col-sm-3 control-label">{{ trans('add_job.costCentersQ') }}</label>
							<div class="col-sm-7">
								<label style="margin-right:5px;" class="label_margin"> <input id="yes_cost_split" type="radio" value="yes" name="cost_split"  /><label for="yes_cost_split"></label> {{ trans('add_job.yes') }}</label>
								<label style="margin-left:10px;" class="label_margin"> <input id="no_cost_split"  type="radio" value="no"  name="cost_split" checked="checked" /><label for="no_cost_split"></label> {{ trans('add_job.no') }}</label>

							</div>
						  </div>

						  <div class="form-group cost_split_multiple" id="cost_split_multiple_1" style="display:none;">
							<label class="col-sm-3 control-label">{{ trans('add_job.costCenter') }}</label>
							
							<div class="col-sm-3">
							  <!--<input type="text" name="cost_center_multipl[]" value = "{{Input::old('cost_center_multipl.0')}}"  class="form-control" placeholder="{{ trans('add_job.costCenter') }}">-->

							  {!! Form::text("cost_center_multipl[0]", isset($project)?array_key_exists(0, $project->cost_centers->toArray())?$project->cost_centers[0]->cost_center_name:null:null, ['class' => 'form-control', 'placeholder' => trans('add_job.costCenter')])!!}
							</div>
							<div class="col-sm-2">
							  <!--<input type="text" name="percent_cc_multipl[]" value = "{{Input::old('percent_cc_multipl.0')}}" class="form-control" placeholder="{{ trans('add_job.perc') }}">-->

							  {!! Form::text("percent_center_multipl[0]", isset($project)?array_key_exists(0, $project->cost_centers->toArray())?$project->cost_centers[0]->percentage:null:null, ['class' => 'form-control', 'placeholder' => trans('add_job.perc')])!!}
							</div>
							<div class="col-sm-2" style="text-align:center;padding-top: 5px;">
								<button onclick="addCostCentre();" class="remove_input sqrButton" type="button">
									<span aria-hidden="true" id="add_cost_centre" class="glyphicon glyphicon-plus"></span>
								</button>
								<!--<input id="add_cost_centre" type="button" value="+" onclick="addCostCentre();" class="remove_input" >-->
							</div>
						  </div>

						  
						   @if(Input::old('cost_split')=="yes" || $count > 1 && null===Input::old('cost_split'))

						   	@if(Input::old('cost_split')=="yes")
						  		@foreach(Input::old('cost_center_multipl') as $key => $cost_center)
						  		@unless($key == 0)
						  		<div class="form-group cost_split_multiple" id='cost_split_multiple_{{$key+1}}'>
									<label class="col-sm-3 control-label">{{ trans('add_job.costCenter') }}</label>
									<div class="col-sm-3">
										<!--<input type="text" name="cost_center_multipl[]" value = "{{Input::old('cost_center_multipl.'.$key)}}" class="form-control" placeholder="{{ trans('add_job.costCenter') }}">-->

										{!! Form::text("cost_center_multipl[$key]", null, ['class' => 'form-control', 'placeholder' => trans('add_job.costCenter')])!!}
									</div>
									<div class="col-sm-2">
										<!--<input type="text" name="percent_cc_multipl[]" value = "{{Input::old('percent_cc_multipl.'.$key)}}" class="form-control" placeholder="{{ trans('add_job.perc') }}">-->
										{!! Form::text("percent_center_multipl[$key]", null, ['class' => 'form-control', 'placeholder' => trans('add_job.perc')])!!}
									</div>
									<div class="col-sm-2" style="text-align:center;padding-top: 5px;">
										<button onclick='remove_cost({{$key+1}})' class="remove_input sqrButton" type="button">
											<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
										</button>
										<!--<input class="remove_input" type="button" value="X" onclick='remove_cost({{$key+1}})'>-->
									</div>
								</div>
						  		@endunless
						  		@endforeach

						  	@elseif($count > 1 && null===Input::old('cost_split'))

						  		@if(isset($project))
						  		@for($i = 0; $i < $count ; $i++)
						  		@unless($i == 0)
						  		<div class="form-group cost_split_multiple" id='cost_split_multiple_{{$i+1}}'>
									<label class="col-sm-3 control-label">{{ trans('add_job.costCenter') }}</label>
									<div class="col-sm-3">
										<!--<input type="text" name="cost_center_multipl[]" value = "{{Input::old('cost_center_multipl.'.$key)}}" class="form-control" placeholder="{{ trans('add_job.costCenter') }}">-->
										{!! Form::text("cost_center_multipl[$i]", $project->cost_centers[$i]->cost_center_name, ['class' => 'form-control', 'placeholder' => trans('add_job.costCenter')])!!}
									</div>
									<div class="col-sm-2">
										<!--<input type="text" name="percent_cc_multipl[]" value = "{{Input::old('percent_cc_multipl.'.$key)}}" class="form-control" placeholder="{{ trans('add_job.perc') }}">-->
										{!! Form::text("percent_center_multipl[$i]", isset($project)?$project->cost_centers[$i]->percentage: null, ['class' => 'form-control', 'placeholder' => trans('add_job.perc')])!!}
									</div>
									<div class="col-sm-2" style="text-align:center;padding-top: 5px;">
										<button onclick='remove_cost({{$key+1}})' class="remove_input sqrButton" type="button">
											<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
										</button>
										<!--<input class="remove_input" type="button" value="X" onclick='remove_cost({{$i+1}})'>-->
									</div>
								</div>
						  	@endunless
						  @endfor
						  @endif
						  	@endif
						  	
						  @endif

						  
						  
						
						  <div class="form-group" id="cost_split_single">
							<label class="col-sm-3 control-label">{{ trans('add_job.costCenter') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" id="cost_center" value = "{{Input::old('cost_center')}}" name="cost_center" class="form-control" placeholder="{{ trans('add_job.costCenter') }}">-->
							  {!! Form::text("cost_center", isset($project)?array_key_exists(0, $project->cost_centers->toArray())?$project->cost_centers[0]->cost_center_name:null:null, ['class' => 'form-control', 'placeholder' => trans('add_job.costCenter')])!!}
							</div>
						  </div>
						  
					  

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projCode') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" id="project_code" value = "{{Input::old('project_code')}}" name="project_code" class="form-control" placeholder="{{ trans('add_job.projCode') }}">-->
							  {!! Form::text("project_code", null, ['class' => 'form-control', 'placeholder' => trans('add_job.projCode')])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.networkAct') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" id="network_act" name="network_act" value = "{{Input::old('network_act')}}" class="form-control" placeholder="{{ trans('add_job.networkAct') }}">-->
							  {!! Form::text("network_act", null, ['class' => 'form-control', 'placeholder' => trans('add_job.networkAct')])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.gl') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" name="gl" id="gl" value = "{{Input::old('gl')}}" class="form-control" placeholder="{{ trans('add_job.gl') }}">-->
							  {!! Form::text("gl", null, ['class' => 'form-control', 'placeholder' => trans('add_job.gl')])!!}
							</div>
						  </div>
						  @if(Input::old('cost_split')=="yes" || $count > 1 && null===Input::old('cost_split'))
						   <script type="text/javascript">
						   	$('#yes_cost_split').click();
									$(".cost_split_multiple").show();
									$("#cost_split_single").hide();
								 	</script>
							@endif
					  
					  </div>
					</div>
				  </div>				  
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
						  Contact
						</a>
					  </h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse in">
					  <div class="panel-body">
					  
						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.projOwn') }}</label>
							<div class="col-sm-7">
							  <!--<input type="text" name="name_client" value = "{{Input::old('name_client')}}" id="name_client" class="form-control" placeholder="{{ trans('add_job.projOwn') }}" required>-->
							  {!! Form::text("name_client", null, ['id' => 'name_client', 'class' => 'form-control', 'placeholder' => trans('add_job.projOwn'), 'required' => 'true'])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.clientPhone') }}</label>
							<div class="col-sm-7">
								<!--<input type="text" name="client_phone" value = "{{Input::old('client_phone')}}" id="telephone" class="form-control" placeholder="{{ trans('add_job.clientPhone') }}">-->
								{!! Form::text("client_phone", null, ['id' => 'client_phone', 'class' => 'form-control', 'placeholder' => trans('add_job.clientPhone')])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.adress') }}</label>
							<div class="col-sm-7">
								<!--<input type="text" name="client_address" value = "{{Input::old('client_address')}}" id="adress"  class="form-control" placeholder="{{ trans('add_job.adress') }}">-->
								{!! Form::text("client_address", null, ['id' => 'client_address', 'class' => 'form-control', 'placeholder' => trans('add_job.adress')])!!}
							</div>
						  </div>

						  <div class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_job.other') }}</label>
							<div class="col-sm-7">
								<!--<input type="text" name="other" value = "{{Input::old('other')}}" id="autre"  class="form-control" placeholder="{{ trans('add_job.other') }}">-->
								{!! Form::text("other", null, ['id' => 'autre', 'class' => 'form-control', 'placeholder' => trans('add_job.other')])!!}
							</div>
						  </div>						  
						  
					
					
					
					
					  </div>
					</div>
				  </div>
		</div>
				
			