<style>
a.ui-state-default {
    box-sizing: content-box;
}
.ui-widget-header {background: #4764a5;border: 1px solid #4764a5;}
</style>
    <script type="text/javascript">
$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	});
		$( document ).ready(function() {

			$('#end_time').timepicker({
				hourText: 'Hours',
				minuteText: 'Minutes',
				amPmText: ['', ''],
				timeSeparator: ':',

				hours: { starts: 7, ends: 19 },
				minutes: { interval: 30 },
				rows: 3,
				showPeriodLabels: true
			});
			
			$( "#end_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			$( "#delivery_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			$( "#req_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});

			function save_template(){
				var str = $( "#add_project" ).serialize();
	
				$( "#template_save" ).dialog({
					resizable: false,
					height:250,
					modal: true,
					buttons: [
						{
							text: "Save",
							click: function() {
								$.ajax({
									type: 'post',
									url:'{{URL::route('storeTemplate')}}',
									data: str, 
									dataType: 'html',
									success: function(){
										console.log();
										alert('success');
		
									}
								});	
								$( this ).dialog( "close" );
							}
						},
						{
							text: "Cancel",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					]
				});		
			}

			//$.noConflict();
		});

		function add_file(){
			//var count = $('#files_block .form-group').size();
			var count = parseInt( $('#files_block .form-group').last().attr('id').split('_')[1] );
			var num = count + 1;
			var $html = $( $('#new_file').html() );
				$html.attr('id', 'newfile_'+num+'');
				$html.find('input.file-upload').attr('name', 'project_file['+count+']');
				$html.find('input.file-desc').attr('name', 'file_desc['+count+']');

				$html.find('.remove_input').attr('onclick','remove_file('+num+')');			
				$('#add_new_file').append($html);
		}


		function remove_file(num){
			var html = $('#newfile_'+num+'');
			html.remove();
		}
		
		

		function show_input(){
			var other = $('#other_job').prop('checked');
			if(other == true ){
				$("#text_other_job_type").show();
			}
			else{
				$("#text_other_job_type").hide();
			}
		}
		
		function reset_form(id_form){
			$("#"+id_form+"")[0].reset();
		}

		function addCostCentre(){
			var count = parseInt( $('.cost_split_multiple').last().attr('id').split('cost_split_multiple_')[1] );
			var num = count + 1;
			var $html = $( $('#new_cost').html() );
				$html.attr('id', 'cost_split_multiple_'+num+'');
				$html.find('input.cost_name').attr('name', 'cost_center_multipl['+count+']');
				$html.find('input.percent').attr('name', 'percent_center_multipl['+count+']');

				$html.find('.remove_input').attr('onclick','remove_cost('+num+')');			
				//$('#cost_split_multiple_1').append($html);
				$($html).insertBefore( "#cost_split_single" );
				
		}
		
		function remove_cost(num){
			var html = $('#cost_split_multiple_'+num+'');
			html.remove();
		}


		
		
		function template_fill(){
			var sel = document.getElementById("select_template");
			var val = sel.options[sel.selectedIndex].value;
			var val = parseInt(val);

			window.location.href = "http://xeroxweb.ca/laravel/trax_lara/public/index.php/job/add/"+val;


			
		
		}
		

		
	</script>

	<script type="text/javascript">
	
	$( document ).ready(function() {

		$( "#storeButton" ).click(function( event ) {
			var name_input;
			var  err=0;
			var mystring = 'The following fields are required :<br/>';
			//File upload=> var input_video = $("#video").val();
			var advisor1 = $("#advisor1").val(); //Select box
			var job_type = $('.job_type').is( ":checked" ); //multiple check box

			if(advisor1 == 0){
				err++;
				name_input = 'Communication Services Consultant';
				mystring += '<br/>- '+ name_input;
			}

			if(job_type == false){
				err++;
				name_input = 'Job Types';
				mystring += '<br/>- '+ name_input;
			}
			
			if (err> 0){
				//console.log(err);
				event.preventDefault();
				//console.log(mystring);
				$('#alert_global_body').html(mystring);
				$("#alert_global").modal();
			}


		});
		
		$('#yes_cost_split').click(function(){
				
					//Show add cost center button and remove Single line cost centre
					$(".cost_split_multiple").show();
					$("#cost_split_single").hide();
					
				});
				$('#no_cost_split').click(function(){
					$(".cost_split_multiple").hide();
					$("#cost_split_single").show();
				

				});
		
		$('input[name=import_template]').click(function(){
			var import_template = $('#import_template').prop('checked');
			if(import_template == true ){
				$("#select_template").show();
			}
			else{
				$("#select_template").hide();
			}
			
		});

	

	});
	</script>
	
	<script type="text/template" id="new_file">
	
		  <div class="form-group"><br>
		  	<div class="col-sm-3">
		  		<button class="remove_input sqrButton" type="button">
						<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
			</div>

			<div class="col-sm-7">
				{!! Form::file('project_file_name', ['class' => 'file-upload']) !!}
				{!! Form::text("project_desc_name", null, ['class' => 'form-control file-desc space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
	
			</div><br>
		  </div>
  	
  	</script>	
	
	<script type="text/template" id="new_cost">
		<div class="form-group cost_split_multiple">
			<label class="col-sm-3 control-label">{{ trans('add_job.costCenter') }}</label>
			<div class="col-sm-3">
			  {!! Form::text("cost_random_name", null, ['class' => 'form-control cost_name', 'placeholder' => trans('add_job.costCenter')])!!}
			</div>
			<div class="col-sm-2">
			 {!! Form::text("percent_random_name", null, ['class' => 'form-control percent', 'placeholder' => trans('add_job.perc')])!!}
			</div>
			<div class="col-sm-2" style="text-align:center;padding-top: 5px;">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
				<!--<input class="remove_input" type="button" value="X">-->
			</div>
		</div>
	</script>