@extends('layouts.app')
<?php
	use Illuminate\Support\Facades\Input;
?>

@section('content')

@include('pages.job.javascript')
<div id="xrx_page">

	<div class="row" style="margin-right: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<h1>{{ trans('add_job.newProject') }}</h1>
			@if(isset($project))
				{!! Form::model($project, ['id' => 'add_project', 'name' => 'add_project', 'action' => ['JobController@store'], 'files'=>true])!!}
			@else
				{!! Form::open(['action' => ['JobController@store'], 'id' => 'add_project', 'method' => 'post', 'name' => 'add_project', 'files'=>true])!!}
			@endif
			<input type="hidden" name="edit_job" value=0>
				@include('pages.job.form', ['isEdit' => false])
				<div class="">
				<div class="col-sm-offset-3 col-sm-7 btn_group">
					<button type="submit" class="btn btn-default xrx_arrow_link_right" value="store" name="store">{{ trans('add_job.submit') }}<span ></span></button>
					<button type="submit" class="btn btn-default" onclick="add_project.action='{{URL::route('storeTemplate')}}'; return true;" value="template" name="templateSub" formnovalidate>{{ trans('add_job.saveTemplate') }}</button>
					<button type="button" class="btn btn-default" onclick="reset_form('add_project');" >{{ trans('add_job.reset') }}</button>
				</div>
			</div>
			{!! Form::close()!!}
			</div>
		
	<div class="clear"> </div>
	
	
	</div><!-- end #row -->
	
	<!-- Notifications -->
	
	<div id="alert_global" role="dialog" tabindex="-1" class="modal fade bs-example-modal-sm" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
				{{ trans('add_job.warning') }}
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ trans('add_job.close') }}</span></button>
				</div>
					<div class="modal-body">
						<p id="alert_global_body"> </p>
					</div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('add_job.close') }}</button>
				  </div>
			</div>
		</div>
	</div>
	
	
	<div id="template_save" title="{{ trans('add_job.saveProjTemp') }}?" style="display:none;">

		<div>
			<p style="font-size:12px;margin:0;">{{ trans('add_job.warnBlank') }}</p>
			
		
		</div>
	</div>		
	
	
	

</div><!-- end #xrx_page -->


	
	
	
@endsection