@extends('layouts.app')

@section('content')
<?php
    $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());
 ?>
<script type="text/javascript" src="{{URL::asset('js/popupsV2.js') }}"></script>

<script type="text/javascript">	
	
	$(function() {
		$( "#dialog" ).dialog({
			autoOpen: false,
			width: 500
		});
		$('[data-toggle="tooltip"]').tooltip()
	});

	function view_estimate (id)
		{
		 popup_iframe ("localhost:8082/trax_dev_env/public/estimate/"+id+"/view", 690, 800,  'location.reload()');
		}
		
	function change_status(id_estimate){
		var edit_check = $('#checkbox_status_'+id_estimate).prop('checked');
		if(edit_check == true){
			$("#status_"+id_estimate).hide();
			$("#reject_icon_"+id_estimate).hide();
			$("#select_status_"+id_estimate).show();
		
		}
		else{
			$("#status_"+id_estimate).show();
			$("#reject_icon_"+id_estimate).show();
			$("#select_status_"+id_estimate).hide();
		
		}

	}

	 function new_status(id_estimate,status_estimate){
        //console.log(idproject);
        //console.log(status_project);
        var str = "status="+status_estimate+"&id_estimate="+id_estimate;
        var edit_check = $('#checkbox_status_'+id_estimate).prop('checked');
        var $_token = $('#token').val();
        if (status_estimate == 0 ){
			alert('Please choose a valid status!');			
		}
		else if(status_estimate == 3){
			$("#dialog").dialog('option', 'buttons',
			[
 	   			{
 	   			  text: "{{trans('list_estimate.cancel')}}",
 	   			  click: function() {
 	   			    $( this ).dialog( "close" );
 	   			  }
 	   			},
 	   			{
 	   				text: "{{trans('list_estimate.confirm')}}",
 	   			  	click: function() {
 	   			    	var str2 = "id_estimate="+id_estimate+"&id_rejection="+$('#estimate_rejection').val();
       					$.ajax({
       			     	type: 'post',
       			     	cache: false,
       			     	headers: { 'X-XSRF-TOKEN' : $_token }, 
       			     	url:"{{URL::route('rejectionMessage')}}",
       			     	data: str2, 
       			     	dataType: 'html',
       			     	success: function(msg){
       			     	    //console.log(edit_check);
       			     	    $("#reject_icon_"+id_estimate).empty();
       			     	    $("#reject_icon_"+id_estimate).html(msg);
        	        		$("#reject_icon_"+id_estimate).show();
        	        		$.ajax({
        					    type: 'post',
        					    cache: false,
        					    headers: { 'X-XSRF-TOKEN' : $_token }, 
        					    url:"{{URL::route('updateEstimateStatus')}}",
        					    data: str, 
        					    dataType: 'html',
        					    success: function(msg){
        					        //console.log(edit_check);
        					        $("#status_"+id_estimate).empty();
        					        $("#status_"+id_estimate).html(msg);
        					        $("#status_"+id_estimate).show();
        					        $("#select_status_"+id_estimate).hide();
        					        $('input[name=checkbox_status_'+id_estimate+']').attr('checked', false);
        					    },
        					    error: function(xhr, status, error) {
        					        var err = eval("(" + xhr.responseText + ")");
        					        alert(err.Message);
        					    }
        					});
       			     	    $( '#dialog' ).dialog( "close" );
       			     	    $('[data-toggle="tooltip"]').tooltip()
			
       			     	},
       			     	error: function(xhr, status, error) {
       			     	    var err = eval("(" + xhr.responseText + ")");
       			     	    alert(err.Message);
       			     	}
       			 	});
 	   			  }
 	   			}
 	]
	)
	$('#dialog').dialog('open');
		}
		else{
        	$.ajax({
        	    type: 'post',
        	    cache: false,
        	    headers: { 'X-XSRF-TOKEN' : $_token }, 
        	    url:"{{URL::route('updateEstimateStatus')}}",
        	    data: str, 
        	    dataType: 'html',
        	    success: function(msg){
        	        //console.log(edit_check);
        	        $("#status_"+id_estimate).empty();
        	        $("#status_"+id_estimate).html(msg);
        	        $("#status_"+id_estimate).show();
        	        $("#select_status_"+id_estimate).hide();
        	        $('input[name=checkbox_status_'+id_estimate+']').attr('checked', false);
        	    },
        	    error: function(xhr, status, error) {
        	        var err = eval("(" + xhr.responseText + ")");
        	        alert(err.Message);
        	    }
        	});
    	}

    }
	
	
	
			
</script>
<input id="token" type="hidden" value="{{$encrypted_token}}">
<div id="xrx_page">
	<div class="row" style="margin-right: 0px; margin-left: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<p>
				<a href="{{ URL::route('home') }}" class="btn btn-primary">{{ trans('list_estimate.returnDash') }}  </a>
				@if(Auth::user()->categorie==1)
				<a href="{{ URL::route('addEstimate', ['project'=>$project->id_project]) }}" class="btn btn-primary">{{ trans('list_estimate.addEstimate') }} </a>
				@endif
			</p>
			
			<h2>{{ trans('list_estimate.estimateList') }} {{$project->project_name}} : {{$project->req}} </h2>
			
			<div id="dashboard">
			
				<table class="table table-striped table-bordered table-condensed table-hover">
				    <thead>
				        <tr>
				          <th>{{ trans('list_estimate.revision') }}</th>
				          <th>{{ trans('list_estimate.description') }}</th>
				          <th>{{ trans('list_estimate.price') }}</th>
				          <th>{{ trans('list_estimate.status') }} </th>
				          <th>{{ trans('list_estimate.view') }} </th>
				        </tr>
				    </thead>
				      <tbody>
						@foreach($estimates as $key => $estimate)
						<tr>
							<td>{{$key+1}}</td>
							<td> <a href="{{ URL::route('editEstimate', ['estimate'=>$estimate->id_estimate]) }}">
							@if(strlen($estimate->description) > 25)
								{{substr($estimate->description, 0, 25)}}...
							@else
								{{$estimate->description}}
							@endif
							</a></td>
							<td>{{ number_format($estimate->total_price, 2) }}$</td>
							<td>
							<input type="checkbox" name="checkbox_status_{{ $estimate->id_estimate }}" id="checkbox_status_{{ $estimate->id_estimate }}" onclick="change_status({{$estimate->id_estimate}})"> 
									<span style="display:inline;" id="status_{{$estimate->id_estimate}}"> 
											<span style="$estimate->status->css">
											@if(LaravelLocalization::getCurrentLocale() == 'en')
												{{ $estimate->status->name_en }}
											@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
												{{ $estimate->status->name_fr }}
											@endif
											</span>
											
                                    </span>
                                    <span id="reject_icon_{{$estimate->id_estimate}}">
                                    @if($estimate->validate == 3)
                                    			
                                            	@if(LaravelLocalization::getCurrentLocale() == 'en')
													<a  data-toggle="tooltip" title="{{ $estimate->rejection->rejection_message_en }}">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                            		</a>
												@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
													<a  data-toggle="tooltip" title="{{ $estimate->rejection->rejection_message_fr }}">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                            		</a>
												@endif
                                    @endif
                                    </span>
								@if($estimate->validate != 4) 
								{!! Form::select("estimate_status", $estimate_status, $estimate->validate, ['onchange' => "new_status($estimate->id_estimate,this.value)", 'id' => "select_status_".$estimate->id_estimate, 'class' => "select_status" ]) !!}														

								

								@endif

							
							</td>
							<td>
								<a class="btn btn-primary" onclick="view_estimate({{ $estimate->id_estimate }});">{{ trans('list_estimate.view') }}</a>
								
									@if($estimate->validate != 4) 
										<a href="{{ URL::route('editEstimate', ['estimate'=>$estimate->id_estimate]) }}" class="btn btn-primary">{{trans('list_estimate.edit')}} </a>										
								
								@endif
								

							
							</td>
						</tr>
						
						@endforeach
						
						


				      </tbody>
				</table>			
			
			</div><!-- end dashboard -->

		</div>
		
		<div class="clear"> &nbsp;</div>
	</div><!-- end #row -->

</div><!-- end #xrx_page -->
<div id="dialog" title="{{trans('list_estimate.rejectReason') }}">
	<p>{{trans('list_estimate.rejectQuestion')}}</p>
	{!! Form::select("estimate_rejection", $estimate_rejection, null, ['id' => 'estimate_rejection', 'class' => "form-control" ]) !!}		
</div>

@endsection