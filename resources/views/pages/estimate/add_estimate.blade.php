@extends('layouts.app')

@section('content')

@include('pages.estimate.javascript')
<div id="xrx_page">


	<div class="row" style="margin-right: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<p>
				<a href="{{ URL::route('estimateList', $project->id_project) }}" class="btn btn-primary">{{ trans('add_estimate.returnList') }}  </a>
			</p>
			
			<h1>{{ trans('add_estimate.newEstimate') }}  {{ $project->project_name}} : {{ $project->req }} </h1>
			{!! Form::open(['action' => ['EstimateController@store'], 'id' => 'add_estimate', 'method' => 'post', 'name' => 'add_estimate', 'enctype' => 'multipart/form-data', 'files'=>true])!!}

				@include('pages.estimate.form', ['isEdit' => false])

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7" style="margin-bottom: 10px;">
						<button type="submit" class="btn btn-default xrx_arrow_link_right">{{ trans('add_estimate.submit') }}<span class=""></span></button>
					</div>
				</div>	
			{!! Form::close()!!}
			

		</div>
		<div class="clear"> </div>
	
	
	</div><!-- end #row -->

</div><!-- end #xrx_page -->
@endsection