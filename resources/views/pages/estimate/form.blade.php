@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?php
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Storage;
?>
<?php
  	if(isset($estimate)){
  		$count = count($estimate->titles);
  	}
  	else{
  		$count = 0;
  	}
?>


				@if($isEdit)
					<input type="hidden" name="id_project" value="{{ $estimate->id_project }}" />
					<input type="hidden" name="id_estimate" value="{{ $estimate->id_estimate }}" />
				@else
					<input type="hidden" name="id_project" value="{{ $project->id_project }}" />
				@endif
				
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.description') }}</label>
					<div class="col-sm-7 space_bottom">
						<!--<textarea class="form-control" rows="3" name="project_desc" id="project_desc" required>{{Input::old('project_desc')}}</textarea>-->
						{!! Form::textarea("description", null, ['rows' => '3', 'class' => 'form-control', 'required' => 'true'])!!}
					</div>
				  </div>

				<div class="form-group" >
					<label class="col-sm-3 control-label">{{ trans('add_estimate.estimateBefore') }}</label>
					<div class="col-sm-2">
						{!! Form::number("estimate_before", $prices['estimatePrice'], ['class' => 'form-control', 'id'=>'estimatePrice', 'disabled'=>true, 'step'=>'any', 'required' => 'true'])!!}
						{!! Form::hidden("estimate_before", $prices['estimatePrice'])!!}
					</div>
					<label class="col-sm-3 control-label">{{ trans('add_estimate.estimateHours') }}</label>
					<div class="col-sm-2">
						{!! Form::number("estimate_hours", $hours['estimateHours'], ['class' => 'form-control', 'id'=>'estimateHours', 'disabled'=>true, 'step'=>'any', 'required' => 'true'])!!}
						{!! Form::hidden("estimate_hours", $hours['estimateHours'])!!}
					</div>
				</div>
				<div class="form-group">
				<a class="btn btn-primary" data-toggle="collapse" style="width:125px; margin-left:15px" data-target="#collapsible" onClick="changeText(this)">{{trans('add_estimate.showDetails')}}</a>
				</div>
				<div id="collapsible" class="collapse">
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.test') }}</label>
					<div class="col-sm-1">
						{!! Form::number("test_price", $prices['testPrice'], ['class' => 'form-control', 'id'=>'tePrice', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.testHours') }}</label>
					<div class="col-sm-1">
						{!! Form::number("test_hours", $hours['testHours'], ['class' => 'form-control', 'id'=>'teHours', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.testPercent') }}</label>
					<div class="col-sm-1">
						{!! Form::number("test_percent", $percents['testPercent'], ['class' => 'form-control', 'id'=>'tePercent', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.qa') }}</label>
					<div class="col-sm-1">
						{!! Form::number("qa_price", $prices['qaPrice'], ['class' => 'form-control', 'id'=>'qaPrice', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.qaHours') }}</label>
					<div class="col-sm-1">
						{!! Form::number("qa_hours", $hours['qaHours'], ['class' => 'form-control', 'id'=>'qaHours', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.qaPercent') }}</label>
					<div class="col-sm-1">
						{!! Form::number("qa_percent", $percents['qaPercent'], ['class' => 'form-control', 'id'=>'qaPercent', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.pm') }}</label>
					<div class="col-sm-1">
						{!! Form::number("pm_price", $prices['pmPrice'], ['class' => 'form-control', 'id'=>'pmPrice', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.pmHours') }}</label>
					<div class="col-sm-1">
						{!! Form::number("pm_hours", $hours['pmHours'], ['class' => 'form-control', 'id'=>'pmHours', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
					<label class="col-sm-2 control-label">{{ trans('add_estimate.pmPercent') }}</label>
					<div class="col-sm-1">
						{!! Form::number("pm_percent", $percents['pmPercent'], ['class' => 'form-control', 'id'=>'pmPercent', 'step'=>'any', 'onchange'=>'changedValue(this.id)', 'required' => 'true'])!!}
					</div>
				</div>
				@if($print_job !== null)
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.print') }}</label>
					<div class="col-sm-1">
						{!! Form::number("print_price", round($print_job->price,2), ['class' => 'form-control', 'id'=>'printPrice', 'required' => 'true', 'disabled' => 'true'])!!}
					</div>
					<div>
					<a class="btn btn-primary" style="width:125px; margin-left:15px" href="{{URL::route('viewPrintPrices', $print_job->id_print_job)}}" onClick="target_popup(this)">{{trans('add_estimate.showDetails')}}</a>
					</div>
				</div>
				@endif
				</div>

<!--______________________________________________________________________________________________-->
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.totalPrice') }}</label>
					<div class="col-sm-7">
						{!! Form::number("total_price", $prices['totalPrice'], ['class' => 'form-control', 'onchange'=>'changeTotal()', 'id'=>'totalPrice', 'step'=>'any', 'required' => 'true'])!!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.reqDays') }}</label>
					<div class="col-sm-7">
						{!! Form::number("req_days", $isEdit?null:$days, ['class' => 'form-control', 'id'=>'reqDays', 'required' => 'true'])!!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.date') }}</label>
					<div class="col-sm-7">
						{!! Form::text("date", $isEdit?null:date('Y-m-d'), ['id' => 'date', 'class' => 'space_bottom form-control', 'style'=>'width:150px', 'required' => 'true'])!!}		
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.prepFor') }}</label>
					<div class="col-sm-7">
						{!! Form::text("prepared_for", $isEdit?null:$project->name_client, ['id' => 'prepared_for', 'class' => 'form-control', 'required' => 'true'])!!}							
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">{{ trans('add_estimate.prepBy') }}</label>
					<div class="col-sm-7">
						{!! Form::text("prepared_by", $isEdit?null:$project->advisors[0]->prenom_personal.' '.$project->advisors[0]->nom_personal, ['id' => 'prepared_by', 'class' => 'form-control', 'required' => 'true'])!!}							
					</div>
				</div>
				<div id="body_group">
					<div class="form-group">
						<h2>{{trans('add_estimate.estimateBody')}}
							<button onclick="add_title();"  class="remove_input sqrButton float_r" type="button">
								<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
							</button>
							<!--<input class="remove_input float_r" type="button" onclick="add_title();" value="+">-->
						</h2>
					</div>
					<div class="form-group title_group" id="title_group_1">
						<div class="form-group">
							<div class="col-sm-3" style="margin-bottom:15px;">
								<button onclick="remove_title(1);"  class="remove_input sqrButton" type="button">
									<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
								</button>
								<!--<input class="remove_input" type="button" onClick="remove_title(1)" value="X">-->
								&nbsp;|&nbsp;{{ trans('add_estimate.title') }}
							</div>
							<div class="col-sm-7">
								{!! Form::text("title[0]", isset($estimate)?array_key_exists(0, $estimate->titles->toArray())?$estimate->titles[0]->title:null:"", ['id' => 'title_1', 'class' => 'form-control', 'required' => 'true', 'maxlength'=>'50'])!!}							
							</div>
						</div>
						<div class="form-group">
							{!! Form::textarea("content[0]", isset($estimate)?array_key_exists(0, $estimate->titles->toArray())?$estimate->titles[0]->content:null:"", ['id' => 'content_1', 'class'=>'editor'])!!}
							<script>
             					// Replace the <textarea id="editor1"> with a CKEditor
            				 	// instance, using default configuration.
            				 	CKEDITOR.replace( 'content_1' );
		    				</script>

						</div>
					</div>
					@if(count(Input::old('title')) > 1 || $count > 1 && null===Input::old('title'))

								@if(count(Input::old('title')) > 1)
									@foreach(Input::old('title') as $key => $title)
										@unless($key == 0)
										<?php $id = $key+1; ?>
											<div class="form-group title_group" id="title_group_{{$id}}">
												<div class="form-group">
													<div class="col-sm-3" style="margin-bottom:15px;">
														<button onclick="remove_title({{$id}});"  class="remove_input sqrButton" type="button">
															<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
														</button>
														<!--<input class="remove_input" type="button" onClick="remove_title(1)" value="X">-->
														&nbsp;|&nbsp;{{ trans('add_estimate.title') }}
													</div>
													<div class="col-sm-7">
														{!! Form::text("title[$key]", null, ['id' => "title_$id", 'class' => 'form-control', 'required' => 'true', 'maxlength'=>'50'])!!}
													</div>
												</div>
												<div class="form-group">
													{!! Form::textarea("content[$key]", null, ['id' => "content_$id", 'class'=>'editor'])!!}
													<script>
             											// Replace the <textarea id="editor1"> with a CKEditor
            										 	// instance, using default configuration.
            										 	CKEDITOR.replace( 'content_{{$key}}' );
		    										</script>
						
												</div>
											</div>
										@endunless
									@endforeach
								@elseif($count > 1 && null===Input::old('title'))
									@if(isset($estimate))
										@for($i = 0; $i < $count ; $i++)
											@unless($i == 0)
											<?php $id = $i+1; ?>
												<div class="form-group title_group" id="title_group_{{$id}}">
												<div class="form-group" style="margin-bottom:15px;">
													<div class="col-sm-3">
														<button onclick="remove_title({{$id}});"  class="remove_input sqrButton" type="button">
															<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
														</button>
														<!--<input class="remove_input" type="button" onClick="remove_title(1)" value="X">-->
														&nbsp;|&nbsp;{{ trans('add_estimate.title') }}
													</div>
													<div class="col-sm-7">
														{!! Form::text("title[$i]", $estimate->titles[$i]->title, ['id' => "title_$id", 'class' => 'form-control', 'required' => 'true', 'maxlength'=>'50'])!!}
													</div>
												</div>
												<div class="form-group">
													{!! Form::textarea("content[$i]", $estimate->titles[$i]->content, ['id' => "content_$id", 'class'=>'editor'])!!}
													<script>
             											// Replace the <textarea id="editor1"> with a CKEditor
            										 	// instance, using default configuration.
            										 	CKEDITOR.replace( "content_{{$id}}" );
		    										</script>
						
												</div>
											</div>
											@endunless
										@endfor
									@endif
								@endif
							@endif
					<div id="add_new_title"></div>
				</div>
						


					
				 
					
