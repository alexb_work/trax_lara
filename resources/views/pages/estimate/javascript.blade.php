<style>
a.ui-state-default {
    box-sizing: unset;
}
.ui-widget-header {background: #4764a5;border: 1px solid #4764a5;}
</style>
<script>
$( document ).ready(function() {
			$( "#date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			//$.noConflict();
});

function target_popup(link) {
   	window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
   	link.target = 'formpopup';
}

function calculateTotal(){
	var estimate = parseFloat($('#estimatePrice').val());
	var test = parseFloat($('#tePrice').val());
	var qa = parseFloat($('#qaPrice').val());
	var pm = parseFloat($('#pmPrice').val());
	var print = parseFloat($('#printPrice').val());
	var total = estimate + test + qa + pm + print;
	$('#totalPrice').val(Math.round(total*100)/100);
}

function changedValue(changed_id){
	var prefix = changed_id.substring(0,2);
	var suffix = changed_id.substring(2);
	var rate = 0.0;
	var cost = 0.0;
	var hours = 0.0;
	var percent = 0.0;
	if(prefix == "te")
		rate = {{$rates["testRate"]}};
	else if(prefix == "qa")
		rate = {{$rates["qaRate"]}};
	else if(prefix == "pm")
		rate = {{$rates["pmRate"]}};
	if(suffix == "Price"){
		cost = parseFloat($("#"+prefix+"Price").val());
		calcHoursFromCost(prefix, rate, cost);
		hours = parseFloat($("#"+prefix+"Hours").val());
		calcPercent(prefix, hours);
	}
	else if(suffix == "Hours"){
		hours = parseFloat($("#"+prefix+"Hours").val());
		calcPercent(prefix, hours);
		calcCost(prefix, rate, hours);
	}
	else if(suffix == "Percent"){
		percent = parseFloat($("#"+prefix+"Percent").val())/100;
		calcHoursFromPercent(prefix, percent);
		hours = parseFloat($("#"+prefix+"Hours").val());
		calcCost(prefix, rate, hours);
	}
	calculateTotal();
}

function calcCost(prefix, rate, hours){
	$("#"+prefix+"Price").val(Math.round(rate*hours*100)/100);
}

function calcHoursFromPercent(prefix, percent){
	var totHours = parseFloat($('#estimateHours').val());
	$("#"+prefix+"Hours").val(Math.round(totHours * percent*100)/100);
}
function calcHoursFromCost(prefix, rate, cost){
	$("#"+prefix+"Hours").val(Math.round(cost / rate*100)/100);
}
function calcPercent(prefix, hours){
	var totHours = parseFloat($('#estimateHours').val());
	$("#"+prefix+"Percent").val(Math.round(hours / totHours * 100*100)/100);
}

function changeTotal(){
	var estimate = parseFloat($('#estimatePrice').val());
	var total = parseFloat($('#totalPrice').val());
	var totHours = parseFloat($('#estimateHours').val());
	var difference = total - estimate;
	var price = difference/3;
	$("#tePrice").val(Math.round(price*100)/100);
	$("#qaPrice").val(Math.round(price*100)/100);
	$("#pmPrice").val(Math.round(price*100)/100);
	$("#teHours").val(Math.round(price / {{$rates["testRate"]}}*100)/100);
	$("#qaHours").val(Math.round(price / {{$rates["qaRate"]}}*100)/100);
	$("#pmHours").val(Math.round(price / {{$rates["pmRate"]}}*100)/100);
	$("#tePercent").val(Math.round((price / {{$rates["testRate"]}}) / totHours * 100*100)/100);
	$("#qaPercent").val(Math.round((price / {{$rates["qaRate"]}}) / totHours * 100*100)/100);
	$("#pmPercent").val(Math.round((price / {{$rates["pmRate"]}}) / totHours * 100*100)/100);

}

function changeText(element){
	if(element.innerHTML == "{{trans('add_estimate.showDetails')}}"){
		element.innerHTML = "{{trans('add_estimate.hideDetails')}}";
	}
	else if(element.innerHTML == "{{trans('add_estimate.hideDetails')}}")
		element.innerHTML = "{{trans('add_estimate.showDetails')}}";
}

function remove_title(num){
	var html = $('#title_group_'+num+'');
	html.remove();
}

function add_title(){
	var count = 0;
	if(document.getElementById('title_group_1'))
		count = parseInt( $('#body_group .title_group').last().attr('id').split('_')[2] );
	var num = count + 1;
	var $html = $( $('#new_title').html() );
		$html.attr('id', 'title_group_'+num+'');
		$html.find('input.title_text').attr('name', 'title['+count+']');
		$html.find('textarea.editor').attr('name', 'content['+count+']');
		$html.find('textarea.editor').attr('id', 'content_'+num+'');
		
		$html.find('.remove_input').attr('onclick','remove_title('+num+')');
		$('#add_new_title').append($html);
		CKEDITOR.replace( 'content_'+num+'' );
}



</script>

<script type="text/template" id="new_title">
	
	<div class="form-group title_group" id="title_group_1">
		<div class="form-group">
			<div class="col-sm-3" style="margin-bottom:15px;">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
				<!--<input class="remove_input" type="button" value="X">-->
				&nbsp;|&nbsp;{{ trans('add_estimate.title') }}
			</div>
			<div class="col-sm-7">
				
				{!! Form::text("title_random", null, ['class' => 'form-control title_text', 'required' => 'true', 'maxlength'=>'50'])!!}							
			</div>
		</div>
		<div class="form-group">
			{!! Form::textarea("content_random", null, ['id' => 'content', 'class'=>'editor'])!!}
		</div>
	</div>

  	
  	</script>