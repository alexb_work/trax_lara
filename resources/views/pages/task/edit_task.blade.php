@extends('layouts.app')

@section('content')

@include('pages.task.javascript')
<div id="xrx_page">


	<div class="row" style="margin-right: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<p>
				<a href="{{ URL::route('taskList', $task->project->id_project) }}" class="btn btn-primary">{{ trans('list_task.returnDash') }}  </a>
			</p>
			
			<h1>{{ trans('add_task.editTask') }}</h1>
			{!! Form::model($task, ['id' => 'add_task', 'name' => 'add_project', 'action' => ['TaskController@update', $task->id], 'enctype' => 'multipart/form-data', 'files'=>true])!!}
			<input type="hidden" name="id_task" value="{{$task->id_tasks}}">
				@include('pages.task.form', ['isEdit' => true])
				@if(Auth::user()->categorie==1)
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-7" style="margin-bottom: 10px;">
						<button type="submit" class="btn btn-default xrx_arrow_link_right">{{ trans('add_task.submit') }}<span class=""></span></button>
					</div>
				</div>
				@endif
			{!! Form::close()!!}
			

		</div>
		<div class="clear"> </div>
	
	
	</div><!-- end #row -->

</div><!-- end #xrx_page -->

@endsection