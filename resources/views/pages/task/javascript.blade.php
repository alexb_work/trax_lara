<style>
a.ui-state-default {
    box-sizing: unset;
}
.ui-widget-header {background: #4764a5;border: 1px solid #4764a5;}
</style>
    <script type="text/javascript">
		$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	});
		$( document ).ready(function() {		
			$( "#req_date" ).datepicker({
  				dateFormat: "yy-mm-dd"
			});
			//$.noConflict();
		});
		
		function add_assign(){
			//var count = $('#files_block .form-group').size();
			var count = parseInt( $('#assigned_block .form-group').last().attr('id').split('_')[1] );
			var num = count + 1;
			var $html = $( $('#new_person').html() );
				$html.attr('id', 'assign_'+num+'');
				$html.find('select.assign_person').attr('name', 'assigned_person['+count+']');
				$html.find('select.assign_categorie').attr('name', 'assigned_categorie['+count+']');
				$html.find('input.assign_hours').attr('name', 'assigned_hours['+count+']');

				$html.find('.remove_input').attr('onclick','remove_person('+num+')');
				$('#add_new_person').append($html);
		}


		function remove_person(num){
			var html = $('#assign_'+num+'');
			html.remove();
		}

		function show_input(){
			var other = $('#other_job').prop('checked');
			if(other == true ){
				$("#text_other_job_type").show();
			}
			else{
				$("#text_other_job_type").hide();
			}
		}
		
		function reset_form(id_form){
			$("#"+id_form+"")[0].reset();
		}
		
		function add_file(){
			//var count = $('#files_block .form-group').size();
			var count = parseInt( $('#files_block .form-group').last().attr('id').split('_')[1] );
			var num = count + 1;
			var $html = $( $('#new_file').html() );
				$html.attr('id', 'newfile_'+num+'');
				$html.find('input.file-upload').attr('name', 'project_file['+count+']');
				$html.find('input.file-desc').attr('name', 'file_desc['+count+']');

				$html.find('.remove_input').attr('onclick','remove_file('+num+')');			
				$('#add_new_file').append($html);
		}	


		function remove_file(num){
			var html = $('#newfile_'+num+'');
			html.remove();
		}	

		

	</script>

	<script type="text/template" id="new_person">
	
		<div id="assign_1" class="form-group">
			<div class="col-sm-3">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
				<!--<input class="remove_input" type="button" value="X">-->
			</div>
			<div class="col-sm-7">
				{!! Form::select('assigned_person_random', $personals, null, ['class' => 'assign_person']) !!}
				{!! Form::select('assigned_categorie_random', $categories, null, ['class' => 'assign_categorie']) !!}
				{!! Form::number("assigned_hours_random", null, ['placeholder'=>trans('add_task.assignedHours'), 'step'=>'any', 'class'=>"assign_hours"])!!}
			</div>
		</div>
  	
  	</script>
	
	<script type="text/template" id="new_file">
	
		  <div class="form-group"><br>
			<div class="col-sm-3">
				<button class="remove_input sqrButton" type="button">
					<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
				</button>
				<!--<input class="remove_input" type="button" value="X">-->
			</div>
			<div class="col-sm-7">
				{!! Form::file('project_file_name', ['class' => 'file-upload']) !!}
				{!! Form::text("project_desc_name", null, ['class' => 'form-control file-desc space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
	
			</div><br>
		  </div>
  	
  	</script>	

