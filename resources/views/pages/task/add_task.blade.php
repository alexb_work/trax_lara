@extends('layouts.app')

@section('content')

@include('pages.task.javascript')
<div id="xrx_page">


	<div class="row" style="margin-right: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<p>
				<a href="{{ URL::route('taskList', $project->id_project) }}" class="btn btn-primary">{{ trans('list_task.returnDash') }}  </a>
			</p>
			
			<h1>{{ trans('add_task.newTask') }}  {{ $project->project_name}} : {{ $project->req }} </h1>
			{!! Form::open(['action' => ['TaskController@store'], 'id' => 'add_task', 'method' => 'post', 'name' => 'add_project', 'enctype' => 'multipart/form-data', 'files'=>true])!!}

				@include('pages.task.form', ['isEdit' => false])

				 <div class="form-group">
					<div class="col-sm-offset-3 col-sm-7" style="margin-bottom: 10px;">
						<button type="submit" class="btn btn-default xrx_arrow_link_right">{{ trans('add_task.submit') }}<span class=""></span></button>
					
						<button type="button" class="btn btn-default" onclick="reset_form('add_task');" >{{ trans('add_task.reset') }}</button>
					</div>
				  </div>	
			{!! Form::close()!!}
			

		</div>
		<div class="clear"> </div>
	
	
	</div><!-- end #row -->

</div><!-- end #xrx_page -->
@endsection