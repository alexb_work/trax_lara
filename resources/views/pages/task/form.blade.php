@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?php
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Storage;
?>
<?php
  	if(isset($task)){
  		$count = count($task->assignments);
  	}
  	else{
  		$count = 0;
  	}
?>
				<input type="hidden" name="editing_by" value="{{ Auth::id() }}" />
				@if($isEdit)
					<input type="hidden" name="id_project" value="{{ $task->id_project }}" />
				@else
					<input type="hidden" name="id_project" value="{{ $project->id_project }}" />
				@endif
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								{{ trans('add_task.addTask') }}
							</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse in">
						<div class="panel-body">
						  
							  <div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('add_task.brief') }}</label>
								<div class="col-sm-7">
									{!! Form::textarea("brief", null, ['id' => 'brief', 'rows' => '3', 'class' => 'form-control space_bottom', 'required' => 'true'])!!}
								</div>
							  </div>
							  
							  <div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('add_task.type') }}</label>
								<div class="col-sm-7">

								{!! Form::select('id_task_type', $taskType, null) !!}


								</div>
						  	  </div>

							  <div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('add_task.reqCompDate') }}</label>
								<div class="col-sm-7">
									{!! Form::text("req_date", null, ['id' => 'req_date', 'class' => 'space_bottom'])!!}							
								</div>
							  </div>
							  
							  
							  <div class="form-group">
								<label class="col-sm-3 control-label">{{ trans('add_task.comment') }}</label>
								<div class="col-sm-7">
									{!! Form::textarea("comment", null, ['id' => 'comment', 'rows' => '3', 'class' => 'form-control space_bottom'])!!}
								</div>
							  </div>
							  						  
							  
							  
						</div>
					</div>
				</div>
				<div id="files_block" class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title plus_title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
						  {{ trans('add_task.files') }}
						</a>
						<button onclick="add_file();"  class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
						</button>

					  </h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse in">
					  <div class="panel-body">
						
						  <div id="newfile_1" class="form-group">
							<label class="col-sm-3 control-label">{{ trans('add_task.attachments') }}</label>
							<div class="col-sm-7">

								@if($isEdit)
									@foreach($task->file_uploads as $file)
										<a href="http://xeroxweb.ca/laravel/trax_lara/public/index.php/download/{{$file->id_file_upload}}.{{$file->extension}}">{{$file->original_filename}}</a>
										@if($file->file_desc != "")
										<a  data-toggle="tooltip" title="{{ $file->file_desc }}" style="text-decoration:none">
                                            	    	<img width="25" src="{{ URL::asset('images/alert_icon.ico') }}" />
                                        </a>
                                        @endif<br>
									@endforeach
									
								@else
									{!! Form::file('project_file[0]') !!}
									{!! Form::text("file_desc[0]", null, ['class' => 'form-control space-bottom', 'placeholder'=>trans('add_task.fileDesc')])!!}
									
								@endif
								

							</div><br>
						  </div>
						  
						  <div id="add_new_file" /></div>
					  
					  </div>
				  </div>
				
				
				</div>
						
				
				<div id="assigned_block" class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title plus_title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse_assigned">
						  {{ trans('add_task.assignation') }}
						</a>
						<button onclick="add_assign();"  class="remove_input sqrButton float_r" type="button">
							<span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
						</button>
					  </h4>
					</div>
					
					<div id="collapse_assigned" class="panel-collapse collapse in">
						<div class="panel-body">
						
							<div id="assign_1" class="form-group">
								<label class="col-sm-3 control-label">{{ trans('add_task.personAssign') }}</label>

								<div class="col-sm-7">
									{!! Form::select('assigned_person[0]', $personals, isset($task)?array_key_exists(0, $task->assignments->toArray())?$task->assignments[0]->id_assigned_person:null:"", ['id' => 'assign_person_1']) !!}
									{!! Form::select('assigned_categorie[0]', $categories, isset($task)?array_key_exists(0, $task->assignments->toArray())?$task->assignments[0]->id_estimate_categorie:null:"", ['id' => 'assign_category_1']) !!}
									{!! Form::number("assigned_hours[0]", isset($task)?array_key_exists(0, $task->assignments->toArray())?$task->assignments[0]->assigned_hours:null:"", ['placeholder'=>trans('add_task.assignedHours'), 'required' => 'true', 'step'=>'any', 'id'=>'assign_hours_1'])!!}
								</div>

							</div>
							@if(count(Input::old('assigned_person')) > 1 || $count > 1 && null===Input::old('assigned_person'))

								@if(count(Input::old('assigned_person')) > 1)
									@foreach(Input::old('assigned_person') as $key => $assigned_person)
										@unless($key == 0)
											<div id="assign_{{$count}}" class="form-group">
												<div class="col-sm-3">
													<button onclick="remove_person({{$count}})"  class="remove_input sqrButton" type="button">
														<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
													</button>
													<!--<input class="remove_input" type="button" value="X">-->
												</div>
												<div class="col-sm-7">
													{!! Form::select("assigned_person[$key]", $personals, null, ['id' => "assign_person_$key"]) !!}
													{!! Form::select("assigned_categorie[$key]", $categories, null, ['id' => "assign_categorie_$key"]) !!}
													{!! Form::number("assigned_hours[$key]", null, ['placeholder'=>trans('add_task.assignedHours'), 'step'=>'any', 'required' => 'true', 'id'=>"assign_hours_$key"])!!}
												</div>
											</div>
										@endunless
									@endforeach
								@elseif($count > 1 && null===Input::old('assigned_person'))

									@if(isset($task))
										@for($i = 0; $i < $count ; $i++)
											@unless($i == 0)
												<div id="assign_{{$count}}" class="form-group">
													<div class="col-sm-3">
														<button onclick="remove_person({{$count}})"  class="remove_input sqrButton" type="button">
															<span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
														</button>
														<!--<input class="remove_input" type="button" value="X">-->
													</div>
													<div class="col-sm-7">
														{!! Form::select("assigned_person[$i]", $personals, $task->assignments[$i]->id_assigned_person, ['id' => "assign_person_$i"]) !!}
														{!! Form::select("assigned_categorie[$i]", $categories, $task->assignments[$i]->id_estimate_categorie, ['id' => "assign_categorie_$i"]) !!}
														{!! Form::number("assigned_hours[$i]", $task->assignments[$i]->assigned_hours, ['placeholder'=>trans('add_task.assignedHours'), 'required' => 'true', 'step'=>'any', 'id'=>"assign_hours_$i"])!!}
													</div>
												</div>
											@endunless
										@endfor
									@endif
								@endif
							@endif
							
							<div id="add_new_person"> </div>
						
						
						</div>
					</div>
					
				
				
				
				</div>


					
				 
					
