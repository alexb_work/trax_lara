@extends('layouts.app')


@section('content')
<?php 
	use App\task_status;
	use App\hours_per_task;
	use App\categorie_estimate;
?>
<?php
    $encrypter = app('Illuminate\Encryption\Encrypter');
        $encrypted_token = $encrypter->encrypt(csrf_token());
 ?>
<script>
$(function() {
	$( ".dialog" ).dialog({
		autoOpen: false
	});
});
function complete_task(task){
	$("#dialog_"+task).dialog('option', 'buttons',
	[
 	   {
 	     text: "{{trans('list_task.cancel')}}",
 	     click: function() {
 	       $( this ).dialog( "close" );
 	     }
 	   },
 	   {
 	   	 text: "{{trans('list_task.confirm')}}",
 	     click: function() {
 	       	var str = "task="+task;
        	var $_token = $('#token').val();
       		$.ajax({
            	type: 'post',
            	cache: false,
            	headers: { 'X-XSRF-TOKEN' : $_token }, 
            	url:"{{URL::route('completeTask')}}",
            	data: str, 
            	dataType: 'html',
            	success: function(){
            	    //console.log(edit_check);
            	    $("#status_"+task).empty();
            	    @if(LaravelLocalization::getCurrentLocale() == 'en')
            	    $("#status_"+task).html("<p>{{task_status::find(3)->nom_en}}</p>");
            	    @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
            	    $("#status_"+task).html("<p>{{task_status::find(3)->nom_fr}}</p>");
            	    @endif
            	    $("#status_"+task).show();
            	    $("#complete_button_"+task).hide();
            	    $("#time_track_"+task).hide();
            	    $("#message_"+task).html("<p>{{trans('list_task.completedTask')}}</p>")
            	    $( "#dialog_"+task ).dialog( "close" );

            	},
            	error: function(xhr, status, error) {
            	    var err = eval("(" + xhr.responseText + ")");
            	    alert(err.Message);
            	}
        	});
 	     }
 	   }
 	]
	)
	$("#dialog_"+task).dialog('open');
	
}
</script>
<input id="token" type="hidden" value="{{$encrypted_token}}">
<div id="xrx_page">
	<div class="row" style="margin-right: 0px; margin-left: 0px;">
		@include('partials.menu')
		
		<div class="col-md-10 xrx_content">
			<div id="menu" class="sub-menu">
				<a href="{{URL::route('taskList', ['project'=>$project->id_project])}}">{{ trans('list_task.myTasks') }}</a>&nbsp; | &nbsp; 
				<a href="{{URL::route('taskListAll', ['project'=>$project->id_project])}}">{{ trans('list_task.allTasks') }}</a>
			</div>
			<p>
				<a href="{{ URL::route('home') }}" class="btn btn-primary">{{ trans('list_task.returnDash') }}  </a>
				@if(Auth::user()->categorie==1)
				<a href="{{ URL::route('addTask', ['project'=>$project->id_project]) }}" class="btn btn-primary">{{ trans('list_task.addTask') }} </a>
				@endif
			</p>
			<h2>{{ trans('list_task.taskList') }}  {{$project->project_name}} : {{$project->req}} </h2>
			

			
			<div id="dashboard">
			
				<table class="table table-striped table-bordered table-condensed table-hover">
				    <thead>
				        <tr>
				          <th>{{ trans('list_task.brief') }}</th>
				          <th>{{ trans('list_task.status') }} </th>
				          <th>{{ trans('list_task.assigned') }}</th>
				          <th>{{ trans('list_task.type') }} </th>
						  <th>{{ trans('list_task.reqCompDate') }}</th>
						  <th>{{ trans('list_task.lastChange') }}</th>
						  <th>{{ trans('list_task.editing') }}</th>
				          <th>{{ trans('list_task.comment') }}</th>
						  <th>{{ trans('list_task.hours') }}</th>
						  <th>{{ trans('list_task.timeTrack') }} </th>
				        </tr>
				    </thead>
				      <tbody>
						@foreach($tasks as $task)
						<tr>
							<td> <a href="{{URL::route('editTask', ['task'=>$task->id_tasks])}}"> 
							@if(strlen($task->brief) > 25)
								{{substr($task->brief, 0, 25)}}...
							@else
								{{$task->brief}}
							@endif
							</a></td>
							<td>  
								<p id="status_{{$task->id_tasks}}">
									<?php
									 $taskStatus = task_status::where('id_task_status', $task->task_status)->get();
									 ?>
									 @foreach($taskStatus as $status)
									@if(LaravelLocalization::getCurrentLocale() == 'en')
                                    	{{ $status->nom_en }}
                                	@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                                    	{{ $status->nom_fr }}
                                	@endif
                                	@endforeach
								</p>
								@foreach($task->assignments as $assigned)
								@if($task->task_status != 3 && Auth::user()->id_personal == $assigned->id_assigned_person)
								<a class="btn btn-primary" id="complete_button_{{$task->id_tasks}}" onclick="complete_task({{$task->id_tasks}})">Complete</a>
								<?php break; ?>
								@endif
								@endforeach
							</td>
							<td> 
									<?php $previous = null; ?>
									@foreach($task->assignments as $assigned)
										@if($assigned->id_assigned_person !== (($previous!==null)?($previous->id_assigned_person):null))
										- {{ $assigned->user->prenom_personal }} {{ $assigned->user->nom_personal }} <br>
										<?php $previous = $assigned ?>
										@endif

									@endforeach

							</td>
							@if(LaravelLocalization::getCurrentLocale() == 'en')
								<td> {{ $task->taskType->name_en}}</td>
							@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
								<td> {{ $task->taskType->name_fr}}</td>
							@endif
							<td> {{ $task->req_date }} </td>
							<td> {{ $task->last_change }} </td>
							<td>
										
										
										{{ $task->editor->prenom_personal }} {{ $task->editor->nom_personal }}	
													
							
							</td>
							<td> {{ $task->comment }} </td>
							<td> {{ $task->hours->sum('hours_completed') }}/{{ $task->assignments->sum('assigned_hours') }}</td>
							<td> 
							<?php $timeTrack=false; ?>
							@foreach($task->assignments as $assigned)
								@if($task->task_status != 3 && Auth::user()->id_personal == $assigned->id_assigned_person)
									<a href="{{URL::route('time', ['task'=>$task->id_tasks])}}" class="btn btn-primary" id="time_track_{{$task->id_tasks}}">{{ trans('list_task.timeTrack') }}  </a>
									<p id="message_{{$task->id_tasks}}">
									<?php 
										$timeTrack = true;
										break; 
									?>
								@endif
							@endforeach
							@if($task->task_status == 3)
							<p>{{trans('list_task.completedTask')}}</p>
							@elseif(!$timeTrack)
							<p>{{trans('list_task.notYourTask')}}</p>
							@endif
							</td>
						</tr>
						<div id="dialog_{{$task->id_tasks}}" class="dialog" title="{{trans('list_task.confirm') }}">

							<p>{{trans('list_task.confirmMessage')}}</p>
							<?php
								$hoursCategories = hours_per_task::groupBy('id_categorie_estimate')->where('id_task', $task->id_tasks)->selectRaw('sum(hours_completed) as hours, id_categorie_estimate')->pluck('hours', 'id_categorie_estimate');
							?>
							<p>
							@foreach($hoursCategories as $key=>$hour)
							@if(LaravelLocalization::getCurrentLocale() == 'en')
							<?php $category = categorie_estimate::where('id_categorie_estimate', $key)->select('title_en')->get(); ?>
								{{$category[0]->title_en}}: {{$hour}}<br>
							@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
								<?php $category = categorie_estimate::where('id_categorie_estimate', $key)->select('title_fr')->get(); ?>
								{{$category[0]->title_en}}: {{$hour}}<br>
							@endif
							@endforeach
						</div>
						@endforeach

				      </tbody>
				</table>			
			
			</div><!-- end dashboard -->

		</div>
		
		<div class="clear"> &nbsp;</div>
	</div><!-- end #row -->

</div><!-- end #xrx_page -->




@endsection