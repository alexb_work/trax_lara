@extends('layouts.app')


@section('content')
<link rel="stylesheet" href="{{ URL::asset('css/jquery-ui-1.10.0.custom.min.css') }}" type="text/css" />    
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.ui.timepicker.css') }}" type="text/css" />
    <link href=' //fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:300italic,400italic,700italic,400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/jquery1.11.1/jquery.min.js') }}"></script> 
    
    <!--<script src="../js/jquery1.9.0/jquery-1.9.0.min.js"></script>-->

    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.ui.core.min.js') }}"></script>

    <script src="{{ URL::asset('js/jquery-ui-1.10.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.ui.timepicker.js') }}"></script>
    
    <!--<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">-->
    <link href="{{ URL::asset('css/xrx_brand.css') }}" rel="stylesheet">

    <!-- these css and js files are required by php grid -->
    <link rel="stylesheet" href="{{ URL::asset('phpgrid/lib/js/themes/blitzer/jquery-ui.custom.css') }}"></link>  
    <link rel="stylesheet" href="{{ URL::asset('phpgrid/lib/js/jqgrid/css/ui.jqgrid.css') }}"></link> 
    <script src="{{ URL::asset('phpgrid/lib/js/jquery.min.js') }}" type="text/javascript')"></script>
    @if(LaravelLocalization::getCurrentLocale() == 'en')
    	<script src="{{ URL::asset('phpgrid/lib/js/jqgrid/js/i18n/grid.locale-en.js') }}" type="text/javascript"></script>
    @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
    	<script src="{{ URL::asset('phpgrid/lib/js/jqgrid/js/i18n/grid.locale-fr.js') }}" type="text/javascript"></script>
    @endif
    <script src="{{ URL::asset('phpgrid/lib/js/jqgrid/js/jquery.jqGrid.min.js') }}" type="text/javascript"></script>  
    <script src="{{ URL::asset('phpgrid/lib/js/themes/jquery-ui.custom.min.js') }}" type="text/javascript"></script>
    <!-- these css and js files are required by php grid -->
<script type="text/javascript">
$( document ).ready(function() {
    $( "#task_id_select" ).change(function() {
		window.location.href = "http://localhost:8082/trax_dev_env/public/task/time/"+$( "#task_id_select" ).val();
	});
	$( "#project_id_select" ).change(function() {
		window.location.href = "http://localhost:8082/trax_dev_env/public/task/time/"+$( "#project_id_select" ).val();
	});
	$('body').on('DOMNodeInserted', 'input[name="hours_non_billable"]', function () {
      $(this).change(function(){
		var task_id = $(this).attr('id').split('_')[0];
		if($(this).val() != 0 && $(this).val() != ""){
			
			if($('textarea[name=hours_comment]').val()==null)
				var prevText = "";
			else
				var prevText = $('textarea[name=hours_comment]').val();

			if(prevText.indexOf('-Non-billable comment:')!=-1 || prevText.indexOf('-Non-facturable commentaire:')!=-1){
				return false;
			}

			@if(LaravelLocalization::getCurrentLocale() == 'en')
				$('textarea[name=hours_comment]').val(prevText +'-Non-billable comment:\n'); 
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
				$('textarea[name=hours_comment]').val(prevText+'-Non-facturable commentaire:\n'); 
			@endif
		}
		else{
			@if(LaravelLocalization::getCurrentLocale() == 'en')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Non-billable comment:', ''));
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Non-facturable commentaire:', ''));
			@endif
		}
	});
	});

	$('body').on('DOMNodeInserted', 'select[name="tarif_horaire"]', function () {
      $(this).change(function(){
      	var task_id = $(this).attr('id').split('_')[0];
		if($(this).val() != "1"){
			
			if($('textarea[name=hours_comment]').val()==null)
				var prevText = "";
			else
				var prevText = $('textarea[name=hours_comment]').val();
			if(prevText.indexOf('-Rate comment:')!=-1 || prevText.indexOf('-Tarif horaire commentaire:')!=-1)
				return false;
			@if(LaravelLocalization::getCurrentLocale() == 'en')
			$('textarea[name=hours_comment]').val(prevText +'-Rate comment:\n'); 
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
			$('textarea[name=hours_comment]').val(prevText+'-Tarif horaire commentaire:\n'); 
			@endif
		}
		else{
			@if(LaravelLocalization::getCurrentLocale() == 'en')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Rate comment:', ''));
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Tarif horaire commentaire:', ''));
			@endif
		}
	});
	});

	$('body').on('DOMNodeInserted', 'input[name="hours_completed"]', function () {
      $(this).change(function(){
      	var task_id = $(this).attr('id').split('_')[0];
		if(parseInt($(this).val()) + parseInt({{$hoursSoFar}}) > parseInt({{$assignments}})){
			
			if($('textarea[name=hours_comment]').val()==null)
				var prevText = "";
			else
				var prevText = $('textarea[name=hours_comment]').val();
			if(prevText.indexOf('-Overtime comment:')!=-1 || prevText.indexOf('-Surtemps commentaire:')!=-1)
				return false;
			@if(LaravelLocalization::getCurrentLocale() == 'en')
			$('textarea[name=hours_comment]').val(prevText +'-Overtime comment:\n'); 
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
			$('textarea[name=hours_comment]').val(prevText+'-Surtemps commentaire:\n'); 
			@endif
		}
		else{
			@if(LaravelLocalization::getCurrentLocale() == 'en')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Overtime comment:', ''));
			@elseif(LaravelLocalization::getCurrentLocale() == 'fr')
			$('textarea[name=hours_comment]').val($('textarea[name=hours_comment]').val().replace('-Surtemps commentaire:', ''));
			@endif
		}
	});
	});
	
});

function grid_onload(){
	var grid = $("#list1")

	billable = grid.jqGrid('getCol', 'hours_completed', false, 'sum');

	non_billable = grid.jqGrid('getCol', 'hours_non_billable', false, 'sum');

	grid.jqGrid('footerData','set', {hours_completed:billable, hours_non_billable:non_billable});
}
	
</script>
<div id="xrx_page">
	<div class="row" style="margin-right: 0px; margin-left: 0px;">
		@include('partials.menu')
		<div class="col-md-10 xrx_content">
			<p>
				<a href="{{ URL::route('taskList', $task->id_project) }}" class="btn btn-primary">{{ trans('list_task.returnDash') }}  </a>
			</p>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="task">{{trans('time_track.project')}}</label>
						<div class="col-sm-6">
							{!! Form::select('projects', $projects, $project->tasks->first()->id_tasks, ['id'=>'project_id_select']) !!}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="task">{{trans('time_track.task')}}</label>
						<div class="col-sm-6">
							{!! Form::select('tasks', $tasks, $task->id_tasks, ['id'=>'task_id_select']) !!}
						</div>
					</div>
				<div class="form-group col-md-10">
					{!! $grid !!}
				</div>
			</div>
		</div>
	</div>
	@endsection
		  