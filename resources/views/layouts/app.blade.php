<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XEROX - TRAX</title>

    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui-1.10.0.custom.min.css') }}" type="text/css" />    
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.ui.timepicker.css') }}" type="text/css" />
    <link href=' //fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:300italic,400italic,700italic,400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/jquery1.11.1/jquery.min.js') }}"></script> 
    
    <script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
    
    <!--<script src="../js/jquery1.9.0/jquery-1.9.0.min.js"></script>-->

    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/jquery.ui.core.min.js') }}"></script>

    <script src="{{ URL::asset('js/jquery-ui-1.10.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.ui.timepicker.js') }}"></script>
    
    <!--<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">-->
    <link href="{{ URL::asset('css/xrx_brand.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->

  </head>
  
  <body> 

<!--TODO php block in user/header.php -->

<div class="xerox_navbar navbar navbar-static-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{{ URL::route('home') }}" class="navbar-brand xerox_brand" style="font-size:3em; font-weight:lighter"><img src="{{ URL::asset('images/xrx_logo_old.png') }}" style="padding-right:10px" ><img src="{{ URL::asset('images/logo-trax.jpg') }}" class="vertical_line" style="padding-left:10px" /></a>
    
    </div>
    <div class="float_r" style="margin-top:10px">
    @if(Auth::check())
               {{trans('master.welcome')}}{{Auth::user()->prenom_personal}} {{Auth::user()->nom_personal}}
               </div>
    @endif
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">

      <ul class="nav navbar-nav navbar-right" style="clear:right">

      @if(Auth::check())
        
        <li>
          <a href="{{URL::route('logout') }}">{{trans('master.logout')}}</a>
        </li>  
        @endif
        <li style="margin-top: 5px; margin-right: 10px">|</li>
        <li>

        @if(LaravelLocalization::getCurrentLocale() == "en")
            <a rel="alternate" hreflang="fr" href="{{LaravelLocalization::getLocalizedURL('fr') }}">
            FR
            </a>
        @elseif(LaravelLocalization::getCurrentLocale() == "fr")
            <a rel="alternate" hreflang="en" href="{{LaravelLocalization::getLocalizedURL('en') }}">
            EN
            </a>
        @endif
        </li>
        
        


        
      </ul>
            <!-- User Navigation: User Link -->
                <!-- <span class="glyphicon glyphicon-user"></span> -->
                

    </nav>

</div> 
</div>
<?php
  use Illuminate\Support\Facades\Input;
?>

@yield('content')

<footer role="contentinfo" class="bs-docs-footer">
    <div class="container">
        <div class="footer_inner">
          <img src="{{ URL::asset('images/xrx_logo_old.png') }}" /><br>
            © 2013 - {{ \Carbon\Carbon::now()->year }} Xerox Corporation. {{ trans('login.rights') }}.
        </div>
    </div>
</footer>

</body>
</html>

