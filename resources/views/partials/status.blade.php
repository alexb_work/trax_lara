    	<span style='color:{{$color}}'>
					@if(LaravelLocalization::getCurrentLocale() == 'en')
                        {{ $newProject->status->title_status_en }}
                    @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                        {{ $newProject->status->title_status_fr }}
                    @endif
		</span>