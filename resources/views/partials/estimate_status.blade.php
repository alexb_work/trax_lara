    	<span>
					@if(LaravelLocalization::getCurrentLocale() == 'en')
                        {{ $newEstimate->status->name_en }}
                    @elseif(LaravelLocalization::getCurrentLocale() == 'fr')
                        {{ $newEstimate->status->name_fr }}
                    @endif
		</span>