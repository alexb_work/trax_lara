<div id="menu">
	<a href="{{URL::route('all')}}">{{ trans('menu.allProjects') }}</a>&nbsp; | &nbsp; 
	<a href="{{URL::route('home')}}">{{ trans('menu.myProjects') }}</a>
	@if(Auth::user()->categorie == 1)
	&nbsp; | &nbsp; 
	<a href="{{URL::route('archives')}}">{{ trans('menu.archProjects') }}</a>&nbsp; | &nbsp; 
	<a href="{{URL::route('home')}}">{{ trans('menu.overview') }}</a>
	@endif
	@if(Auth::user()->assignedTasks !== null)
	&nbsp; | &nbsp; 
	<a href="{{URL::route('time', ['task'=>Auth::user()->assignedTasks->last()->id_task])}}">{{ trans('menu.timeTrack') }}</a>
	@endif
	
</div>
<hr>
