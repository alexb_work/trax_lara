<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XEROX - TRAX</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/jquery1.11.1/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    
    <link href="{{ URL::asset('css/xrx_brand.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
       
  </head>
  
  <body>
<div class="xerox_navbar navbar navbar-static-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{{ URL::route('home') }}" class="navbar-brand xerox_brand" style="font-size:3em; font-weight:lighter"><img src="{{ URL::asset('images/xrx_logo_old.png') }}" style="padding-right:10px" ><img src="{{ URL::asset('images/logo-trax.jpg') }}" class="vertical_line" style="padding-left:10px" /></a>
    
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">

      <ul class="nav navbar-nav navbar-right">

        <li>

        @if(LaravelLocalization::getCurrentLocale() == "en")
            <a rel="alternate" hreflang="fr" href="{{LaravelLocalization::getLocalizedURL('fr') }}">
            FR
            </a>
        @elseif(LaravelLocalization::getCurrentLocale() == "fr")
            <a rel="alternate" hreflang="en" href="{{LaravelLocalization::getLocalizedURL('en') }}">
            EN
            </a>
        @endif
        </li>
        
        


        
      </ul>
    </nav>
  </div>
</div> 

    <div class="container" style="background-image:url("{{ URL::asset('images/bg_'. LaravelLocalization::getCurrentLocale() .'.jpg') }}");background-repeat: no-repeat; background-position: right center;">

      <form role="form" class="form-signin" method="post" action="{{ url('/login') }}" >
      {{ csrf_field() }}
        <h3 class="form-signin-heading" style="margin-bottom: 30px;">{{ trans('login.connexion') }}</h3>

        <div class="form-group">
            <label for="exampleInputEmail1" style="margin-bottom: 10px;">{{ trans('login.email') }}</label>
            <input type="email" name="mail_personal" class="form-control" id="exampleInputEmail1" placeholder="{{ trans('login.enterEmail') }}" required>
            @if ($errors->has('mail_personal'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mail_personal') }}</strong>
                                    </span>
                                @endif
        </div>
          
        <div class="form-group">
            <label for="exampleInputPassword1" style="margin-bottom: 10px; margin-top: 7px;">{{ trans('login.password') }}</label>
            <input type="password" name="password"   class="form-control" id="exampleInputPassword1" placeholder="{{ trans('login.password') }}" required>
            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
        </div>        
        
        <button type="submit" class="btn btn-lg btn-primary btn-block" style="margin-bottom: 20px; margin-top: 30px;">{{ trans('login.signIn') }} </button>
      </form>
    
    </div>
    
<footer role="contentinfo" class="bs-docs-footer">
    <div class="container">
        <div class="footer_inner">
          <img src="{{ URL::asset('images/xrx_logo_old.png') }}" /><br>
            © 2013 - {{ \Carbon\Carbon::now()->year }} Xerox Corporation. {{ trans('login.rights') }}.
        </div>
    </div>
</footer>

</body>
</html>