<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'text_other_job_type' => [
            'required_if' => 'You must enter a name for the other job type.',
        ],
        'advisor.1' => [
            'different' => 'The first and second consultants must be different.',
        ],
        'allocated_hours' => [
            'hours_valid' => "With the allocated hours for this task, you exceed the estimated time required to complete the project.",
        ],
        'idproject' => [
            'task_closed' => "The project you are trying to close still has tasks that are not completed. Please review the tasks before trying again.",
        ],
        'estimated_hours' => [
            'estimate_hours' => "The estimated hours for this project are below the combined amount of hours allowed for the tasks.",
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'advisor.0'            => 'first consultant',
        'advisor.1'            => 'second consulant',
        'job_type'              => 'job type',
        'text_other_job_type'   => 'other job type',
        'project_desc'          => 'project description',
        'name_client'           => 'client name',
        'req_date'              => 'requested completion date',
        'qa_price'              => "quality assurance price",
        'qa_hours'              => "quality assurance hours",
        'qa_percent'            => "quality assurance percent",
        'pm_price'              => 'project management price',
        'pm_hours'              => 'project management hours',
        'pm_percent'            => 'project management percent',

        'p_province'            => 'province',
        'p_received_date'       => 'received date',
        'p_due_date'            => 'due date',
        'p_due_time'            => 'due time',
        'p_start_date'          => 'start date',
        'p_start_time'          => 'start time',
        'p_work_date'           => 'work date', 
        'p_work_time'           => 'work time',
        'p_tel'                 => 'phone',
        'p_adress'              => 'adress',
        'delivery_type'         => 'delivery type',
        'p_purolator'           => 'purolator number',
        'p_doc_name.*'          => 'document name (reprography)',
        'p_one_sided.*'         => 'one sided (reprography)',
        'p_double_sided.*'      => 'double sided (reprography)',
        'p_nb_copies.*'         => 'number of copies (reprography)',
        'supply_price.*'        => 'supply item (reprography)',
        'p_doc_color.*'         => 'document color (reprography)',
        'supply_item.*'         => 'supply item (supplies)',
        'supply_qty.*'          => 'quantity (supplies)',
        'outsource_name.*'      => 'name (outsource)',
        'outsource_code.*'      => 'code (outsource)',
        'outsource_cost.*'      => 'cost (outsource)',
        's_quantity.*'          => 'staple quantity (finish - stapled)',
        'book_qty.*'            => 'booklet quantity (finish - booklet)',
        'book_sheets.*'         => 'booklet sheets (finish - booklet)',
        'book_size.*'           => 'booklet size (finish - booklet)',
        'sc_book_qty.*'         => 'book quantity (finish - spiral/cerlox)',
        'sc_book_sheets.*'      => 'quantity of sheets/book (finish - spiral/cerlox)',
        'tape_qty.*'            => 'tape quantity (finish - tape binding)',
        'bind_ins_qty.*'        => 'binder insert quantity (finish - binder)',
        'drill_holes.*'         => 'holes (finish - drilling)',
        'drill_sheets.*'        => 'sheets (finish - drilling)',
        'cut_final_size.*'      => 'final cutsize (finish - cuts)',
        'cut_qty.*'             => 'cuts quantity(finish - cuts)',
        'cut_sheet_qty.*'       => 'cuts sheet quantity(finish - cuts)',
        'pad_size.*'            => 'pads size (finish - pads)',
        'pad_qty.*'             => 'pads quantity (finish - pads)',
        'pad_sheet_qty.*'       => 'pads sheet quantity(finish - pads)',
        'fold_qty.*'            => 'folds quantity(finish - folds)',
        'fold_sheet_qty.*'      => 'folds sheet quantity (finish - folds)',
        'insert_env_size.*'     => 'envelope size (finish - insertion)',
        'insert_qty.*'          => 'insertion quantity (finish - insertion)',
        'assembly_quantity_lift.*'=> 'quantity lift (finish - assembly)',
        'shrink_qty.*'          => 'shrink wrap quantity (finish - shrink wrap)',
    ],

];
