<?php

return [
    'connexion' => 'Sign in to Trax',
    'email'		=> 'Email address',
    'enterEmail'=> 'Enter email',
    'password'	=> 'Password',
    'signIn'	=> 'Sign in',
    'rights'	=> 'All rights reserved'
];