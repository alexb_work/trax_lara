<?php

return [
    'returnList'    => 'Return to the estimate list',
    'newEstimate'   => 'New estimate for ',
    'editEstimate'  => 'Estimate for ',
    'description'   => 'Estimate decription',
    'estimateBefore'=> 'Estimate price without buffer',
    'estimateHours' => 'Total project hours',
    'showDetails'   => 'Show details',
    'hideDetails'   => 'Hide details',
    'test'			=> '$ Testing',
    'testHours'     => 'Hours testing',
    'testPercent'   => '% Testing',
    'qa'			=> '$ Quality assurance',
    'qaHours'       => 'Hours quality assurance',
    'qaPercent'     => '% Quality assurance',
    'pm'			=> '$ Project management',
    'pmHours'       => 'Hours project management',
    'pmPercent'     => '% Project management',
    'print'         => '$ Printing',
    'totalPrice'    => 'Total estimate price (with added buffer)',
    'reqDays'       => 'Required number of days',
    'calcPriceExp'  => 'The price was calculated by taking the time allocated for each task multiplied by the rate associated to the type of the task and adding 10% as a buffer.',
    'date'          => 'Date',
    'prepFor'       => 'Prepared for',
    'prepBy'        => 'Prepared by',
    'estimateBody'  => 'Body of the estimate',
    'title'         => 'Title',
    'submit'        => 'Submit'
    

];
