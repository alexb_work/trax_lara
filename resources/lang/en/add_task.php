<?php

return [
    'returnDash'    => 'Return to your dashboard',
    'newTask'	    => 'New Task for ',
    'editTask'      => 'Edit Task',
    'addTask'       => 'Add Task ',
    'brief'         => 'Brief:',
    'reqCompDate'   => 'Requested completion Date:',
    'comment'       => 'Comment:',
    'fileDesc'      => 'File description',
    'status'        => 'Status:',
    'allocHours'    => 'Allocated hours:',
    'files'         => 'File(s):',
    'attachments'   => 'Attachments:',
    'assignation'   => 'Assignation:',
    'personAssign'  => 'Person assigned:',
    'submit'        => 'Submit',
    'reset'         => 'Reset',
    'type'          => 'Task type:',
    'assignedHours' => 'Assigned hours'
    

];