<?php

return [
    'myTasks'       => 'My tasks',
    'allTasks'      => 'All tasks',
    'returnDash'    => 'Return to your dashboard',
    'addTask'	    => 'Add task',
    'taskList'      => 'Task list for',
    'brief'         => 'Brief',
    'status'        => 'Status',
    'assigned'      => 'Assigned to',
    'reqCompDate'   => 'Requested Completion Date',
    'lastChange'    => 'Last change',
    'editing'       => 'Editing by',
    'comment'       => 'Comment',
    'hours'         => 'Hours',
    'timeTrack'     => 'Time Track',
    'notYourTask'   => 'Your are not assigned to this task',
    'completedTask' => 'This task is complete',
    'type'          => "Task type",
    'confirm'       => 'Confirm',
    'confirmMessage'=> 'Do you really want to complete this task with the following hours?',
    'cancel'        => 'Cancel'
    

];