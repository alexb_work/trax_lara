<?php

return [
    'projectDelete'	=> 'Delete project',
    'cancel'		=> 'Cancel',
    'addProject'	=> 'Add project',
    'requisition'	=> 'Requisition',
    'clickEdit'		=> 'Click to edit project',
    'name'			=> 'Name',
    'clickManage'	=> 'Click to manage tasks',
    'clientName'	=> 'Client name',
    'requestCompDate'=> 'Requested Completion Date',
    'totalTasks'	=> 'Total Tasks',
    'projectType'	=> 'Project type',
    'status'		=> 'Status',
    'lastChange'	=> 'Last change',
    'clickDelete'	=> 'Click to delete job',
    'clickSubmission'=>'Click to manage submission',
    'changeStatus'	=> 'Change status',
    'statusError'	=> 'The project status should be "in progress" to add task',
    'close'			=> 'Close',
    'deleteConfirm'	=> 'This project will be archived. Are you sure?',
    'alertOvertime' => 'This project has one or more task(s) that are over the allocated time'
];