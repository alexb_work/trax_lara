<?php

return [
    'returnDash'    => 'Return to your dashboard',
    'addEstimate'   => 'Add estimate',
    'estimateList'  => 'Estimate list for',
    'revision'		=> 'Revision #',
    'description'	=> 'Estimate description',
    'price'         => 'Total price',
    'status'        => 'Status',
    'view'          => 'View',
    'edit'          => 'Edit',
    'rejectReason'	=> 'Rejection reason',
    'rejectQuestion'=> 'What is the reason for the rejection of this estimate?',
    'confirm'		=> 'Confirm',
    'cancel'		=> 'Cancel'
    

];