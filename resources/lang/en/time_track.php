<?php

return [
	'project'		=> 'Project',
	'task'			=> 'Task',
    'addHours' 		=> 'Add hours',
    'billable'		=> 'Billable',
    'nbrHours'		=> 'Enter the number of hours',
    'nonBillable'	=> 'Non-Billable',
    'rate'			=> 'Rate',
    'category'		=> 'Category/Service',
    'comment'		=> 'Comment',
    'date'			=> "Hours' date"
];