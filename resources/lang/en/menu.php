<?php

return [
    'projects' 		=> 'Projects',
    'allProjects'	=> 'All Projects',
    'myProjects'	=> 'My Projects',
    'archProjects'	=> 'Archived Projects',
    'addProject'	=> 'Add new project ',
    'overview'		=> 'Overview',
    'timeTrack'		=> 'Time track'
];