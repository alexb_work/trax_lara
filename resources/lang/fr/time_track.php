<?php

return [
	'project'		=> 'Projet',
	'task'			=> 'Tâche',
    'addHours' 		=> 'Ajouter vos heures',
    'billable'		=> 'Facturable',
    'nbrHours'		=> "Entrez le nombre d'heure",
    'nonBillable'	=> 'Non-Facturable',
    'rate'			=> 'Tarif',
    'category'		=> 'Catégorie/Service',
    'comment'		=> 'Commentaire',
    'date'			=> 'Date des heures'
];