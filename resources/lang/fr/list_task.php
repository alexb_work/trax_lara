<?php

return [
    'myTasks'       => 'Mes tâches',
    'allTasks'      => 'Toutes les tâches',
    'returnDash'    => 'Retour à votre tableau de bord',
    'addTask'	    => 'Ajouter une tâche',
    'taskList'      => 'Liste des tâches pour ',
    'brief'         => 'Brief',
    'status'        => 'Statut',
    'assigned'      => 'Assigné à',
    'reqCompDate'   => 'Date de livraison demandée',
    'lastChange'    => 'Dernier changement',
    'editing'       => 'Modification par',
    'comment'       => 'Commentaire',
    'hours'         => "Nbr d'heures",
    'timeTrack'     => 'Horaire',
    'notYourTask'   => "Vous n'êtes pas assigné à cette tâche",
    'completedTask' => 'Cette tâche est completée',
    'type'          => "Type de tâche",
    'confirm'       => 'Confirmation',
    'confirmMessage'=> 'Voulez vous vraiment compléter cette tâche avec les heures suivantes?',
    'cancel'        => 'Annuler'
    

];