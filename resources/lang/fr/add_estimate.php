<?php

return [
    'returnList'    => 'Retour à la liste des offres de service',
    'newEstimate'   => 'Nouvelle offre de service pour ',
    'editEstimate'  => 'Offre de service pour ', 
    'description'   => 'Description de la soumission',   
    'estimateBefore'=> 'Prix estimé sans tampon',
    'estimateHours' => 'Total des heures du projet',
    'moreDetails'   => 'Plus de détails',
    'lessDetails'   => 'Moins de détails',
    'test'			=> '$ Test',
    'qa'			=> "$ Assurance de qualité",
    'pm'			=> '$ Gestion de projet',
    'testHours'     => 'Temps de test',
    'testPercent'   => '% Test',
    'qaHours'       => "Temps d'assurance de qualité",
    'qaPercent'     => "% Assurance de qualité",
    'pmHours'       => 'Temps de gestion',
    'pmPercent'     => '% Gestion',
    'print'         => '$ Impression',
    'totalPrice'    => 'Prix estimé total',
    'reqDays'       => 'Nombre de jours requis',
    'calcPriceExp'  => 'Le prix affiché à été calculé en multipliant le temps associé à chaque tâche par le prix associé à son type, tout en ajoutant une marge de 10%',
    'date'          => 'Date',
    'prepFor'       => 'Préparée pour',
    'prepBy'        => 'Préparée par',
    'estimateBody'  => 'Contenu de la soumission',
    'title'         => 'Titre',
    'submit'        => 'Soumettre'
    

];