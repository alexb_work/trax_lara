<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    'accepted'             => 'Le champ :attribute doit être accepté.',
    'active_url'           => "Le champ :attribute n'est pas une URL valide.",
    'after'                => 'Le champ :attribute doit être une date postérieure au :date.',
    'alpha'                => 'Le champ :attribute doit seulement contenir des lettres.',
    'alpha_dash'           => 'Le champ :attribute doit seulement contenir des lettres, des chiffres et des tirets.',
    'alpha_num'            => 'Le champ :attribute doit seulement contenir des chiffres et des lettres.',
    'array'                => 'Le champ :attribute doit être un tableau.',
    'before'               => 'Le champ :attribute doit être une date antérieure au :date.',
    'between'              => [
        'numeric' => 'La valeur de :attribute doit être comprise entre :min et :max.',
        'file'    => 'Le fichier :attribute doit avoir une taille entre :min et :max kilo-octets.',
        'string'  => 'Le texte :attribute doit avoir entre :min et :max caractères.',
        'array'   => 'Le tableau :attribute doit avoir entre :min et :max éléments.',
    ],
    'boolean'              => 'Le champ :attribute doit être vrai ou faux.',
    'confirmed'            => 'Le champ de confirmation :attribute ne correspond pas.',
    'date'                 => "Le champ :attribute n'est pas une date valide.",
    'date_format'          => 'Le champ :attribute ne correspond pas au format :format.',
    'different'            => 'Les champs :attribute et :other doivent être différents.',
    'digits'               => 'Le champ :attribute doit avoir :digits chiffres.',
    'digits_between'       => 'Le champ :attribute doit avoir entre :min et :max chiffres.',
    'distinct'             => 'Le champ :attribute a une value dupliquée.',
    'email'                => 'Le champ :attribute doit être une adresse email valide.',
    'exists'               => 'Le champ :attribute sélectionné est invalide.',
    'filled'               => 'Le champ :attribute est obligatoire.',
    'image'                => 'Le champ :attribute doit être une image.',
    'in'                   => 'Le champ :attribute est invalide.',
    'in_array'             => 'Le champ :attribute n\'existe pas dans :other.',
    'integer'              => 'Le champ :attribute doit être un entier.',
    'ip'                   => 'Le champ :attribute doit être une adresse IP valide.',
    'json'                 => 'Le champ :attribute doit être un document JSON valide.',
    'max'                  => [
        'numeric' => 'La valeur de :attribute ne peut être supérieure à :max.',
        'file'    => 'Le fichier :attribute ne peut être plus gros que :max kilo-octets.',
        'string'  => 'Le texte de :attribute ne peut contenir plus de :max caractères.',
        'array'   => 'Le tableau :attribute ne peut avoir plus de :max éléments.',
    ],
    'mimes'                => 'Le champ :attribute doit être un fichier de type : :values.',
    'min'                  => [
        'numeric' => 'La valeur de :attribute doit être supérieure à :min.',
        'file'    => 'Le fichier :attribute doit être plus gros que :min kilo-octets.',
        'string'  => 'Le texte :attribute doit contenir au moins :min caractères.',
        'array'   => 'Le tableau :attribute doit avoir au moins :min éléments.',
    ],
    'not_in'               => "Le champ :attribute sélectionné n'est pas valide.",
    'numeric'              => 'Le champ :attribute doit contenir un nombre.',
    'present'              => 'Le champ :attribute doit être présent.',
    'regex'                => 'Le format du champ :attribute est invalide.',
    'required'             => 'Le champ :attribute est obligatoire.',
    'required_if'          => 'Le champ :attribute est obligatoire quand la valeur de :other est :value.',
    'required_unless'      => 'Le champ :attribute est obligatoire sauf si :other est :values.',
    'required_with'        => 'Le champ :attribute est obligatoire quand :values est présent.',
    'required_with_all'    => 'Le champ :attribute est obligatoire quand :values est présent.',
    'required_without'     => "Le champ :attribute est obligatoire quand :values n'est pas présent.",
    'required_without_all' => "Le champ :attribute est requis quand aucun de :values n'est présent.",
    'same'                 => 'Les champs :attribute et :other doivent être identiques.',
    'size'                 => [
        'numeric' => 'La valeur de :attribute doit être :size.',
        'file'    => 'La taille du fichier de :attribute doit être de :size kilo-octets.',
        'string'  => 'Le texte de :attribute doit contenir :size caractères.',
        'array'   => 'Le tableau :attribute doit contenir :size éléments.',
    ],
    'string'               => 'Le champ :attribute doit être une chaîne de caractères.',
    'timezone'             => 'Le champ :attribute doit être un fuseau horaire valide.',
    'unique'               => 'La valeur du champ :attribute est déjà utilisée.',
    'url'                  => "Le format de l'URL de :attribute n'est pas valide.",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
        'text_other_job_type' => [
            'required_if' => 'Vous devez entrez un type de projet autre.',
        ],
        'advisor.1' => [
            'different' => 'Le premier et le deuxième consultant doivent être différents.',
        ],
        'req_date' => [
            'after' => "La date de fin demandé doit être une date à partir d'aujourd'hui.",
        ],
        'allocated_hours' => [
            'hours_valid' => "Avec le nombre d'heures alloués, vous dépassez le temps estimé pour compléter le projet.",
        ],
        'idproject' => [
            'task_closed' => "Le projet que vous tentez de supprimer contient des tâches qui ne sont pas complété. Veuillez re-vérifier avant de tenter à nouveau.",
        ],
        'estimated_hours' => [
            'estimate_hours' => "Le nombre d'heures estimées pour ce projet est inférieur aux total des heures alloué pour les tâches.",
        ]
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes'           => [
        'name'                  => 'Nom',
        'username'              => 'Pseudo',
        'email'                 => 'E-mail',
        'first_name'            => 'Prénom',
        'last_name'             => 'Nom',
        'password'              => 'Mot de passe',
        'password_confirmation' => 'Confirmation du mot de passe',
        'city'                  => 'Ville',
        'country'               => 'Pays',
        'address'               => 'Adresse',
        'phone'                 => 'Téléphone',
        'mobile'                => 'Portable',
        'age'                   => 'Age',
        'sex'                   => 'Sexe',
        'gender'                => 'Genre',
        'day'                   => 'Jour',
        'month'                 => 'Mois',
        'year'                  => 'Année',
        'hour'                  => 'Heure',
        'minute'                => 'Minute',
        'second'                => 'Seconde',
        'title'                 => 'Titre',
        'content'               => 'Contenu',
        'description'           => 'Description',
        'excerpt'               => 'Extrait',
        'date'                  => 'Date',
        'time'                  => 'Heure',
        'available'             => 'Disponible',
        'size'                  => 'Taille',
        'project_name'          => 'nom du project',
        'end_date'              => 'date de fin du projet',
        'end_time'              => 'temps de fin du projet',
        'delivery_date'         => 'date de livraison',
        'advisor.0'             => 'premier consultant',
        'advisor.1'             => 'deuxième consultant',
        'job_type'              => 'type de projet',
        'text_other_job_type'   => 'autre type de projet',
        'project_desc'          => 'description du projet',
        'estimated_hours'       => "nombres d'heures",
        'name_client'           => 'nom du client',
        'req_date'              => 'date de fin demandé',
        'test_price'            => 'coût de test',
        'test_hours'            => 'heures de test',
        'test_percent'          => 'pourcentage du projet dédié aux tests',
        'qa_price'              => "coût d'assurance de qualité",
        'qa_hours'              => "heures pour l'assurance de qualité",
        'qa_percent'            => "pourcentage du projet dédié à l'assurance de qualité",
        'pm_price'              => 'coût de gestion de projet',
        'pm_hours'              => 'heures de gestion de projet',
        'pm_percent'            => 'pourcentage du projet dédié à la gestion',
        'total_price'           => 'prix total',
        'prepared_for'          => 'préparé pour',
        'prepared_by'           => 'préparé par',
        'brief'                 => 'instructions',
        'assigned_person.*'     => 'personne assignée',
        'assigned_hours.*'      => 'heures assignées',
        'title.*'               => 'titre'

        'p_province'            => 'province',
        'p_received_date'       => 'date de réception',
        'p_due_date'            => "date d'échéance",
        'p_due_time'            => "temps d'échéance",
        'p_start_date'          => 'date de début',
        'p_start_time'          => 'temps de début',
        'p_work_date'           => "date d'exigence du travail", 
        'p_work_time'           => "temps d'exigence du travail",
        'p_tel'                 => 'numéro de téléphone',
        'p_adress'              => 'addresse',
        'delivery_type'         => 'livraison',
        'p_purolator'           => 'numéro purolator',
        'p_doc_name.*'          => 'nom du document (reprographie)',
        'p_one_sided.*'         => 'recto (reprographie)',
        'p_double_sided.*'      => 'recto verso (reprographie)',
        'p_nb_copies.*'         => 'nombre de copies (reprographie)',
        'supply_price.*'        => 'papier/support (reprographie)',
        'p_doc_color.*'         => 'couleur (reprographie)',
        'supply_item.*'         => 'item (fournitures)',
        'supply_qty.*'          => 'quantité (fournitures)',
        'outsource_name.*'      => 'nom (sous-traitance)',
        'outsource_code.*'      => 'code (sous-traitance)',
        'outsource_cost.*'      => 'cout (sous-traitance)',
        's_quantity.*'          => 'quantité (finition - broché)',
        'book_qty.*'            => 'quantité (finition - livret)',
        'book_sheets.*'         => 'quantité de pages (finition - livret)',
        'book_size.*'           => 'dimension (finition - livret)',
        'sc_book_qty.*'         => 'quantité de livrets (finition - spirale/cerlox)',
        'sc_book_sheets.*'      => 'quantité de pages par livret (finition - spirale/cerlox)',
        'tape_qty.*'            => 'quantité (finition - tape binding)',
        'bind_ins_qty.*'        => "quantité d'insertions (finition - cartable)",
        'drill_holes.*'         => 'trous (finition - percage)',
        'drill_sheets.*'        => 'feuilles (finition - percage)',
        'cut_final_size.*'      => 'dimension final de coupe (finition - coupes)',
        'cut_qty.*'             => 'quantité(finition - coupes)',
        'cut_sheet_qty.*'       => 'feuilles(finition - coupes)',
        'pad_size.*'            => 'dimension (finition - pads)',
        'pad_qty.*'             => 'quantité (finition - pads)',
        'pad_sheet_qty.*'       => 'feuilles (finition - pads)',
        'fold_qty.*'            => 'quantité (finition - plis)',
        'fold_sheet_qty.*'      => 'feuilles (finition - plis)',
        'insert_env_size.*'     => 'dimension enveloppe (finition - insertion)',
        'insert_qty.*'          => 'quantité (finition - insertion)',
        'assembly_quantity_lift.*'=> 'quantity lift (finition - assemblage)',
        'shrink_qty.*'          => 'quantité (finition - shrink wrap)',
    ],          
    ];          