<?php

return [
    'returnDash'    => 'Retour à votre tableau de bord',
    'newTask'	    => 'Nouvelles tâches pour ',
    'editTask'      => 'Éditer tâches',
    'addTask'       => 'Nouvelles tâches ',
    'brief'         => 'Instructions :',
    'reqCompDate'   => 'Date de fin demandée :',
    'comment'       => 'Commentaire :',
    'status'        => 'Statut :',
    'fileDesc'      => 'Description du fichier',
    'allocHours'    => "Nombre d'heures attribuées :",
    'files'         => 'Fichier(s) :',
    'attachments'   => 'Pièces à joindre :',
    'assignation'   => 'Assignation :',
    'personAssign'  => 'Personne assignée :',
    'submit'        => 'Soumettre',
    'reset'         => 'Réinitialiser',
    'type'          => 'Type de tâche :',
    'assignedHours' => 'Heures assignées'
    

];