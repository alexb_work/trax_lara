<?php

return [
    'returnDash'    => 'Retour à votre tableau de bord',
    'addEstimate'   => 'Ajouter une estimation',
    'estimateList'  => 'Liste des estimation pour ',
    'revision'		=> '# de révision',
    'description'	=> "Description de l'estimation",
    'price'         => 'Prix total',
    'status'        => 'Statut',
    'view'          => 'Voir',
    'edit'          => 'Éditer',
    'rejectReason'	=> 'Raison du rejet',
    'rejectQuestion'=> 'Quel est la raison pour le rejet de cette soumission?',
    'confirm'		=> 'Confirmer',
    'cancel'		=> 'Annuler'
    

];