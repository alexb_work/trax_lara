<?php

return [
	'deleteProject'	=> 'Effacer le projet',
    'cancel'		=> 'Annuler',
    'addProject'	=> 'Ajouter un projet',
    'requisition'	=> 'Réquisition',
    'clickEdit'		=> 'Cliquez pour éditer le projet',
    'name'			=> 'Nom',
    'clickManage'	=> 'Cliquez pour gérer les tâches',
    'clientName'	=> 'Nom du client',
    'requestCompDate'=>'Date de livraison demandée',
    'totalTasks'	=> 'Total tâches',
    'projectType'	=> 'Type projet',
    'status'		=> 'Statut',
    'lastChange'	=> 'Dernier changement',
    'clickDelete'	=> 'Cliquez pour effacer le projet',
    'clickSubmission'=>'Cliquez pour gérer les soumission',
    'changeStatus'	=> 'Changer le statut',
    'statusError'	=> 'Le statut du projet doit etre "en cours" pour pouvoir ajouter des taches',
    'close'			=> 'Fermer',
    'deleteConfirm'	=> 'Ce projet sera archivé. Êtes-vous sûr de vouloir poursuivre?',
    'alertOvertime' => 'Ce projet a une ou des tâche(s) qui dépassent le temps alloué'
];