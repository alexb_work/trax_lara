<?php

return [
    'connexion' => 'Connectez-vous à Trax',
    'email'		=> 'Courriel',
    'enterEmail'=> 'Entrez courriel',
    'password'	=> 'Mot de passe',
    'signIn'	=> 'Connectez-vous',
    'rights'	=> 'Tous droits réservés'
];