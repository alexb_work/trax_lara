<?php

return [
    'projects' 		=> 'Projets',
    'allProjects'	=> 'Tous les projets',
    'myProjects'	=> 'Mes projets',
    'archProjects'	=> 'Projets archivés',
    'addProject'	=> 'Ajouter un projet ',
    'overview'		=> "Vue d'ensemble",
    'timeTrack'		=> 'Horaire'
];