<?php

return [
    'returnDash'    => 'Retour à votre tableau de bord',
    'printMgmt'     => 'Gestion des impression',
    'genInfo'       => 'Information Général',
    'projName'      => 'Nom du projet',
    'projOwn'       => 'Responsable du projet :',
    'province'      => 'Province :',
    'recOn'         => 'Reçu le :',
    'infoDue'       => 'Echéance infographie :',
    'startPrint'    => "Date de début de l'impression :",
    'workReq'       => 'Travail éxigé le :',
    'tel'           => 'Tel :',
    'other'         => 'Autre :',
    'address'       => 'Adresse :',
    'delivery'      => 'Livraison :',
    'puroNum'       => 'Numéro purolator :',
    'billInfo'      => 'Renseignements pour la facturation :',
    'costCenter'    => 'Centre de coût :',
    'repro'         => 'Reprographie :',
    'scan'          => 'Numérisation :',
    'yes'           => 'Oui',
    'docName'       => "Nom du document",
    'oneSided'      => 'Recto',
    'dblSided'      => 'R/V',
    'copies'        => 'Nombre de copies',
    'paperSupport'  => 'Papier / Support',
    'type'          => 'Type',
    'price'         => 'Prix',
    'action'        => 'Action',
    'global'        => 'Finition globale :',
    'finition'      => 'Finition:',
    'finiSpecs'     => 'Spécification de la finition :',
    'supplies'      => 'Fournitures :',
    'item'          => 'Item',
    'qty'           => 'Qté',
    'docs'          => 'Documents :',
    'outsource'     => 'Sous-traitance :',
    'name'          => 'Nom',
    'cost'          => 'Coût',
    'info'          => 'Informations :',
    'redo'          => 'Re-faire',
    'explain'       => 'Explication :',
    'submit'        => 'Soumettre',
    'addDelFinish'  => 'Ajouter / Effacer finition',
    'stapled'       => 'Broché',
    '79sheets'      => "+79 feuilles",
    'booklet'       => 'Livret',
    'qtyBooks'      => 'Qté de livrets',
    'qtySheets'      => 'Qté de pages/livret',
    'sizeClose'     => 'Dimension (Fermé)',
    'spiral'        => 'Spirale',
    'cerlox'        => 'Cerlox',
    'color'         => 'Couleur',
    'cover'         => 'Couverture',
    'back'          => 'Dos',
    'tapeBinding'   => 'Tape binding',
    'qtyMin20'      => 'Qté -> min. 20 feuilles || max. 125 feuilles',
    'black'         => 'Noir',
    'blue'          => 'Bleu',
    'binder'        => 'Cartable',
    'suppliedByCust'=> 'Fournis par le client',
    'spine'         => 'Reliure',
    'insert'        => 'Insertion (qté)',
    'comment'       => 'Commentaire',
    'drill'         => 'Percage',
    'holes'         => 'Trous (Qté)',
    'sheetsQty'     => 'Feuilles (Qté)',
    'cuts'          => 'Coupes',
    'finalCutSize'  => 'Dimension final de coupe',
    'cutsQty'       => 'Coupes (qté)',
    'pads'          => 'Bloc-notes',
    'size'          => 'Dimension',
    'padsQty'       => 'Bloc-note (qté)',
    'folds'         => 'Plis',
    'foldsQty'      => 'Plis (qté)',
    'method'        => 'Méthode',
    'shape'         => 'Forme',
    'insertion'     => 'Insertion',
    'envSize'       => 'Dimension env.',
    '(qty)'         => '(qté)',
    'labelQty'      => 'étiquettes (qté)',
    'manSeal'       => 'Quantité de sellage manuel',
    'canadaPost'    => 'Poste Canada',
    'internalMail'  => 'Courrier interne',
    'assembly'      => 'Assemblage',
    'collating'     => 'Méthode de collation',
    'qtyLift'       => 'Quantity lift',
    'shrinkWrap'    => 'Shrink Wrap',
    'quantity'      => 'Quantité',
    'category'      => 'Catégorie',
    'viewPrice'     => 'Détails du prix',
    'printPrice'    => "Prix d'impression",
    'supplyPrice'   => 'Prix des fournitures',
    'finishPrice'   => 'Prix de la finition',
    'outsourcePrice'=> 'Prix de sous-traitance',
    'finishing'     => 'Finition',
    'manual'        => 'Manuel',
    'supplier'      => 'Fournisseur',
    'price'         => 'Prix',
    'winner'        => 'Gagnant',
    

];